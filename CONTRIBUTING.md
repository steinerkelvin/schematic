## Contributing

Please use `git commit -s` flag when authoring commits to include in this
repository. The `-s` flag adds a line to the commit message indicating
agreement to these contribution terms.

## License, Copyright, and Assignment

By contributing to this work, you accept and agree to the following terms
and conditions for your past, present and future contributions submitted.

You assign copyright for your contributions to Scott Milliken.

All contributions are subject to the following license terms in LICENSE.md,
and the Developer's Certificate of Origin.

## Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

* (a) The contribution was created in whole or in part by me and I
  have the right to submit it under the open source license
  indicated in the file; or

* (b) The contribution is based upon previous work that, to the best
  of my knowledge, is covered under an appropriate open source
  license and I have the right under that license to submit that
  work with modifications, whether created in whole or in part
  by me, under the same open source license (unless I am
  permitted to submit under a different license), as indicated
  in the file; or

* (c) The contribution was provided directly to me by some other
  person who certified (a), (b) or (c) and I have not modified
  it.

* (d) I understand and agree that this project and the contribution
  are public and that a record of the contribution (including all
  personal information I submit with it, including my sign-off) is
  maintained indefinitely and may be redistributed consistent with
  this project or the open source license(s) involved.
