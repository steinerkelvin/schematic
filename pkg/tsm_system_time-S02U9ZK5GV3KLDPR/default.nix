stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S02U9ZK5GV3KLDPR";
    name = "tsm_system_time";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-tsm_system_time-R000VZAVYU0Q8OK6>
    ];
}
