stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0EZN7URREJIMESM";
    name = "dblink";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-dblink-R000VZAU4SDFND8D>
    ];
}
