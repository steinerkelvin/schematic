CREATE TABLE meta.revision (
    guid text not null,
    name text not null,
    date timestamp not null default now(),
    env jsonb not null,
    argv text[] not null,
    dependencies text[] not null
);
ALTER TABLE meta.revision ADD CONSTRAINT revision_pk PRIMARY KEY (guid);
CREATE INDEX revision_name_idx ON meta.revision (name);
CREATE INDEX revision_date_idx ON meta.revision (date);
