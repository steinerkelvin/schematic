stdargs @ { scm, pkgs, ... }:

scm.namespace {
    guid = "N0BUZPS77QZ95D7B";
    name = "logic";
    comment = "Utility functions and operators for conditionals for basic logic in SQL.";
}
