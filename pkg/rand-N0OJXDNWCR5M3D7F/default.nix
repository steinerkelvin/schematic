stdargs @ { scm, pkgs, ... }:

scm.namespace {
    guid = "N0OJXDNWCR5M3D7F";
    name = "rand";
    comment = "Utilities for working with random numbers.";
}
