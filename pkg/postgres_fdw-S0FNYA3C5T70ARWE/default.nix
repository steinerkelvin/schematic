stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0FNYA3C5T70ARWE";
    name = "postgres_fdw";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-postgres_fdw-R000VZ9ZL4K06C1M>
    ];
}
