stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAWJKA3L16O";
    name = "2021-09-13-sslinfo";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
