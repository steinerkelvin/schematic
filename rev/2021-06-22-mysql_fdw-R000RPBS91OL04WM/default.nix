stdargs @ { scm, pkgs, ... }:

scm.revision rec {
    guid = "R000RPBS91OL04WM";
    name = "2021-06-22-mysql_fdw";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix (stdargs // {inherit name guid;}) )
    ];
}
