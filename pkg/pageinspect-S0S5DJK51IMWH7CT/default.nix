stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0S5DJK51IMWH7CT";
    name = "pageinspect";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-pageinspect-R000VZ9TE0Y4CYGE>
    ];
}
