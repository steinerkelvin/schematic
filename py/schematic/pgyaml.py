import itertools
from collections import OrderedDict

import yaml
from toolz import dicttoolz

from schematic import seqs


class TableSub:
    ''' Represents a postgresql logical replication subscription to a table.

        :sql_where: a text string to be used as a WHERE clause to filter the table;
            this is only a performance optimization for initial COPY exports, the predicate function still needs
            to be implemented for incremental updates.
        :initcopy: whether to perform an initial COPY export.
        :included: pass false to exclude this table from the subscription
        :inc_columns: a list of column names to include
            non-empty list will cause unnamed columns to be omitted
            an empty list will include all columns (including ones created during processing), except exc_columns
        :exc_columns: a list of column names to omit
    '''

    def __init__(self, tablename, namespace='public', sql_where=None, initcopy=True,
                 inc_columns=None, exc_columns=None, included=True):
        self.tablename = tablename
        self.namespace = namespace
        self.sql_where = sql_where
        self.initcopy = initcopy
        self.included = included
        self._all_columns = self._colkeys = self.colrecs = None
        self.inc_columns = inc_columns
        self.exc_columns = exc_columns
        self.inc_columns_set = set(inc_columns or [])
        self.exc_columns_set = set(exc_columns or [])

    @property
    def all_columns(self):
        return self._all_columns

    @all_columns.setter
    def all_columns(self, val):
        self._all_columns = val
        self._colkeys = [
            c for c in (self.inc_columns or self.all_columns.keys())
            if c in self.all_columns and c not in (self.exc_columns or ())]
        self.colrecs = [self.all_columns[k] for k in self._colkeys]

    @property
    def colkeys(self):
        return self._colkeys

    @property
    def fqname(self):
        return f'{self.namespace}.{self.tablename}'

    @property
    def fqtup(self):
        return (self.namespace, self.tablename)

    def __repr__(self):
        return f'<TableSub({self.namespace!r}, {self.tablename!r})'

def cons_tablesubs(yamlpath, overrides=None):
    ''' Build an OrderedDict of TableSub instances from a yaml file + overrides defined in python.

        :yamlpath: a yaml file that defines the tables and columns to subscribe to;
            TODO: example, generator
            Note! Virtual/computed column names must also be specified in here, or they will be removed.
        :overrides: map of (namespace, tablename) -> values that contain overrides for parameters for TableSub
    '''
    overrides = overrides or {}
    with open(yamlpath, encoding='utf8') as yamlfp:
        schemadef = yaml.load(yamlfp, Loader=yaml.CSafeLoader)
    ts = [TableSub(**dicttoolz.merge(t, overrides.get((t['namespace'], t['tablename'])) or {})) for t in schemadef]
    return OrderedDict([(t.fqtup, t) for t in ts])

def transitive_deps(edges):
    ''' Compute the transitive closure of edge-wise dependencies.
        :edges: a list of tuples (dependee, dependency)
    >>> sorted(list(transitive_deps([(1, 2), (2, 3)])))
    [(1, 2), (1, 3), (2, 3)]
    >>> sorted(list(transitive_deps([(1, 2), (2, 3), (3, 4)])))
    [(1, 2), (1, 3), (1, 4), (2, 3), (2, 4), (3, 4)]
    '''
    dependees = {x: [y for (_, y) in ys] for (x, ys) in seqs.groups(edges, key=lambda x: x[0])}
    edges = set(edges)
    while True:
        initlen = len(edges)
        for x, y in list(edges):
            if y in dependees:
                edges.update([(x, z) for z in dependees[y]])
        if len(edges) == initlen:
            break
    return edges

def get_table_fk_deps(pg):
    return pg.execute('''
        SELECT cl1.oid AS depender, cl2.oid AS dependee
        FROM pg_constraint as co
        JOIN pg_class AS cl1 ON co.conrelid = cl1.oid
        JOIN pg_class AS cl2 ON co.confrelid = cl2.oid
        WHERE co.contype = 'f'
        AND cl1.oid != cl2.oid
        ORDER BY 1, 2;
    ''').all()

def topographic_sort_tables(tables, fk_deps):
    ''' Topographic sort tables so that foreign key dependencies come before dependees. This is helpful for ensuring
        that inserts are performed against dependencies first, to avoid foreign key constraint violations. In the
        absense of a (transitive) dependency between two tables, sort on the (namespace, tablename).
    '''
    table_deps = transitive_deps(fk_deps)
    def table_cmp(t0, t1):
        if (t0.oid, t1.oid) in table_deps:
            return 1
        if (t1.oid, t0.oid) in table_deps:
            return -1
        return 0
    tables = sorted(tables, key=lambda r: (r.namespace, r.tablename))
    for x, y in itertools.product(list(tables), list(tables)):
        x_idx = tables.index(x)
        y_idx = tables.index(y)
        if table_cmp(x, y) and (seqs.cmp(x_idx, y_idx) != table_cmp(x, y)):
            if x_idx < y_idx:
                tables.remove(y)
                tables.insert(x_idx, y)
            else:
                tables.insert(x_idx, y)
                tables.remove(y)
    return tables

def write_schema_yaml(pg, namespaces=('public',), out='/dev/stdout', inc_columns=False):
    ''' Output a schema.yaml file to stdout. Useful for pgsub/oplog subscriptions for clients.
        Takes a postgresql uri reference (presumably a sandbox database built for a client) and outputs a yaml
        object with all tables (in :namespaces:) and optionally columns if :inc_columns:.

        Important! It topographically sorts the tables, so that dependencies come first, helping to avoid foreign
        key constraint violations from inserts coming in the wrong order for initial exports.
    '''
    rels = pg.execute('''
        SELECT c.oid, c.relname as tablename, ns.nspname AS namespace
        FROM pg_class c, pg_namespace ns
        WHERE ns.nspname in %(namespaces)s
        AND ns.oid = c.relnamespace
        AND c.relkind = 'r'
        AND EXISTS (SELECT * FROM pg_constraint con WHERE con.conrelid = c.oid AND con.contype = 'p')
        AND c.relpersistence = 'p'
        ORDER BY ns.nspname, c.relname;
    ''', {'namespaces': tuple(namespaces)}).all()
    rel_oids = {r.oid: r for r in rels}
    cols = pg.execute('''
        SELECT * FROM pg_attribute
        WHERE attrelid IN %(rel_oids)s
        AND attnum > 0
        AND attisdropped IS FALSE
        ORDER BY attrelid, attnum;
    ''', {'rel_oids': tuple(rel_oids.keys())}).all()
    rel_cols = {
        oid: tuple(g)
        for (oid, g) in seqs.groups(cols, key=lambda x: x.attrelid)}
    rels = topographic_sort_tables(rels, get_table_fk_deps(pg))
    def mkrec(t):
        r = {'namespace': t.namespace, 'tablename': t.tablename}
        if inc_columns:
            r['inc_columns'] = [a.attname for a in rel_cols[t.oid]]
        return r
    with open(out, 'w', encoding='utf8') as outfp:
        yaml.dump([mkrec(t) for t in rels], outfp, sort_keys=False, Dumper=yaml.CSafeDumper)
