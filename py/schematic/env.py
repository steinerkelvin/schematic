import os

def get_str(key, default=None):
    return os.environ[key].rstrip('\n') if key in os.environ else default

def get_int(key, default=None):
    try:
        return int(os.environ[key].rstrip('\n'))
    except (TypeError, KeyError):
        return default

def parse_bool(val):
    return {'1': True, '': False, '0': False, 'True': True, 'False': False,
            'true': True, 'false': False, None: False}[val]

def get_bool(key, default=False):
    return parse_bool(get_str(key)) if get_str(key) is not None else default

def unquote(text):
    '''
    >>> unquote('abc')
    'abc'
    >>> unquote('"abc"')
    'abc'
    >>> unquote('"abc')
    '"abc'
    '''
    return text[1:-1] if text and text[0] == text[-1] == '"' else text

def parse_bash_array(text):
    '''
    >>> parse_bash_array(None)
    >>> parse_bash_array('')
    []
    >>> parse_bash_array('a b')
    ['a', 'b']
    >>> parse_bash_array('a b "c d"')
    ['a', 'b', 'c d']
    '''
    import re  # pylint: disable=import-outside-toplevel
    return [unquote(x) for x in re.findall(r'("(?:.+)"|(?:[^ ]+))', text) if x] if text is not None else None

def get_array(key):
    return parse_bash_array(get_str(key)) or []
