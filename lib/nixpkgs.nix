{
    url ? "https://github.com/NixOS/nixpkgs/archive/2992f686ea49cdf7b89a6092b08fa14e4d56efd3.tar.gz",
    sha256 ? "1wfxkymcclx07q9jrb3lyji31fxsq9ynf5r1rwlr68iw45s1crwf",
    overlays ? []
}:
with rec {
    buildPostgreSQL = (import ./postgresql);
}; (import (builtins.fetchTarball { inherit url sha256; }) {
    overlays = [
        (self: super: rec {
            tmux = (self.callPackage ./tmux {});

            postgresql_12 = self.callPackage buildPostgreSQL {
                version = "12.13";
                sha256 = "sha256-tsYjBGr0VI8RqEtAeTTWddEe0HDHk9FbBGg79fMi4C0=";
                this = self.postgresql_12;
                inherit self;
            };

            postgresql_13 = self.callPackage buildPostgreSQL {
                version = "13.9";
                sha256 = "sha256-7xlmwKXkn77TNwrSgkkoy2sRZGF67q4WBtooP38zpBU=";
                this = self.postgresql_13;
                inherit self;
            };

            postgresql_14 = self.callPackage buildPostgreSQL {
                version = "14.6";
                sha256 = "sha256-UIhA/BgJ05q3InTV8Tfau5/X+0+TPaQWiu67IAae3yI=";
                this = self.postgresql_14;
                inherit self;
            };

            postgresql_15beta1 = self.callPackage buildPostgreSQL {
                version = "15beta1";
                sha256 = "sha256-XdikZvsMnsoR8QsSdVJPyPONFpnKxqaJeAtJ6sh4968=";
                this = self.postgresql_15beta1;
                inherit self;
            };

            postgresql_15 = self.callPackage buildPostgreSQL {
                version = "15.1";
                sha256 = "sha256-ZP3yPXNK+tDf5Ad9rKlqxR3NaX5ori09TKbEXLFOIa4=";
                this = self.postgresql_15;
                inherit self;
            };

            postgresql = postgresql_15;
        })
    ] ++ overlays;
})
