CREATE EXTENSION IF NOT EXISTS bloom WITH SCHEMA public;
COMMENT ON EXTENSION bloom IS 'bloom access method - signature file based index';

