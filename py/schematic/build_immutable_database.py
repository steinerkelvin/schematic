'''
Build a new postgresql database server.
'''
import os
import subprocess
import sys

import psycopg2

from schematic import env
from schematic.build_util import build_setup, pg_ctl_fn, apply_sql_file, pg_is_ready, mkdir

def create_database(out, port, user, dbname):
    conn = psycopg2.connect(env.get_str('pguri_postgres'))
    c = conn.cursor()
    c.execute('''SELECT * FROM pg_database WHERE datname = %(dbname)s;''', vars={'dbname': dbname})
    db = c.fetchone()
    if not db:
        subprocess.check_call([
            os.path.join(out, 'bin/createdb'), '-p', str(port), '-U', user, dbname,
            '--encoding', 'utf8', '--locale', 'C'])

def apply_sql_files():
    pguri = env.get_str('pguri')
    conn = psycopg2.connect(pguri)
    c = conn.cursor()
    upgrade_sql = env.get_str('upgrade_sql')
    print('applying %r' % upgrade_sql, file=sys.stdout)
    apply_sql_file(c, upgrade_sql)
    conn.commit()

def main():
    build_setup()
    port = int(env.get_str('port'))
    name = env.get_str('name')  # pylint: disable=unused-variable
    dbname = env.get_str('dbname')
    user = env.get_str('user')
    out = env.get_str('out')
    mkdir(out)
    pgdir = env.get_str('postgresql')
    for subdir in 'bin include lib share'.split():
        subprocess.check_call(['rsync', '-arc', os.path.join(pgdir, subdir), out])
        subprocess.check_call(['chmod', '-R', '+w', os.path.join(out, subdir)])
    datadir = os.path.join(out, 'data')
    if not os.path.exists(datadir):
        with open('pwfile', 'w') as fp:
            fp.write(env.get_str('password'))
        subprocess.check_call([
            os.path.join(out, 'bin/initdb'), '-D', datadir, '-U', user, '--pwfile', 'pwfile',
            '--encoding', 'utf8', '--locale', 'C'])
        os.remove('pwfile')
        subprocess.check_call(['chmod', '-R', '0700', datadir])
        # TODO: consider using ALTER SYSTEM instead of writing to postgresql.conf
        with open(os.path.join(datadir, 'postgresql.conf'), 'w') as fp:
            fp.write('port = %d\n' % port)
    if not pg_is_ready(out, port):
        pg_ctl_fn(out, 'start')
    create_database(out, port, user, dbname)
    apply_sql_files()

if __name__ == '__main__':
    main()
