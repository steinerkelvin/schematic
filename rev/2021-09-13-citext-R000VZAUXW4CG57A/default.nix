stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAUXW4CG57A";
    name = "2021-09-13-citext";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
