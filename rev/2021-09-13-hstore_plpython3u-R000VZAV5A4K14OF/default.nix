stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAV5A4K14OF";
    name = "2021-09-13-hstore_plpython3u";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-plpython3u-R000VZDMTW1CCIL1>
        <2021-09-13-hstore-R000VZAVDPM02V78>
    ];
}
