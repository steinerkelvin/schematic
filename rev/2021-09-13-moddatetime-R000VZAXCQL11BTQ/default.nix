stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAXCQL11BTQ";
    name = "2021-09-13-moddatetime";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
