stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0F8OSBS2P1R25Z8";
    name = "pgrowlocks";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-pgrowlocks-R000VZAU01S29LYD>
    ];
}
