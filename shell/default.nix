args @ { pkgs, SCM_PATH, repos, verbose ? false, version ? null }:

let stdenv = pkgs.stdenv; in
pkgs.mkShell rec {
    name = "schematic";
    inherit SCM_PATH;
    SCM_VERBOSE = if verbose then "1" else "0";
    SCM_VERSION = version;
    SCM_REPOS = builtins.concatStringsSep ":" args.repos;
    shellHook = ''
        source ${SCM_PATH}/shell/.bashrc
    '';
    buildInputs = (with pkgs; [
        (pkgs.callPackage ../lib/python3 {})
        bashInteractive
        broot
        curl
        git
        glibcLocales
        gnugrep
        hostname
        htop
        jq
        less
        lsof
        man
        nix-prefetch-scripts
        postgresql
        s3cmd
        shellcheck
        tig
        tmux
        tree
        which
    ] ++ (if stdenv.isLinux then [
        # pgadmin  # build error with postgresql14
        pg_top
    ] else []));
}
