stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAUKUQZ13LE";
    name = "2021-09-13-ltree";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
