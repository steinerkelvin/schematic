stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "DISKYALJIBASOCAC";
    name = "2020-03-23-pg_repack";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
