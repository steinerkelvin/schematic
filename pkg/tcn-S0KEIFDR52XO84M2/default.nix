stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0KEIFDR52XO84M2";
    name = "tcn";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-tcn-R000VZAYATX8ZWBU>
    ];
}
