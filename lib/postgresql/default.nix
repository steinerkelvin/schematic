# dependencies
{ stdenv, lib, fetchurl, makeWrapper
, glibc, zlib, readline, openssl, icu, libossp_uuid
, pkgconfig, libxml2, tzdata, lz4
, this, self
, perl, tcl, python310
, version, sha256
}:
stdenv.mkDerivation rec {
    pname = "postgresql";
    inherit version;
    src = fetchurl {
      url = "mirror://postgresql/source/v${version}/${pname}-${version}.tar.bz2";
      inherit sha256;
    };
    outputs = [ "out" ];
    setOutputFlags = false; # $out retains configureFlags :-/
    buildInputs =
      [ zlib
        readline
        libxml2
        makeWrapper
        # clang
        # llvm
        perl
        python310
        tcl
        openssl
        icu
        lz4
      ]
      ++ lib.optionals (!stdenv.isDarwin) [ libossp_uuid ];
    nativeBuildInputs = [ pkgconfig ];
    enableParallelBuilding = !stdenv.isDarwin;
    buildFlags = [ "world" ];
    NIX_CFLAGS_COMPILE = "-I${libxml2.dev}/include/libxml2";
    # Otherwise it retains a reference to compiler and fails; see #44767.  TODO: better.
    preConfigure = "CC=${stdenv.cc.targetPrefix}cc";
    configureFlags = [
      # "--with-llvm"
      "--with-python"
      "--with-perl"
      "--with-tcl"
      "--with-segsize=16"
      "--with-blocksize=8"
      "--with-openssl"
      "--with-libxml"
      "--with-icu"
      "--with-lz4"
      "--sysconfdir=/etc"
      "--with-system-tzdata=${tzdata}/share/zoneinfo"
      (if stdenv.isDarwin then "--with-uuid=e2fs" else "--with-ossp-uuid")
    ];
    patches = [
        ./patches/disable-resolve_symlinks-94.patch
        ./patches/less-is-more-96.patch
        ./patches/hardcode-pgxs-path-96.patch
        ./patches/specify_pkglibdir_at_runtime.patch
        ./patches/findstring.patch
    ];
    installTargets = [ "install-world" ];
    LC_ALL = "C";
    postConfigure = ''
        # Hardcode the path to pgxs so pg_config returns the path in $out
        substituteInPlace "src/common/config_info.c" --replace HARDCODED_PGXS_PATH $out/lib
    '';
    postInstall = ''
        moveToOutput "lib/pgxs" "$out" # looks strange, but not deleting it
        moveToOutput "lib/libpgcommon*.a" "$out"
        moveToOutput "lib/libpgport*.a" "$out"
        moveToOutput "lib/libecpg*" "$out"

        # Prevent a retained dependency on gcc-wrapper.
        substituteInPlace "$out/lib/pgxs/src/Makefile.global" --replace ${stdenv.cc}/bin/ld ld

        if [ -z "''${dontDisableStatic:-}" ]; then
          # Remove static libraries in case dynamic are available.
          for i in $out/lib/*.a $lib/lib/*.a; do
            name="$(basename "$i")"
            ext="${stdenv.hostPlatform.extensions.sharedLibrary}"
            if [ -e "$lib/lib/''${name%.a}$ext" ] || [ -e "''${i%.a}$ext" ]; then
              rm "$i"
            fi
          done
        fi
      '';
    postFixup = lib.optionalString (!stdenv.isDarwin && stdenv.hostPlatform.libc == "glibc")
      ''
        # initdb needs access to "locale" command from glibc.
        wrapProgram $out/bin/initdb --prefix PATH ":" ${glibc.bin}/bin
      '';
    doCheck = !stdenv.isDarwin;
    # autodetection doesn't seem to able to find this, but it's there.
    checkTarget = "check";
    preCheck =
      # On musl, comment skip the following tests, because they break due to
      #     ! ERROR:  could not load library "/build/postgresql-11.5/tmp_install/nix/store/...-postgresql-11.5-lib/lib/libpqwalreceiver.so": Error loading shared library libpq.so.5: No such file or directory (needed by /build/postgresql-11.5/tmp_install/nix/store/...-postgresql-11.5-lib/lib/libpqwalreceiver.so)
      # See also here:
      #     https://git.alpinelinux.org/aports/tree/main/postgresql/disable-broken-tests.patch?id=6d7d32c12e073a57a9e5946e55f4c1fbb68bd442
      if stdenv.hostPlatform.isMusl then ''
        substituteInPlace src/test/regress/parallel_schedule \
          --replace "subscription" "" \
          --replace "object_address" ""
      '' else null;
    doInstallCheck = false; # needs a running daemon?
    disallowedReferences = [ stdenv.cc ];
    meta = with lib; {
      homepage    = https://www.postgresql.org;
      description = "A powerful, open source object-relational database system";
      license     = licenses.postgresql;
      platforms   = platforms.unix;
    };
}
