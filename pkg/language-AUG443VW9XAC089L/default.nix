stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "AUG443VW9XAC089L";
    name = "language";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-12-10-language-R000HQQ3VW9NWU28>
    ];
}
