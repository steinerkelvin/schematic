stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S02OJ7RPEKLQ97I2";
    name = "logic.if";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <logic-N0BUZPS77QZ95D7B>
        <2021-09-07-logic.if-R000VN4T3C4O1U2P>
    ];
}
