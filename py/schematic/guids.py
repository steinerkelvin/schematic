import datetime
import hashlib
import random
import string  # pylint: disable=deprecated-module
import sys

alphabet = string.digits + string.ascii_uppercase
prefixes = {
    'C1': 'pghba',
    'D0': 'database',
    'D1': 'deployment',
    'B0': 'build',
    'R0': 'revision',
    'S0': 'schema',
    'N0': 'namespace',
    'T1': 'tablespace',
    'S1': 'server',
    'P9': 'password',
}

def _get_guid(prefix, length=16, seed=None, alphabet=alphabet):
    '''
    Get a randomly generated unique ID using schematic convention.

    >>> _get_guid('S0', length=16, seed=1)
    'S084G7VSUOD6V1OR'
    '''
    if seed:
        random.seed(seed)
    assert any(prefix.startswith(p) for p in prefixes)
    ret = prefix
    for _ in range(length - len(prefix)):
        ret += random.choice(alphabet)
    return ret

def _encode_int(n, length, alphabet=alphabet):
    '''
    Encode non-negative integer into alphabet.
    >>> _encode_int(0, 3)
    '000'
    >>> _encode_int(1, 3)
    '001'
    >>> _encode_int(100, 3)
    '02S'
    >>> _encode_int(1000, 3)
    '0RS'
    >>> _encode_int(10000, 3)
    '7PS'
    >>> _encode_int(100000, 3)
    '255S'
    >>> _encode_int(1000000, 3)
    'LFLS'
    >>> _encode_int(70000000000, 7)
    'W5O6XOG'
    '''
    ret = ''
    while n:
        div = int(n / len(alphabet))
        mod = n % len(alphabet)
        ret += alphabet[mod]
        n = div
    return ret[::-1].rjust(length, '0')

def _get_time_prefix(now=None):
    '''
    Encode number of seconds since genesis date. Makes guids sortable/comparable.

    >>> _get_time_prefix(now=datetime.datetime(2020, 5, 11, 8, 30, 59))
    '006R65Y'
    >>> _get_time_prefix(now=datetime.datetime(2020, 10, 4, 8, 30, 59))
    '00E9JHY'
    >>> _get_time_prefix(now=datetime.datetime(2022, 10, 4, 8, 30, 59))
    '01FTE5Y'
    >>> _get_time_prefix(now=datetime.datetime(3022, 10, 4, 8, 30, 59))
    'EJBZGTY'
    '''
    genesis = datetime.datetime(2020, 1, 1, 1, 1, 1)
    now = now or datetime.datetime.now()
    seconds = int((now - genesis).total_seconds())
    return _encode_int(seconds, 7)

def get_database_guid(seed=None):
    '''
    >>> get_database_guid(seed=1)
    'D084G7VSUOD6V1OR'
    '''
    return _get_guid('D0', seed=seed)

def get_deployment_guid(now=None, seed=None):
    '''
    >>> get_deployment_guid(now=datetime.datetime(2021, 5, 4, 8, 35, 55), seed=1)
    'D100P652684G7VSU'
    '''
    return _get_guid('D1' + _get_time_prefix(now), seed=seed)

def get_schema_guid(seed=None):
    '''
    >>> get_schema_guid(seed=1)
    'S084G7VSUOD6V1OR'
    '''
    return _get_guid('S0', seed=seed)

def get_tablespace_guid(seed=None):
    '''
    >>> get_tablespace_guid(seed=1)
    'T184G7VSUOD6V1OR'
    '''
    return _get_guid('T1', seed=seed)

def get_pghba_guid(seed=None):
    '''
    >>> get_pghba_guid(seed=1)
    'C184G7VSUOD6V1OR'
    '''
    return _get_guid('C1', seed=seed)

def get_namespace_guid(seed=None):
    '''
    >>> get_namespace_guid(seed=2)
    'N0355NAJGD2ARPWN'
    '''
    return _get_guid('N0', seed=seed)

def get_server_guid(seed=None):
    '''
    >>> get_server_guid(seed=1)
    'S184G7VSUOD6V1OR'
    '''
    return _get_guid('S1', seed=seed)

def get_build_guid(seed=None):
    '''
    >>> get_build_guid(seed=1)
    'B084G7VSUOD6V1OR'
    '''
    return _get_guid('B0', seed=seed)

def get_revision_guid(now=None, seed=None):
    '''
    >>> get_revision_guid(now=datetime.datetime(2020, 5, 11, 8, 30, 59), seed=1)
    'R0006R65Y84G7VSU'
    >>> get_revision_guid(now=datetime.datetime(2020, 10, 4, 8, 30, 59), seed=1)
    'R000E9JHY84G7VSU'
    >>> get_revision_guid(now=datetime.datetime(2022, 10, 4, 8, 30, 59), seed=1)
    'R001FTE5Y84G7VSU'
    >>> get_revision_guid(now=datetime.datetime(3022, 10, 4, 8, 30, 59), seed=1)
    'R0EJBZGTY84G7VSU'
    '''
    return _get_guid('R0' + _get_time_prefix(now), seed=seed)

def get_password(seed=None):
    '''
    >>> get_password(seed=1)
    'P98ASPM4g7vMsuFoOd'
    '''
    return _get_guid('P9', seed=seed, length=18, alphabet=string.digits + string.ascii_letters)

def content_hash(content):
    return hashlib.sha256(content).digest()[:-2].encode('base64').strip()[:16]

if __name__ == '__main__':
    guid_type = sys.argv[1]
    if guid_type == 'revision':
        print(get_revision_guid())
    elif guid_type == 'deployment':
        print(get_deployment_guid())
    elif guid_type == 'password':
        print(get_password())
    elif guid_type == 'pghba':
        print(get_pghba_guid())
    else:
        prefix = [k for (k, v) in prefixes.items() if v == guid_type][0]
        print(_get_guid(prefix))
