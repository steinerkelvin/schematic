stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0DHLWSSNULIJDFV";
    name = "pgcrypto";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-pgcrypto-R000VZAY6PBO3C1E>
    ];
}
