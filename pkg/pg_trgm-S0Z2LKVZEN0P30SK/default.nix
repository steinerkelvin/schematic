stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0Z2LKVZEN0P30SK";
    name = "pg_trgm";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-pg_trgm-R000VZ9XQ1A6277S>
    ];
}
