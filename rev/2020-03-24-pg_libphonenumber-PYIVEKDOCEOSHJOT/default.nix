stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "PYIVEKDOCEOSHJOT";
    name = "2020-03-24-pg_libphonenumber";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
