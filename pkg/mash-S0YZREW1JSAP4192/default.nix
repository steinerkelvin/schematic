stdargs @ { scm, ... }:

scm.schema {
    guid = "S0YZREW1JSAP4192";
    name = "mash";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2022-08-28-mash-R001DXDDZ09BVMO2>
    ];
}
