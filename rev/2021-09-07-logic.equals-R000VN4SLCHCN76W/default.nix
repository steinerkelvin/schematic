stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VN4SLCHCN76W";
    name = "2021-09-07-logic.equals";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <logic-N0BUZPS77QZ95D7B>
    ];
}
