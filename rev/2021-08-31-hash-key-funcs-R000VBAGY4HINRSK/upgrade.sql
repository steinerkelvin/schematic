CREATE OR REPLACE FUNCTION public.key16(VARIADIC text[])
 RETURNS smallint
 LANGUAGE sql
 IMMUTABLE PARALLEL SAFE LEAKPROOF
AS $function$
    SELECT ('x' || md5(concat_ws(
        'OdamIadewtIcpifcovDoasheimKadoic',
        VARIADIC (SELECT array_agg(coalesce(x::text, 'UskEursuvMaurAunIranElbOtAvEibco')) FROM unnest($1) x))))::bit(16)::smallint;
$function$
;

CREATE OR REPLACE FUNCTION public.key32(VARIADIC text[])
 RETURNS integer
 LANGUAGE sql
 IMMUTABLE PARALLEL SAFE LEAKPROOF
AS $function$
    SELECT ('x' || md5(concat_ws(
        'OdamIadewtIcpifcovDoasheimKadoic',
        VARIADIC (SELECT array_agg(coalesce(x::text, 'UskEursuvMaurAunIranElbOtAvEibco')) FROM unnest($1) x))))::bit(32)::integer;
$function$
;

CREATE OR REPLACE FUNCTION public.key64(VARIADIC text[])
 RETURNS bigint
 LANGUAGE sql
 IMMUTABLE PARALLEL SAFE LEAKPROOF
AS $function$
    SELECT ('x' || md5(concat_ws(
        'OdamIadewtIcpifcovDoasheimKadoic',
        VARIADIC (SELECT array_agg(coalesce(x::text, 'UskEursuvMaurAunIranElbOtAvEibco')) FROM unnest($1) x))))::bit(64)::bigint;
$function$
;

CREATE OR REPLACE FUNCTION public.key16(VARIADIC anyarray)
 RETURNS smallint
 LANGUAGE sql
 IMMUTABLE PARALLEL SAFE LEAKPROOF
AS $function$
    SELECT public.key16(VARIADIC $1::text[]);
$function$
;

CREATE OR REPLACE FUNCTION public.key32(VARIADIC anyarray)
 RETURNS integer
 LANGUAGE sql
 IMMUTABLE PARALLEL SAFE LEAKPROOF
AS $function$
    SELECT public.key32(VARIADIC $1::text[]);
$function$
;

CREATE OR REPLACE FUNCTION public.key64(VARIADIC anyarray)
 RETURNS bigint
 LANGUAGE sql
 IMMUTABLE PARALLEL SAFE LEAKPROOF
AS $function$
    SELECT public.key64(VARIADIC $1::text[]);
$function$
;


COMMENT ON FUNCTION public.key16(VARIADIC text[]) IS 'Compute a 16-bit/smallint hash key for use as a space efficient identifier (example: for deterministic sorting, or a unique key where collisions are OK). Accepts a variable number of arguments that can cast to text.';
COMMENT ON FUNCTION public.key32(VARIADIC text[]) IS 'Compute a 32-bit/integer hash key for use as a space efficient identifier (example: for deterministic sorting, unique key of a tiny table of a few dozen rows, or a unique key where collissions are OK). Accepts a variable number of arguments that can cast to text.';
COMMENT ON FUNCTION public.key64(VARIADIC text[]) IS 'Compute a 64-bit/integer hash key for use as a space efficient identifier (example: for deterministic sorting, unique key of a small table of up to about a million rows, or a unique key where collissions are OK). Accepts a variable number of arguments that can cast to text.';
