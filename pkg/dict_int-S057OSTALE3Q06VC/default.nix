stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S057OSTALE3Q06VC";
    name = "dict_int";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-dict_int-R000VZAX0IX5E44G>
    ];
}
