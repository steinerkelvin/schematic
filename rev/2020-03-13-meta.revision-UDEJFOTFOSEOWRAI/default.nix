stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "UDEJFOTFOSEOWRAI";
    name = "2020-03-13-meta.revision";
    add_meta_revision = false;  # prevent self-dependency
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <meta-N0FLM2YWXLMJE5BC>
    ];
}
