stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZATV8C6DE82";
    name = "2021-09-13-intagg";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
