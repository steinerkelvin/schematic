stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAU82APQW3F";
    name = "2021-09-13-pg_freespacemap";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
