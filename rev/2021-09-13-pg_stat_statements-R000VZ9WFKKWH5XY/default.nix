stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZ9WFKKWH5XY";
    name = "2021-09-13-pg_stat_statements";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
