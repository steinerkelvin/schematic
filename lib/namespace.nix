stdargs @ { scm, pkgs, stdenv, postgresql, python3, rsync, ... }:
args @ {
    guid, name, comment ? null,
    dependencies ? [], preBuildInputs ? null,
    ...
}:

rec {
    scm_type = "namespace";
    inherit guid name dependencies preBuildInputs comment;
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    apply = { self, server }: scm.effect rec {
        inherit (server) postgresql pguri port basedir;
        inherit (self) guid name scm_type transDepGuids comment;
        SCM_EFFECT_PROG = ../py/schematic/build_namespace.py;
        buildInputs = [ rsync ] ++ (scm.buildDeps server self);
    };
}
