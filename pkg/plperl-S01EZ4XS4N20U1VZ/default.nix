stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S01EZ4XS4N20U1VZ";
    name = "plperl";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-plperl-R000VZATJTXXQGMN>
    ];
}
