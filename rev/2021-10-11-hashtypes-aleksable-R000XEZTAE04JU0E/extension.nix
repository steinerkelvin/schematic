{ stdenv, pkgs, postgresql, ... }:

stdenv.mkDerivation {

  name = "hashtypes";
  src = pkgs.fetchurl {
      url = "https://github.com/aleksabl/hashtypes/archive/master.tar.gz";
      sha256 = "0yqh03j0c1y5hxrs311yrsfrnns9j1h4laafh8ykvs4wsg6wkxpd";
  };
  installPhase = ''
    install -D hashtypes.so -t $out/basefiles/lib/
    install -D ./{sql/hashtypes--0.1.5.sql,hashtypes.control,sql/md5.sql,sql/crc32.sql,sql/sha.sql.type} -t $out/basefiles/share/postgresql/extension/
  '';
  buildInputs = [ postgresql];
}
