stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000GHY0WOEAEDLN";
    name = "2020-11-16-immutable_random";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <rand-N0OJXDNWCR5M3D7F>
    ];
}
