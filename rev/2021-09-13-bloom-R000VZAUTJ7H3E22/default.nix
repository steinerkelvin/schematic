stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAUTJ7H3E22";
    name = "2021-09-13-bloom";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
