CREATE OR REPLACE FUNCTION public.show_mash(x mash) RETURNS text
LANGUAGE sql IMMUTABLE STRICT LEAKPROOF PARALLEL SAFE AS $_$
    SELECT concat((CASE get_byte(x, 0)
        WHEN 1 THEN 'md5'
        WHEN 2 THEN 'sha-1'
        WHEN 3 THEN 'sha-256'
        WHEN 4 THEN 'blake2b-128'
    END), '/', encode(substr(x::bytea, 2), 'hex'));
$_$;
