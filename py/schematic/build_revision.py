import json
import os
import sys

from schematic import env, pgcn
from schematic.build_util import install_buildinput_files, apply_sql_file, log_msg_upgrade_sql


def revision_exists(pg):
    return pg.execute('''
        SELECT EXISTS (SELECT * FROM pg_tables WHERE schemaname = 'meta' AND tablename = 'revision');
    ''').scalar()

def is_applied(pg, guid):
    return revision_exists(pg) and pg.execute('''
        SELECT EXISTS (SELECT * FROM meta.revision WHERE guid = %(guid)s);
    ''', {'guid': guid}).scalar()

def insert_revision(pg):
    pg.execute('''
        INSERT INTO meta.revision (guid, name, env, argv, dependencies)
        SELECT
            %(guid)s,
            %(name)s,
            %(env)s,
            %(argv)s,
            %(dependencies)s
    ''', {
        'env': json.dumps(dict(os.environ)),
        'argv': sys.argv,
        'guid': env.get_str('guid'),
        'name': env.get_str('name'),
        'dependencies': env.get_array('dependencies')})

def apply_revision():
    autocommit = env.get_bool('autocommit')
    with pgcn.connect(env.get_str('pguri'), autocommit=autocommit) as pg:
        upgrade_sql = env.get_str('upgrade_sql')
        if pg.execute('SELECT pg_is_in_recovery();').scalar():
            print('read-only, skipping %r' % upgrade_sql, file=sys.stderr)
            return
        name = env.get_str('name')
        guid = env.get_str('guid')
        if is_applied(pg, guid):
            print('already applied: %s-%s' % (name, guid), file=sys.stderr)
            return
        scm_fast_forward = env.get_bool('SCM_FAST_FORWARD')
        add_meta_revision = env.get_bool('add_meta_revision')  # true if the revision is adding the meta.revision table
        if scm_fast_forward and add_meta_revision:
            log_msg_upgrade_sql(upgrade_sql, prefix='fast-forward')
        else:
            apply_sql_file(pg, upgrade_sql)
        insert_revision(pg)
        pg.commit()

def main():
    sandbox_mode = env.get_str('SCM_SANDBOX_MODE', 'imperative')
    if sandbox_mode == 'declarative':
        # creating an "declarative" sandbox database from declarative definitions (instead of revisions)
        pass
    elif sandbox_mode == 'imperative':
        # creating a "imperative" sandbox database from revisions
        install_buildinput_files()
        apply_revision()

if __name__ == '__main__':
    main()
