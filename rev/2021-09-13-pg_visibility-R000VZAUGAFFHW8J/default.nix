stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAUGAFFHW8J";
    name = "2021-09-13-pg_visibility";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
