{ pkgs
, stdenv
, buildPythonPackage
, six
, sqlbag
, schemainspect
}: buildPythonPackage rec {
    name = "migra-1.0.1610499506";
    src = pkgs.fetchurl {
        url = "https://files.pythonhosted.org/packages/ba/af/9d77d3fe24ddb5b5fff6faf8c493e5d146cb283f259abced5f0680a88ea8/migra-3.0.1658662267.tar.gz";
        sha256 = "sha256-LxTrlq+uTkgneL4wDuRQNjtK3ivrRgQM68zQrD7ePs0=";
    };
    propagatedBuildInputs = [
        six
        schemainspect
        sqlbag
    ];
}
