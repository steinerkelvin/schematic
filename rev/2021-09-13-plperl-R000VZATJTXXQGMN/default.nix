stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZATJTXXQGMN";
    name = "2021-09-13-plperl";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
