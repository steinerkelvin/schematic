stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAV9K7MHKBA";
    name = "2021-09-13-earthdistance";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-cube-R000VZAVI37GCQPX>
    ];
}
