CREATE EXTENSION IF NOT EXISTS pg_visibility WITH SCHEMA public;
COMMENT ON EXTENSION pg_visibility IS 'examine the visibility map (VM) and page-level visibility info';

