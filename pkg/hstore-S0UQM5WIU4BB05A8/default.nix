stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0UQM5WIU4BB05A8";
    name = "hstore";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-hstore-R000VZAVDPM02V78>
    ];
}
