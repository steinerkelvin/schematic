{ pkgs, stdenv, postgresql, ... }:

stdenv.mkDerivation rec {
    name = "vector-${version}";
    version = "0.2.5";
    src = pkgs.fetchurl {
        url = "https://github.com/ankane/pgvector/archive/refs/tags/v${version}.tar.gz";
        sha256 = "sha256-/umk3hXSAGlg85tCJMhy6El0P8E0itlTlEGGB88qEJA=";
    };
    installPhase = ''
        ls -lah
        targetdir=$out/basefiles
        install -D vector.so -t $targetdir/lib/
        install -D vector.control -t $targetdir/share/postgresql/extension/
        install -D sql/vector--${version}.sql -t $targetdir/share/postgresql/extension/
    '';
    buildInputs = [
        postgresql
    ];
}
