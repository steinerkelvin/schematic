{ pkgs, stdenv }:

with rec {
    py3 = pkgs.python310.override {
        packageOverrides = self: super: with self; {
            autodebug = (self.callPackage ./autodebug.nix {});
            bugsnag = (self.callPackage ./bugsnag.nix {});
            locust = (self.callPackage ./locust.nix {});
            migra = (self.callPackage ./migra.nix {});
            pglast = (self.callPackage ./pglast.nix {});
            psycopg2 = (self.callPackage ./psycopg2.nix {});
            pylint_quotes = (self.callPackage ./pylint_quotes.nix {});
            schemainspect = (self.callPackage ./schemainspect.nix {});
            sqlbag = (self.callPackage ./sqlbag {});
        };
    };
}; py3.buildEnv.override {
    ignoreCollisions = true;
    extraLibs = with py3.pkgs; [
        (if stdenv.isLinux then autodebug else null)
        (if stdenv.isLinux then pylint else null)
        Fabric
        boto3
        bugsnag
        click
        clint
        gevent
        ipython
        isort
        locust
        lz4
        migra
        nose
        pglast
        psutil
        psycopg2
        pudb
        pylint_quotes
        pytz
        requests
        schemainspect
        setproctitle
        sh
        sqlalchemy
        sqlbag
        tkinter
        toolz
    ];
}
