'''
Utility module for using logical replication to sync from one postgresql database to another.

Compared to the build-in logical replication feature of postgresql ("CREATE SUBSCRIPTION ..."):
- merging into populated tables is supported (without errors due to conflicting records)
- ingesting updates from multiple sources into one destination is possible, even when the tables and records overlap
- the subscription is defined dynamically, without the need for schema migrations
- initial export doesn't cause as much load on source database (one table is exported at a time instead of all at once)
- WALs don't accumulate as much during initial export (replication slot advances between table exports)
- subsets of columns are supported
- mapping columns with functions is possible
'''

import datetime
import itertools
import multiprocessing
import time

from schematic import pgcn, pgingest, pgcopy, pglsn, pgoutput, pgsub, slog


def initcopy_proc(pguri, replication_role, column_defs, tablesub, table, queue, is_done, onconflict):
    with pgcn.connect(pguri) as pg:
        pgingest.init_replication_settings(pg, replication_role)
        while not is_done.is_set() or not queue.empty():
            try:
                batch = queue.get(timeout=.1)
            except multiprocessing.queues.Empty:
                continue
            if not batch:
                continue
            pgingest.upsert_batch(pg, table, [
                pgsub.decode_to_dict(tablesub.colrecs, pgcopy.decode_record(x), pg.cur) for x in batch],
                column_defs, onconflict)
            pg.commit()

def ingress_initcopy(
        pg, command, stream, start_tsn, column_defs, replication_role, onconflict,
        parallelism=16, status_interval: int=5 * 60):
    ''' Ingest stream, applying records to database.

        For higher performance (reducing round-trips to database), consider using `execute_batch`:
        https://www.psycopg.org/docs/extras.html#fast-execution-helpers
    '''
    ops, tsn, tts, txn, tablesub, lastlog = 0, command.tsn, command.tts, command.txn, command.tablesub, None
    namespace, tablename = tablesub.namespace, tablesub.tablename
    table = {
        'namespace': namespace, 'tablename': tablename,
        'key': [c.attname for c in tablesub.all_columns.values() if c.iskey]}
    is_done = multiprocessing.Event()
    queue = multiprocessing.Queue()
    procs = []
    for i in range(parallelism):
        p = multiprocessing.Process(
            name='scm initcopy %s/%s %s.%s' % (i, parallelism, namespace, tablename),
            target=initcopy_proc,
            args=(pg.pguri, replication_role, column_defs, tablesub, table, queue, is_done, onconflict))
        p.start()
        procs.append(p)
    def logstatus():
        slog.info2(f'initcopy {namespace}.{tablename} {ops:10d} inserts, txn {txn}, tsn {tsn}, tts {tts}')
    logstatus()
    stream = itertools.dropwhile(lambda _: start_tsn and tsn <= start_tsn, stream)  # already written this record, skip
    stream = itertools.takewhile(lambda x: not isinstance(x, pgsub.CopyTableEnd), stream)
    for copybatch in stream:
        queue.put(copybatch.records)
        while queue.qsize() >= parallelism:
            time.sleep(.1)
        ops += len(copybatch.records)
        now = time.time()
        if not lastlog or now - lastlog >= status_interval:
            logstatus()
            lastlog = now
    is_done.set()
    for p in procs:
        p.join()
        assert p.exitcode == 0, 'got non-zero exitcode from initcopy_proc: %r' % p.exitcode
        p.close()
    logstatus()
    # This logical message is partially a work-around for a bug that prevents the logical replication origin from being
    # updated in pgingest.commit_tsn. The bug occurs because this main session isn't doing any writes of its own,
    # and pg_replication_origin_xact_setup doesn't take effect if no writes have been performed in the same txn.
    fqtable = '%s.%s' % (tablesub.namespace, tablesub.tablename)
    pg.execute('''SELECT pg_logical_emit_message(true, 'fujladyoihys-initcopy-finished', %s);''', (fqtable,))
    pgingest.commit_tsn(pg, tsn, tts)

def crud_table_field(relation, _cache={}):  # pylint: disable=dangerous-default-value
    tbl = (relation.namespace, relation.tablename)
    if tbl not in _cache:
        _cache[tbl] = {
            'namespace': relation.namespace,
            'tablename': relation.tablename,
            'key': [c.attname for c in relation.columns if c.iskey],
        }
    return _cache[tbl]

def ingress_stream(pg, stream, start_tsn, column_defs, onconflict, status_interval: int=5 * 60):
    ''' Ingest stream, applying records to database. '''
    txn = tsn = tts = lastlog = None
    def logstatus(i, txn, tsn, tts):
        lag = datetime.datetime.utcnow() - tts.replace(tzinfo=None) if tts else '?'
        slog.info2(f'''cursor {i:10d} ops, txn {txn}, tsn {tsn}, tts {tts}, lag {lag}''')
    i = 0
    for i, lmsg in enumerate(stream, start=1):
        if isinstance(lmsg, pgsub.StreamEnd):
            break
        payload, tsn, tts, txn = lmsg.payload, lmsg.tsn, lmsg.tts, lmsg.txn
        now = time.time()
        if txn and not lastlog or now - lastlog >= status_interval:
            logstatus(i, txn, tsn, tts)
            lastlog = now
        if isinstance(payload, (pgoutput.Begin, pgoutput.Origin, pgoutput.PgType, pgoutput.Relation, pgoutput.Message)):
            pass
        if start_tsn and tsn and tsn <= start_tsn:
            continue  # already written this op, skip
        if isinstance(payload, pgoutput.Commit):
            pgingest.commit_tsn(pg, pglsn.encode_tsn(payload.final_tx_lsn), payload.commit_tx_ts)
        elif isinstance(payload, pgoutput.Insert) and lmsg.new_record:
            pgingest.upsert(
                pg, crud_table_field(lmsg.relation), 'insert', None, lmsg.new_record, column_defs, onconflict)
        elif isinstance(payload, pgoutput.Update) and (lmsg.new_record or lmsg.old_record):
            pgingest.upsert(
                pg, crud_table_field(lmsg.relation), 'update', lmsg.old_record, lmsg.new_record, column_defs,
                onconflict)
        elif isinstance(payload, pgoutput.Delete) and lmsg.old_record:
            pgingest.delete(pg, crud_table_field(lmsg.relation), lmsg.old_record)
        elif isinstance(payload, pgoutput.Truncate):
            tables = [crud_table_field(lmsg.relcache[relid]) for relid in payload.relids]
            pgingest.truncate(pg, tables, cascade=payload.cascade, restartid=payload.restartid)
    logstatus(i, txn, tsn, tts)

def sync(pg_src, pg_dst, slotname, pubname, subname, tablesubs, replication_role='replica', onconflict='do update',
         time_limit=0, seginterval=60 * 10):
    ''' Use logical replication to synchronize from pg_src to pg_dst.

        :slotname: name of the postgresql replication slot to create/use on src server
        :pubname: name of the postgresql publication to create and use
        :subname: name of the postgresql subscription/replication slot
        :tablesubs: an OrderedDict of TableSub instances
        :replication_role: specifies PostgreSQL's session_replication_role setting[1];
            use 'replica' to disable triggers including foreign key constraint validation (requires superuser);
            use 'origin' to enable triggers and foreign key constraint validation
        :seginterval: approximate interval in seconds to size stream segments. This is only a suggestion; there's
            other constraints that make this impossible to enforce precisely (eg transactional atomicity can force
            going for longer).
    '''
    assert onconflict in ('do update', 'do nothing'), 'unexpected value for onconflict: %r' % onconflict
    lock = pg_dst.execute('SELECT pg_try_advisory_lock(-21039576, hashtext(%s));', (subname,)).scalar()
    if not lock:
        slog.info2(f'there is another running process consuming the subscription {subname}')
        raise SystemExit(107)
    start_tsn = pgingest.init_replication_settings(pg_dst, replication_role, subid=subname)
    column_defs = pgingest.get_column_defs(pg_dst)
    pg_dst.commit()  # prevent holding idle txn
    slog.info2('target database at tsn: %s', start_tsn or 'null')
    start = time.time()
    if time_limit and seginterval and time_limit < seginterval:
        seginterval = time_limit
    stream = pgsub.stream_commands(pg_src, slotname, pubname, tablesubs, seginterval)
    for command in stream:
        if isinstance(command, pgsub.Terminate):
            break
        if isinstance(command, pgsub.CopyTableBegin):
            ingress_initcopy(pg_dst, command, stream, start_tsn, column_defs, replication_role, onconflict)
            stream.send(pgsub.StreamCommit(keep_going=not time_limit or time.time() - start < time_limit))
        elif isinstance(command, pgsub.StreamBegin):
            ingress_stream(pg_dst, stream, start_tsn, column_defs, onconflict)
            stream.send(pgsub.StreamCommit(keep_going=not time_limit or time.time() - start < time_limit))
        elif command is None:
            continue
        else:
            raise NotImplementedError(f'unknown command {command!r}')
