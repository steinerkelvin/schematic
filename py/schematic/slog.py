'''
Utility module for logging.

All log functions will log to a log file (default ~/var/schematic/schematic.log).
The functions that end with "2" will also log the message to stderr (ie file descriptor 2) for interactive feedback.

An env var SCM_LOG_LEVEL dictates the log level to write to the log file.
'''
import hashlib
import logging
import os.path
import subprocess
import sys
from subprocess import PIPE

from schematic import env

__all__ = ('debug', 'debug2', 'info', 'info2', 'warn', 'warn2', 'error', 'error2', 'critical', 'critical2')

class Slogger(logging.Logger):

    def findCaller(self, stack_info=False, stacklevel=1):
        ''' Look a couple more frames backward in the stack to identify the true caller. Because we wrap the funcs. '''
        return super().findCaller(stack_info=stack_info, stacklevel=stacklevel + 2)

def _enable_logfile(_slog, logpath):
    try:
        subprocess.check_call(['mkdir', '-p', os.path.dirname(logpath)])
        subprocess.check_call(['touch', logpath])
    except subprocess.CalledProcessError:  # pylint: disable=broad-except
        print(f'error: unable to write to logfile {logpath}', file=sys.stderr)
        return
    fh = logging.FileHandler(logpath)
    fh.setLevel(logging.INFO)
    formatter = logging.Formatter(
        f'%(asctime)s {get_version()} %(process)s %(pathname)s:%(funcName)s:%(lineno)s %(levelname)-8s %(message)s')
    fh.setFormatter(formatter)
    _slog.addHandler(fh)

def get_log_level():
    log_level = env.get_str('SCM_LOG_LEVEL')
    if log_level not in ('DEBUG', 'INFO', 'WARN', 'ERROR', 'CRITICAL'):
        log_level = 'INFO'
    return getattr(logging, log_level)

def _get_logger(name='schematic'):
    _slog = Slogger(name, level=get_log_level())
    logpath = os.path.join(env.get_str('SCM_VAR', os.path.expanduser('~/var')), 'schematic/', f'{name}.log')
    _enable_logfile(_slog, logpath)
    return _slog

def get_version():
    version = (env.get_str('SCM_VERSION') or '').strip()
    if version:
        return version
    try:
        return subprocess.check_output(
            ['git', 'rev-parse', 'HEAD'], cwd=env.get_str('ROOT_DIR'), stderr=PIPE, text=True).strip()
    except subprocess.CalledProcessError:
        pass
    try:
        sentinelpath = os.path.normpath(os.path.join(__file__, '../scm_cli.py'))
        return '!%s' % hashlib.md5(open(sentinelpath, 'rb').read()).hexdigest()
    except OSError:
        return 'novers'

_logger = _get_logger()

def debug(msg, *args, **kwargs):
    ''' Write debug message to log. '''
    return _logger.debug(msg, *args, **kwargs)

def debug2(msg, *args, **kwargs):
    ''' Write debug message to log and stderr. '''
    print(msg % args, file=sys.stderr)
    return _logger.debug(msg, *args, **kwargs)

def info(msg, *args, **kwargs):
    ''' Write info message to log. '''
    return _logger.info(msg, *args, **kwargs)

def info2(msg, *args, **kwargs):
    ''' Write info message to log and stderr. '''
    print(msg % args, file=sys.stderr)
    return _logger.info(msg, *args, **kwargs)

def warn(msg, *args, **kwargs):
    ''' Write warning message to log. '''
    return _logger.warning(msg, *args, **kwargs)

def warn2(msg, *args, **kwargs):
    ''' Write warning message to log and stderr. '''
    print(msg % args, file=sys.stderr)
    return _logger.warning(msg, *args, **kwargs)

def error(msg, *args, exc_info=False, **kwargs):
    ''' Write error message to log. '''
    return _logger.error(msg, *args, exc_info=exc_info, **kwargs)

def error2(msg, *args, exc_info=False, **kwargs):
    ''' Write error message to log and stderr. '''
    print(msg % args, file=sys.stderr)
    return _logger.error(msg, *args, exc_info=exc_info, **kwargs)

def critical(msg, *args, exc_info=False, **kwargs):
    ''' Write critical message to log. '''
    return _logger.critical(msg, *args, exc_info=exc_info, **kwargs)

def critical2(msg, *args, exc_info=False, **kwargs):
    ''' Write critical message to log and stderr. '''
    print(msg % args, file=sys.stderr)
    return _logger.critical(msg, *args, exc_info=exc_info, **kwargs)
