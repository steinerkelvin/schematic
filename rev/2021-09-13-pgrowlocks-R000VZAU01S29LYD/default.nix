stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAU01S29LYD";
    name = "2021-09-13-pgrowlocks";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
