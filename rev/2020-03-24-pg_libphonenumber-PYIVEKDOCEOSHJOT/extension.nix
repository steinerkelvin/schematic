{ pkgs, stdenv, postgresql, libphonenumber, protobuf, ... }:

stdenv.mkDerivation rec {
    name = "pg_libphonenumber-FURWIAHEDBAYRIJO";
    version = "alpha";
    src = pkgs.fetchgit {
        url = "https://github.com/blm768/pg-libphonenumber.git";
        rev = "5a4a37ce9ad6c236f7aae7c288e9e5b42f226770";
        sha256 = "1i9062vxmx3s6w9lk0lckalhrdb72sx09ix0a55fg2xjaqdiny6n";
    };
    buildInputs = [
        postgresql
        libphonenumber
        protobuf
    ];
    installPhase = ''
        targetdir=$out/basefiles
        install -D pg_libphonenumber.so -t $targetdir/lib/
        install -D pg_libphonenumber.control -t $targetdir/share/postgresql/extension/
        install -D sql/pg_libphonenumber*.sql -t $targetdir/share/postgresql/extension/
    '';
}
