stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S07PMSDRZWJGSHMZ";
    name = "language_code";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-11-16-language_code-R000GHY170DFEF0Z>
    ];
}
