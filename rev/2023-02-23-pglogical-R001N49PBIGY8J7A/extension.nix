{ pkgs, stdenv, openssl, postgresql, zlib, ... }:

stdenv.mkDerivation rec {
    version = "2.4.2";
    name = "pglogical-${version}";
    src = pkgs.fetchFromGitHub {
        owner = "2ndQuadrant";
        repo = "pglogical";
        rev = "REL${builtins.replaceStrings ["."] ["_"] version}";
        url = "https://github.com/2ndQuadrant/pglogical/archive/refs/tags/REL${builtins.replaceStrings ["."] ["_"] version}.tar.gz";
        sha256 = "sha256-a1g5GpgAv/5JCqEM5N4CMruIvlNd6DqRT6OlCC+pq9k=";
    };
    buildInputs = [
        openssl
        postgresql
        zlib
    ];
    installPhase = ''
        targetdir=$out/basefiles
        install -D pglogical.so -t $targetdir/lib/
        install -D pglogical_output.so -t $targetdir/lib/
        install -D pglogical.control -t $targetdir/share/postgresql/extension/
        install -D pglogical--${version}.sql -t $targetdir/share/postgresql/extension/
    '';
}
