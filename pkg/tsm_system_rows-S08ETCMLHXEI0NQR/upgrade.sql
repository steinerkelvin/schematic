CREATE EXTENSION IF NOT EXISTS tsm_system_rows WITH SCHEMA public;
COMMENT ON EXTENSION tsm_system_rows IS 'TABLESAMPLE method which accepts number of rows as a limit';

