stdargs @ { scm, pkgs, ... }:

let
    reverseString = str: with pkgs.lib; concatStrings (reverseList (stringToCharacters str));
    modpath = (/. + (builtins.getEnv "SCM_SANDBOX_MODULE"));
    mod = (import modpath stdargs);
in (scm.database rec {
    guid = reverseString server.guid;
    name = ''${pkgs.lib.maybeEnv "SCM_NAME" "sandbox"}'';
    server = scm.server rec {
        postgresql = if mod.scm_type == "database" then mod.server.postgresql else pkgs.postgresql;
        guid = builtins.getEnv "SCM_GUID";
        name = ''${pkgs.lib.maybeEnv "SCM_NAME" "sandbox"}'';
        dbname = ''${pkgs.lib.maybeEnv "SCM_NAME" "sandbox"}'';
        port = builtins.getEnv "SCM_PORT";
        user = pkgs.lib.maybeEnv "SCM_USER" "root";
        password = pkgs.lib.maybeEnv "SCM_PASS" "pass";
        SCM_ISTEMP = pkgs.lib.maybeEnv "SCM_ISTEMP" true;
        SCM_VERBOSE = pkgs.lib.maybeEnv "SCM_VERBOSE" false;
        SCM_SANDBOX_MODE = pkgs.lib.maybeEnv "SCM_SANDBOX_MODE" "declarative";  # NB: "declarative" populates database using declarative definitions, "imperative" populates database using revisions
    };
    dependencies = if mod.scm_type == "database" then mod.dependencies else [ modpath ];
    preBuildInputs = if mod.scm_type == "database" then mod.preBuildInputs else null;
})
