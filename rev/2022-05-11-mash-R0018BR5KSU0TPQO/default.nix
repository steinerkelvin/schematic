stdargs @ { scm, ... }:

scm.revision {
    guid = "R0018BR5KSU0TPQO";
    name = "2022-05-11-mash";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
