stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAU4SDFND8D";
    name = "2021-09-13-dblink";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
