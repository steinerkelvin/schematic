CREATE OR REPLACE FUNCTION public.validate_mash(x bytea) RETURNS boolean
LANGUAGE sql IMMUTABLE STRICT LEAKPROOF PARALLEL SAFE AS $_$
    SELECT (CASE (get_byte(x, 0), octet_length(x) - 1)
        WHEN (1, 16) THEN true  -- md5
        WHEN (2, 20) THEN true  -- sha-1
        WHEN (3, 28) THEN true  -- sha-256
        WHEN (4, 16) THEN true  -- blake2b-128
        ELSE false END
    );
$_$;

CREATE DOMAIN public.mash AS bytea CONSTRAINT mash_chk CHECK (validate_mash(VALUE));
COMMENT ON DOMAIN public.mash IS 'A "many hash", represents one of several possible hash digests.';

CREATE OR REPLACE FUNCTION public.to_mash(x text) RETURNS mash
LANGUAGE sql IMMUTABLE STRICT LEAKPROOF PARALLEL SAFE AS $_$
    SELECT ((CASE split_part(x, '/', 1)
        WHEN 'md5' THEN 1
        WHEN 'sha-1' THEN 2
        WHEN 'sha-256' THEN 3
        WHEN 'blake2b-128' THEN 4
    END)::"char"::text::bytea || decode(split_part(x, '/', 2), 'hex'))::mash;
$_$;
