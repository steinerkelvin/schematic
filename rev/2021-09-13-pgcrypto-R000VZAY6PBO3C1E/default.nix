stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAY6PBO3C1E";
    name = "2021-09-13-pgcrypto";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
