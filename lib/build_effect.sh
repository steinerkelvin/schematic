set -eu

"$coreutils/bin/mkdir" -p "$out"
cd "$out"
export -p > local.env  # just the env vars for this derivation
source $stdenv/setup
export -p > transitive.env  # the full env vars, including transitive dependencies
cat > scm-effect << EOF
#! $bash/bin/bash -e
set -eu
source "$out/transitive.env"
exec "$SCM_EFFECT_EXEC" "$SCM_EFFECT_PROG"
EOF
chmod +x scm-effect  # a program that can be invoked to apply an effect
