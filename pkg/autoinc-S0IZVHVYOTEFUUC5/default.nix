stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0IZVHVYOTEFUUC5";
    name = "autoinc";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-autoinc-R000VZAY1176BNC8>
    ];
}
