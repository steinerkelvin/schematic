stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAVI37GCQPX";
    name = "2021-09-13-cube";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
