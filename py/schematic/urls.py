import urllib.parse


def modquery(uri, query=None):
    '''
    >>> modquery('postgresql:///db0?host=localhost&port=1234', {'application_name': 'py/schematic/urls.py'})
    'postgresql:///db0?host=localhost&port=1234&application_name=py%2Fschematic%2Furls.py'
    >>> modquery('postgresql:///db0?host=localhost&port=1234&options=-c%20wal_sender_timeout%3D0', {'application_name': 'scm-sync'})
    'postgresql:///db0?host=localhost&port=1234&options=-c%20wal_sender_timeout%3D0&application_name=scm-sync'
    '''
    parsed = urllib.parse.urlparse(uri)
    newquery = urllib.parse.parse_qs(parsed.query, keep_blank_values=True)
    newquery.update(query or {})
    for key, val in query.items():
        if val is None:
            del newquery[key]
    netloc_guid = 'wrutbocikald'  # work-around bug where empty string isn't respected
    parsed = parsed._replace(
        netloc=netloc_guid if parsed.netloc == '' else parsed.netloc,
        query=urllib.parse.urlencode(newquery, doseq=True, quote_via=urllib.parse.quote))
    return urllib.parse.urlunparse(parsed).replace(netloc_guid, '')

def getquery(uri):
    return urllib.parse.parse_qs(urllib.parse.urlparse(uri).query, keep_blank_values=True)
