CREATE SEQUENCE public.country_id_seq;
ALTER TABLE public.country ADD COLUMN id integer NOT NULL DEFAULT nextval('country_id_seq');
CREATE UNIQUE INDEX country_id_idx ON public.country USING btree (id);
