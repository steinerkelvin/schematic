stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000HQQ3VW9NWU28";
    name = "2020-12-10-language";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
