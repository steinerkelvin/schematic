
CREATE TABLE public.language_code (
    id integer NOT NULL,
    language_family character varying,
    language_name character varying NOT NULL,
    native_name character varying,
    iso_639_1 character varying NOT NULL,
    iso_639_2_t character varying,
    iso_639_2_b character varying,
    iso_639_3 character varying,
    iso_639_6 character varying,
    notes character varying,
    iso_639_1_upper text
);
ALTER TABLE ONLY public.language_code ADD CONSTRAINT language_code_iso_639_1_upper_uniq UNIQUE (iso_639_1_upper);
ALTER TABLE ONLY public.language_code ADD CONSTRAINT language_code_pkey PRIMARY KEY (id);
