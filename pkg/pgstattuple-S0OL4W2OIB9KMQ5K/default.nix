stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0OL4W2OIB9KMQ5K";
    name = "pgstattuple";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-pgstattuple-R000VZ9YKP9OGCZK>
    ];
}
