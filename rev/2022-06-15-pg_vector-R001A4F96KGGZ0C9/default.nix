stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R001A4F96KGGZ0C9";
    name = "2022-06-15-pg_vector";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-10-08-pg_vector-R000X9SJZ87QYCEE>
    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
