stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAYZVPKU3L7";
    name = "2021-09-13-pg_prewarm";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
