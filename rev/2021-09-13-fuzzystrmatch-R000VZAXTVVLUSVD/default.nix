stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAXTVVLUSVD";
    name = "2021-09-13-fuzzystrmatch";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
