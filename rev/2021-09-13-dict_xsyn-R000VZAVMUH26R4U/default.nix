stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAVMUH26R4U";
    name = "2021-09-13-dict_xsyn";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
