stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAVDPM02V78";
    name = "2021-09-13-hstore";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
