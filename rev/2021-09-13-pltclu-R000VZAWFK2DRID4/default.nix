stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAWFK2DRID4";
    name = "2021-09-13-pltclu";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
