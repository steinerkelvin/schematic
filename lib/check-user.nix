stdargs @ { scm, pkgs, stdenv, python3, ... }:
{ server, username, super ? false }:

scm.effect rec {
    inherit (server) postgresql pguri;
    inherit username super;
    name = "checkUser-${username}";
    SCM_EFFECT_PROG = ../py/schematic/check_user.py;
}
