import itertools


def cmp(x, y):
    return (x > y) - (x < y)

def groups(iterable, key=None):
    return itertools.groupby(sorted(iterable, key=key), key)

def batches(generator, batch_size):
    gen = iter(generator)
    make_chunk = lambda: list(itertools.islice(gen, batch_size))
    return iter(make_chunk, [])
