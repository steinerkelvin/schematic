stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000GHY1HT1XZ13U";
    name = "2020-11-16-map_hashtext";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
