stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0YZ4XJNI5UYMEXF";
    name = "logic.equals";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <logic-N0BUZPS77QZ95D7B>
        <2021-09-07-logic.equals-R000VN4SLCHCN76W>
    ];
}
