CREATE FUNCTION rand.immutable_random(integer) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT random();$$;
CREATE FUNCTION rand.immutable_random(text) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT random();$$;
