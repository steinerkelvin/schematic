CREATE OR REPLACE FUNCTION rand.immutable_random(anyelement) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT random();$$;
CREATE OR REPLACE FUNCTION rand.immutable_random(integer) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT random();$$;
CREATE OR REPLACE FUNCTION rand.immutable_random(bigint) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT random();$$;
CREATE OR REPLACE FUNCTION rand.immutable_random(text) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT random();$$;
