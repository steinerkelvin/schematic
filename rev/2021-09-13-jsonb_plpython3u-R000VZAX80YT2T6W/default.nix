stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAX80YT2T6W";
    name = "2021-09-13-jsonb_plpython3u";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-plpython3u-R000VZDMTW1CCIL1>
    ];
}
