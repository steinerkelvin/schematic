stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0VDJYD83D69XEU4";
    name = "pg_vector";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2022-06-15-pg_vector-R001A4F96KGGZ0C9>
    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
