import os
import tempfile
import codecs

import lz4.frame
import sh
from psycopg2.sql import SQL, Composed

from schematic import env


def copy_to(pg, query, outfp, **flags):
    ''' Use PostgreSQL COPY command to export a query to output file.
        https://www.postgresql.org/docs/current/sql-copy.html

    Example:
        with pgcn.connect(...) as pg, open('/path/to/file.csv', 'w') as fp:
            copy_to(session.query(MyModel), fp, engine, format='csv', null='.')

    :param query: string query, or psycopg2.sql.Composable
    :param outfp: output file
    :param **flags: params passed to COPY
    '''
    try:
        cur = pg.conn.cursor()
        copyexp = Composed([SQL(f'COPY ( {query} ) TO STDOUT '), SQL(format_flags(flags) if flags else '')])
        cur.copy_expert(copyexp, outfp)
    finally:
        cur.close() if cur else None

def format_flags(flags):
    return SQL(', ').join([f'{key.upper()} {format_flag(value)}' for key, value in flags.items()])

def format_flag(value):
    if isinstance(value, bool):
        return {True: 'TRUE', False: 'FALSE'}[value]
    return repr(value)

def decode_value(text):
    r'''
    >>> decode_value('\\N')
    >>> decode_value('hello\\tgoodbye')
    'hello\tgoodbye'
    '''
    if isinstance(text, str):  # codecs.escape_decode expects bytes
        text = text.encode('utf8')
    if text == rb'\N':
        return
    (ret, _) = codecs.escape_decode(text)
    return ret.decode('utf8')

def decode_record(line):
    r'''
    >>> decode_record(b'123\tfour\n')
    ('123', 'four')
    >>> decode_record(b'hello\tgoodbye\n')
    ('hello', 'goodbye')
    '''
    return tuple(map(decode_value, line[:-1].split(b'\t')))

def read_via_fs(pg, query, tmpdir=None, **flags):
    ''' COPY query output to local filesystem, then yield lines. '''
    tmpdir = tmpdir or env.get_str('SCM_TMPDIR') or os.path.expandvars('$HOME/var/tmp')
    sh.mkdir('-p', tmpdir)
    with tempfile.NamedTemporaryFile(dir=tmpdir) as tmpfp:
        lztmpfp = lz4.frame.open(tmpfp.name, mode='wb')
        copy_to(pg, query, lztmpfp, **flags)
        lztmpfp.close()
        yield from lz4.frame.open(tmpfp.name, mode='rb')

def read_tuple_batch(pg, query, tmpdir=None, **flags):
    ''' Yield tuples from a postgresql copy statement. Saves to temp file first, then reads from disk. '''
    yield from (decode_record(line) for line in read_via_fs(pg, query, tmpdir=tmpdir, **flags))
