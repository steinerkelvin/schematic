To create a user, you may follow an approach similar to creating schemas for tables:
* Create a schema named `user_xyz` , where xyz is the name of new user
    ```
    scm schema user_xyz
    ```
* Use SQL commands to initialize the user in `upgrade.sql`, for example:
    ```
    CREATE USER xyz WITH ENCRYPTED PASSWORD 'xyz';
    REVOKE ALL ON SCHEMA public FROM xyz;
    GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO xyz;
    GRANT SELECT ON ALL TABLES IN SCHEMA public TO xyz;
    ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT USAGE, SELECT ON SEQUENCES TO xyz;
    ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO xyz;
    ```
* Create the initial revision
    ```
    scm revision pkg/user_xyz*
    ```
* Any subsequent changes to the role may be made similarly to table schemas, by creating a new revision
