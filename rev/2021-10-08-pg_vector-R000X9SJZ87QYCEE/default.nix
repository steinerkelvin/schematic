stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000X9SJZ87QYCEE";
    name = "2021-10-08-pg_vector";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
