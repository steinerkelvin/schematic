{ pkgs, stdenv, postgresql, ... }:

stdenv.mkDerivation rec {
    name = "vector-${version}";
    version = "0.2.0";
    src = pkgs.fetchurl {
        url = "https://github.com/ankane/pgvector/archive/refs/tags/v${version}.tar.gz";
        sha256 = "0pnw94nkappwh3hxaa3fcp3yk8yv67hyhnh1v7gb3b865zm2dn8y";
    };
    installPhase = ''
        ls -lah
        targetdir=$out/basefiles
        install -D vector.so -t $targetdir/lib/
        install -D vector.control -t $targetdir/share/postgresql/extension/
        install -D sql/vector--${version}.sql -t $targetdir/share/postgresql/extension/
    '';
    buildInputs = [
        postgresql
    ];
}
