CREATE DOMAIN unixtime AS integer;

CREATE OR REPLACE FUNCTION timestamp_to_unixtime(ts timestamp) RETURNS unixtime AS $$
    SELECT EXTRACT (EPOCH FROM ts)::unixtime;
$$ LANGUAGE SQL STRICT IMMUTABLE;
COMMENT ON FUNCTION timestamp_to_unixtime(ts timestamp) IS 'Converts a timestap without time zone to unixtime.';

CREATE OR REPLACE FUNCTION timestamptz_to_unixtime(ts timestamp with time zone) RETURNS unixtime AS $$
    SELECT EXTRACT (EPOCH FROM ts)::unixtime;
$$ LANGUAGE SQL STRICT IMMUTABLE;
COMMENT ON FUNCTION timestamptz_to_unixtime(ts timestamp with time zone) IS 'Converts a timestap with time zone to unixtime.';

CREATE OR REPLACE FUNCTION unixtime_to_timestamp(ts integer) RETURNS timestamp AS $$
    SELECT timestamp 'epoch' + (ts::text || ' seconds')::interval;
$$ LANGUAGE SQL STRICT IMMUTABLE;
COMMENT ON FUNCTION unixtime_to_timestamp(ts integer) IS 'Converts a unixtime to a timestap without time zone.';

CREATE OR REPLACE FUNCTION unixtime_to_timestamptz(ts integer) RETURNS timestamptz AS $$
    SELECT timezone('utc', timestamp 'epoch') + (ts::text || ' seconds')::interval;
$$ LANGUAGE SQL STRICT IMMUTABLE;
COMMENT ON FUNCTION unixtime_to_timestamptz(ts integer) IS 'Converts a unixtime to a timestap with time zone.';

CREATE CAST (integer AS timestamp)
WITH FUNCTION unixtime_to_timestamp(integer)
AS ASSIGNMENT;

CREATE CAST (timestamp AS integer)
WITH FUNCTION timestamp_to_unixtime(timestamp)
AS ASSIGNMENT;

CREATE CAST (integer AS timestamptz)
WITH FUNCTION unixtime_to_timestamptz(integer)
AS ASSIGNMENT;

CREATE CAST (timestamptz AS integer)
WITH FUNCTION timestamptz_to_unixtime(timestamptz)
AS ASSIGNMENT;
