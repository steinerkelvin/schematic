from schematic import env, pgcn
from schematic.build_util import install_buildinput_files, apply_sql_file

def main():
    name = env.get_str('name')  # pylint: disable=unused-variable
    sandbox_mode = env.get_str('SCM_SANDBOX_MODE', 'imperative')
    autocommit = env.get_bool('autocommit')
    if sandbox_mode == 'declarative':
        # creating an "declarative" sandbox database from declarative definitions (instead of revisions)
        install_buildinput_files()
        upgrade_sql = env.get_str('upgrade_sql')
        with pgcn.connect(env.get_str('pguri'), autocommit=autocommit) as pg:
            apply_sql_file(pg, upgrade_sql)
            pg.commit()
    elif sandbox_mode == 'imperative':
        # creating a "imperative" sandbox database from revisions
        pass

if __name__ == '__main__':
    main()
