stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZDMTW1CCIL1";
    name = "2021-09-13-plpython3u";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
