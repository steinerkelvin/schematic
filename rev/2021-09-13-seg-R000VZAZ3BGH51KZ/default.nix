stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAZ3BGH51KZ";
    name = "2021-09-13-seg";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
