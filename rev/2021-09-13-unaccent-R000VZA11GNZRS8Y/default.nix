stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZA11GNZRS8Y";
    name = "2021-09-13-unaccent";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
