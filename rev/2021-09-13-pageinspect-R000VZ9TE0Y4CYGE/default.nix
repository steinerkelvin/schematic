stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZ9TE0Y4CYGE";
    name = "2021-09-13-pageinspect";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
