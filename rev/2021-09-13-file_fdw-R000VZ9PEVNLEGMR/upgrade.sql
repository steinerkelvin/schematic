CREATE EXTENSION IF NOT EXISTS file_fdw WITH SCHEMA public;
COMMENT ON EXTENSION file_fdw IS 'foreign-data wrapper for flat file access';
