alias l='ls -CF'
alias la='ls -A'
alias ll='ls -alF'
alias ls='ls --color=auto'
alias sort='LC_ALL=C sort'
alias git='PAGER=less LESS="-RiMSx4 -FX" git'
alias grp='LC_ALL=C grep -P --color=always'
alias man='PAGER=less LESS="-RiMSx4" man'
alias isort="isort --settings-path $ROOT_DIR/etc/.isort.cfg"
alias nose="nosetests --config $ROOT_DIR/etc/nose/nose.cfg"
alias lint='pylint --rcfile=$ROOT_DIR/etc/pylint/default.rc'
alias randname='python $ROOT_DIR/py/schematic/guids.py'
[ -f ~/.bash_aliases ] && source ~/.bash_aliases
unset command_not_found_handle  # looks for a command when not found in path; slow
