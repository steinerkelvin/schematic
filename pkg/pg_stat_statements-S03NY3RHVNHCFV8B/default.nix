stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S03NY3RHVNHCFV8B";
    name = "pg_stat_statements";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-pg_stat_statements-R000VZ9WFKKWH5XY>
    ];
}
