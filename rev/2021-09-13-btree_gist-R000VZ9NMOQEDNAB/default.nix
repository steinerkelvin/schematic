stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZ9NMOQEDNAB";
    name = "2021-09-13-btree_gist";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
