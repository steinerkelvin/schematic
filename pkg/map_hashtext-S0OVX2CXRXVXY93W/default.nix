stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0OVX2CXRXVXY93W";
    name = "map_hashtext";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-11-16-map_hashtext-R000GHY1HT1XZ13U>
    ];
}
