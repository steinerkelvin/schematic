stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0CHPCUEJEZ2N5FS";
    name = "xml2";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-xml2-R000VZAW6PJNFYH0>
    ];
}
