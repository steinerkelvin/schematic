stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0BDWOJB5JQSHO74";
    name = "pltclu";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-pltclu-R000VZAWFK2DRID4>
    ];
}
