stdargs @ { scm, ... }:

scm.revision {
    guid = "R001IGJQ8A0Z56UF";
    name = "2022-11-24-logic.xor";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <logic-N0BUZPS77QZ95D7B>
    ];
}
