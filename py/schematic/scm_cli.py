#! /usr/bin/env python3
'''Schematic - PostgreSQL schema control manager.'''

import codecs
import datetime
import fcntl
import glob
import io
import json
import multiprocessing
import os
import re
import shutil
import signal
import socket
import stat
import struct
import subprocess
import sys
import termios
import traceback
from collections import OrderedDict, defaultdict

import click
import fabric
import invoke
import migra
import pexpect
import pglast
import psutil
import psycopg2
import sqlbag
from clint.textui import colored
from toolz import dicttoolz

from schematic import dates, env, guids, oplog_ingress, pgcn, pgsync, pgyaml, slog
from schematic.build_revision import revision_exists
from schematic.build_util import ensure_pg_running, mkdir, pg_ctl_fn, pg_is_ready
from schematic.pguri import pguri_from_basedir


@click.group()
def main():
    pass

nix_store_path = os.environ.get('NIX_STORE', '/nix/store/')
scm_var_path = env.get_str('SCM_VAR', os.path.expanduser('~/var'))

def parse_derivation_line(line, prefix=nix_store_path):
    ''' Parse output of nix shell commands referencing a .drv output.

    >>> parse_derivation_line('  /nix/store/i96g1m88fi3xhara7q9n86fp3f0l1i0d-country.drv')
    '/nix/store/i96g1m88fi3xhara7q9n86fp3f0l1i0d-country.drv'
    >>> parse_derivation_line('/nix/store/i96g1m88fi3xhara7q9n86fp3f0l1i0d-country.drv\\n')
    '/nix/store/i96g1m88fi3xhara7q9n86fp3f0l1i0d-country.drv'
    >>> parse_derivation_line(' blah')
    >>> parse_derivation_line(None)
    '''
    match = re.match(r'\s*(?P<drv>[^\s]+\.drv)\s*', line) if line else None
    drv = match.group('drv') if match else ''
    return drv if drv.startswith(prefix) else None

def get_nix_version():
    cmd = ['nix', '--version']
    response = subprocess.check_output(cmd)
    version = re.search(r'(?P<version>[0-9\.]+)', str(response)).group('version')
    return version

def parse_drv(drv, recursive=False):
    ''' Parse a nix .drv file using 'nix show derivation'. '''
    cmd = ['nix', 'show-derivation', drv]
    nix_version = get_nix_version()
    if nix_version >= '2.4':
        cmd += ['--extra-experimental-features', 'nix-command']
    if recursive:
        cmd += ['-r']
    ret = json.loads(subprocess.check_output(cmd))
    for depdrv, depobj in ret.items():
        depobj['drv_path'] = depdrv
    return ret

def clear_environment():
    ''' Delete all environmental variables. '''
    for key in os.environ:
        del os.environ[key]

def same_python_exe(exe, this_exe=sys.executable):
    ''' Check if the argument refers to the same python executable.
    >>> same_python_exe('/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8', '/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3')
    True
    >>> same_python_exe('/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8', '/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8')
    True
    >>> same_python_exe('/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8', '/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8')
    True
    >>> same_python_exe('/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8', '/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python')
    True
    >>> same_python_exe('/nix/store/99999999999999l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python3.8', '/nix/store/0lwl5nihn8qj62l5j2rlmq56glyq4djb-python3-3.8.1-env/bin/python')
    False
    '''
    if not exe:
        return
    exe = os.path.realpath(exe)
    this_exe = os.path.realpath(this_exe)
    if exe == this_exe:
        return True
    if exe.startswith(nix_store_path) and this_exe.startswith(nix_store_path):
        if exe.startswith(this_exe) or this_exe.startswith(exe):
            return True
    return False

def unquote_bash(text):
    r'''
    >>> unquote_bash('hello \\"world\\"')
    'hello "world"'
    >>> unquote_bash('hello \\nworld')
    'hello \nworld'
    '''
    (ret, _) = codecs.escape_decode(text)
    return ret.decode('utf8')

def parse_shell_env_line(line):
    ''' Parse a shell line generated from `export -p'.
    >>> parse_shell_env_line('declare -x STRINGS="strings"')
    ('STRINGS', 'strings')
    >>> parse_shell_env_line('')
    (None, None)
    >>> parse_shell_env_line('declare -x rules="[{\"addr\":\"127.0.0.0/24\",\"auth\":\"md5\"}]"')
    ('rules', '[{"addr":"127.0.0.0/24","auth":"md5"}]')
    '''
    if match := re.match(r'declare (\-x )(?P<key>[a-zA-Z_]+[a-zA-Z0-9_]*)="(?P<value>.*)"', (line or '').strip()):
        return match.group('key'), unquote_bash(match.group('value'))
    return (None, None)

def parse_shell_env(path):
    ''' Parse ouput of shell 'export -p'. '''
    ret = {}
    for line in open(path):
        (key, value) = parse_shell_env_line(line)
        if key:
            ret[key] = value
    return ret

def apply_effect(drv_obj, build_guid, environ=None, verbose=False):
    ''' Execute a schematic effect. Expects to be called in a subprocess since it mutates interpreter state. '''
    drv_path = drv_obj['drv_path']
    env = drv_obj['env']
    effect_exec = env.get('SCM_EFFECT_EXEC')
    effect_prog = env.get('SCM_EFFECT_PROG')
    def write_debug_logs(stdout, stderr):
        build_dir = os.path.join(scm_var_path, '.scm-builds', build_guid)
        mkdir(build_dir)
        open(os.path.join(build_dir, 'stdout.log'), 'w').write(stdout) if stdout.strip() else None
        open(os.path.join(build_dir, 'stderr.log'), 'w').write(stderr) if stderr.strip() else None
        stacktrace = traceback.format_exc()
        open(os.path.join(build_dir, 'stacktrace.log'), 'w').write(stacktrace) if stacktrace.strip() else None
    if same_python_exe(effect_exec):  # optimization: save python startup cost by using this process
        sys.argv[:] = [effect_exec, effect_prog]
        clear_environment()
        os.environ.update(environ) if environ else None
        os.environ.update(parse_shell_env(os.path.join(env['out'], 'transitive.env')))
        old_stderr, old_stdout = sys.stderr, sys.stdout
        sys.stderr, sys.stdout = io.StringIO(), io.StringIO()
        import imp  # pylint: disable=import-outside-toplevel
        effect_mod = imp.load_source('effect_prog', effect_prog)
        try:
            effect_mod.main()
            if verbose:
                sys.stderr.seek(0)
                sys.stdout.seek(0)
                old_stderr.write(sys.stderr.read())
                old_stdout.write(sys.stdout.read())
        except BaseException:
            sys.stderr.seek(0)
            sys.stdout.seek(0)
            write_debug_logs(sys.stdout.read(), sys.stderr.read())
            raise
    else:
        scm_effect = os.path.join(drv_obj['outputs']['out']['path'], 'scm-effect')
        cmd = [scm_effect]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True, env=environ)
        (stdout, stderr) = p.communicate()
        if p.returncode != 0:
            write_debug_logs(stdout, stderr)
            raise subprocess.CalledProcessError(p.returncode, cmd, output=stdout, stderr=stderr)
        if verbose:
            sys.stderr.write(stderr) if stderr else None
            sys.stdout.write(stdout) if stdout else None

def instantiate_module(nix_module, environ=None, verbose=False):
    ''' Build a schematic object. '''
    parallelism = env.get_str('SCM_PARALLELISM', 'auto')
    cmd = [
        'nix-instantiate', get_relative_nix_mod('lib/eval.nix'), '--arg', 'modulepath', nix_module,
        '--show-trace', '--max-jobs', parallelism]
    build_environ = dicttoolz.merge(os.environ, environ or {})
    drv_paths = []
    lines = []
    with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True, env=build_environ) as p:
        while True:
            line = p.stdout.readline()
            lines.append(line)
            if not line:
                break
            if verbose and not line.startswith('''warning: you did not specify '--add-root';'''):
                print(line, file=sys.stderr, end='')
            drv_path = parse_derivation_line(line)
            if drv_path:
                drv_paths.append(drv_path)
        p.wait()
        if p.returncode != 0:
            print('Error constructing %s' % (environ.get('SCM_SANDBOX_MODULE') or nix_module), file=sys.stderr)
            print(''.join(lines), file=sys.stderr)
            raise subprocess.CalledProcessError(p.returncode, cmd)
    assert len(drv_paths) == 1, 'expected exactly one drv path: %s' % drv_paths
    return drv_paths[0]

def get_applied_revision_state(basedir, port, pguri):
    ''' Return a set of revision guids that have been applied against a database and flag indicating binary replica. '''
    if pg_is_ready(basedir, port):
        with pgcn.connect(pguri) as pg:
            guids = set(
                pg.execute('SELECT array_agg(guid) FROM meta.revision;').scalar()) if revision_exists(pg) else None
            is_replica = pg.execute('SELECT pg_is_in_recovery();').scalar()
            return guids, is_replica
    return None, None

def get_max_effect_procs(parallelism=env.get_str('SCM_PARALLELISM', 'auto'), cpu_count=None):
    ''' Get number of processes to use for applying effects.
    >>> get_max_effect_procs(parallelism=1)
    1
    >>> get_max_effect_procs(parallelism=7)
    7
    >>> get_max_effect_procs(parallelism=0, cpu_count=5)
    10
    >>> get_max_effect_procs(parallelism='auto', cpu_count=5)
    10
    >>> get_max_effect_procs(parallelism='', cpu_count=5)
    10
    '''
    if isinstance(parallelism, int) and parallelism > 0:
        return parallelism
    cpu_count = cpu_count or multiprocessing.cpu_count()
    return cpu_count * 2

class EvalException(Exception):

    def __init__(self, proc, exitcode, main_drv, main_obj):
        super().__init__()
        self.proc = proc
        self.exitcode = exitcode
        self.main_drv = main_drv
        self.main_obj = main_obj

def debug_effect_error(main_drv, main_obj, effect_obj, proc):
    ''' Debug output for an error generated from an effect. '''
    effect_drv = effect_obj['drv_path']
    procenv = effect_obj['env']
    print('Error: Process %d exited with code %d' % (proc.pid, proc.exitcode), file=sys.stderr)
    print('    drv: %s' % effect_drv, file=sys.stderr)
    for key in ('scm_type', 'guid', 'name', 'upgrade_sql', 'out'):
        if procenv.get(key):
            print('    %s: %s' % (key, procenv[key]), file=sys.stderr)
    if procenv['scm_type'] == 'tablespace' and proc and proc.exitcode == 59:
        print('    hint: Use --tablespaces=0 to skip tablespaces for testing', file=sys.stderr)
    build_guid = effect_obj.get('_build_guid')
    if build_guid:
        build_dir = os.path.join(scm_var_path, '.scm-builds', build_guid)
        def print_log_file(name, label, divider='-' * 80):
            logpath = os.path.join(build_dir, name)
            if os.path.exists(logpath):
                content = open(logpath).read().strip()
                print('    %s: %s\n%s\n%s\n%s' % (label, logpath, divider, content, divider), file=sys.stderr)
        print_log_file('stdout.log', 'stdout')
        print_log_file('stderr.log', 'stderr')
        print_log_file('stacktrace.log', 'stacktrace')
    raise EvalException(proc, proc.exitcode, main_drv, main_obj)

def eval_module(path, environ=None, verbose=False):
    ''' Build a schematic object.
        First, we execute a pure nix build. This builds system dependencies, and the effect scripts.
        Next, we build a dependency DAG of all IO effects to apply, and then apply them.
    '''
    nix_module = get_nix_module(path)
    if not nix_module:
        print('invalid nix module: %s' % path, file=sys.stderr)
        sys.exit(2)
    main_drv = instantiate_module(nix_module, environ=environ, verbose=verbose)
    parallelism = env.get_str('SCM_PARALLELISM', 'auto')
    cmd = [
        'nix-store', '--realise', main_drv, '--max-jobs', parallelism,
        '--option', 'sandbox', 'false']  # sandbox off isn't necessary, but improves performance up to ~70%
    build_environ = dicttoolz.merge(os.environ, environ) if environ else None
    with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True, env=build_environ) as p:
        while True:
            line = p.stdout.readline()
            if not line:
                break
            if verbose:
                print(line, file=sys.stderr, end='')
        p.wait()
        if p.returncode != 0:
            raise subprocess.CalledProcessError(p.returncode, cmd)
    all_deps = parse_drv(main_drv, recursive=True)
    main_obj = all_deps[main_drv]
    pguri = main_obj['env']['pguri']
    basedir = main_obj['env']['basedir']
    port = main_obj['env']['port']
    applied_revs, is_replica = get_applied_revision_state(basedir, port, pguri) or set()
    effect_deps = {}
    dependees = defaultdict(lambda: {})
    for effect_drv, effect_obj in all_deps.items():
        if 'SCM_EFFECT_EXEC' not in effect_obj['env']:
            continue
        effect_deps[effect_drv] = effect_obj
    for effect_drv, effect_obj in effect_deps.items():
        effect_obj['_dependencies'] = {drv: effect_deps[drv] for drv in effect_obj['inputDrvs'] if drv in effect_deps}
        for dependency_drv in effect_obj['_dependencies']:
            dependees[dependency_drv][effect_drv] = effect_obj
    max_procs = get_max_effect_procs()
    procs_active = OrderedDict()
    procs_scheduled = OrderedDict()
    procs_done = OrderedDict()
    def spawn_proc(effect_obj):
        effect_drv = effect_obj['drv_path']
        effect_obj['_build_guid'] = build_guid = guids.get_build_guid()
        proc = multiprocessing.Process(
            target=apply_effect, name=effect_drv,
            args=(effect_obj, build_guid), kwargs=dict(environ=environ, verbose=verbose))
        proc.start()
        effect_obj['_proc'] = proc
        procs_active[effect_drv] = effect_obj
    def pop_scheduled():
        ''' Move tasks from scheduled to active. '''
        if procs_scheduled and len(procs_active) < max_procs:
            for drv, obj in list(procs_scheduled.items())[:max_procs - len(procs_active)]:
                del procs_scheduled[drv]
                if obj['env'].get('guid') in (applied_revs or []):  # optimization: already executed, don't bother
                    # .. but not if basefiles exist, fixes missing basedir files on replicas
                    if is_replica and obj['env'].get('basefiles'):
                        procs_done[drv] = obj
                        continue
                spawn_proc(obj)
    def schedule_proc(effect_obj):
        drv = effect_obj['drv_path']
        if drv not in procs_done and drv not in procs_active and drv not in procs_scheduled:
            procs_scheduled[drv] = effect_obj
    for effect_obj in (v for v in effect_deps.values() if not v['_dependencies']):
        schedule_proc(effect_obj)
    while procs_active or procs_scheduled:
        pop_scheduled()
        for effect_drv, effect_obj in list(procs_active.items()):
            proc = effect_obj['_proc']
            proc.join(0.01)   # alternatively: multiprocessing.connection.wait([proc.sentinel])
            if not proc.is_alive():
                if proc.exitcode != 0:
                    for term_obj in list(procs_active.values()):  # sigkill siblings to prevent noisy error output
                        term_obj['_proc'].terminate() if term_obj['_proc'].exitcode is None else None
                    debug_effect_error(main_drv, main_obj, effect_obj, proc)
                    raise EvalException(proc, proc.exitcode, main_drv, main_obj)
                del procs_active[effect_drv]
                proc.close()
                procs_done[effect_drv] = effect_obj
                for dependee_obj in dependees[effect_drv].values():  # schedule the unlocked dependees
                    dependee_deps = dependee_obj['_dependencies']
                    if effect_drv in dependee_deps:
                        del dependee_deps[effect_drv]
                        if not dependee_deps:
                            schedule_proc(dependee_obj)
                pop_scheduled()
    return main_obj

def autocomplete_path(ctx, args, incomplete):  # pylint: disable=unused-argument
    completions = glob.glob('%s*' % incomplete)
    if len(completions) == 1 and os.path.isdir(completions[0]):
        completions = glob.glob('%s/*' % completions[0])
    return completions

def pretty_basedir(basedir):
    if os.environ.get('HOME'):
        return '~/%s' % os.path.relpath(basedir, os.environ['HOME'])
    return basedir

def maybe_open_psql(basedir, no_psql):
    ''' Open psql on basedir, or print the command for user to do so. '''
    if no_psql:
        print('open psql shell: scm psql %s' % pretty_basedir(basedir), file=sys.stderr)
    elif sys.stderr.isatty():
        psql_from_basedir(basedir)

@main.command()
@click.argument('path', shell_complete=autocomplete_path)
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
@click.option('-n', '--no-psql', is_flag=True, help='Do not automatically open psql.')
@click.option('--tablespaces', envvar='SCM_INSTALL_TABLESPACES', type=click.Choice(['0', '1']), default='1', help=(
    'Install symlinks for tablespaces; turn off to avoid errors related to missing directories or permissions.'))
def upgrade(path, tablespaces, no_psql, verbose=False):
    '''Create a database and its dependencies or upgrade to latest revision.'''
    drvobj = eval_module(path, verbose=verbose, environ={'SCM_INSTALL_TABLESPACES': str(tablespaces)})
    maybe_open_psql(drvobj['env']['basedir'], no_psql)

def get_relative_nix_mod(path, scm_path=env.get_str('SCM_PATH')):
    return os.path.join(scm_path, path)

def _sandbox(path, verbose=False, scm_sandbox_mode='declarative', scm_install_tablespaces=0):
    '''
    Create a temporary sandbox database given a schema, database, or sql file.

    :scm_sandbox_mode: options ('declarative', 'imperative')
        declarative: build using the declarative specifications
        imperative: build using incremental revisions
    '''
    environ = {
        'SCM_PORT': env.get_str('SCM_PORT') or str(get_free_port()),
        'SCM_GUID': env.get_str('SCM_GUID') or guids.get_server_guid(),
        'SCM_SANDBOX_MODE': scm_sandbox_mode,
        'SCM_INSTALL_TABLESPACES': str(scm_install_tablespaces),
        'SCM_VERBOSE': str(verbose)
    }
    nix_module = get_nix_module(path)
    sql_file = get_sql_file(path)
    if sql_file and not nix_module:
        environ['SCM_SANDBOX_SQL'] = os.path.abspath(sql_file)
        nix_module = get_relative_nix_mod('lib/schema-from-sql.nix')
    if not nix_module:
        print('invalid nix module: %s' % path, file=sys.stderr)
        sys.exit(1)
    if not (environ['SCM_GUID'].startswith('S1') and len(environ['SCM_GUID']) == 16):
        print('invalid SCM_GUID: %s it should start with S1 and have length 16' % environ['SCM_GUID'], file=sys.stderr)
        sys.exit()
    environ['SCM_SANDBOX_MODULE'] = os.path.abspath(nix_module)
    return eval_module(get_relative_nix_mod('lib/sandbox.nix'), environ=environ, verbose=verbose)

def head(iterable):
    for item in iterable:
        return item

def prompt_yesno(prompt, default=False):
    fmt = '%s (y/N) ' if not default else '%s (Y/n) '
    sys.stdout.write(fmt % prompt)
    sys.stdout.flush()
    answer = sys.stdin.readline().strip().lower()
    return {'y': True, 'n': False}.get(answer, default)

@main.command()
@click.argument('path', shell_complete=autocomplete_path)
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
@click.option('-n', '--no-psql', is_flag=True, help='Do not automatically open psql.')
@click.option('-k', '--kill', is_flag=True, help='Delete database when done.')
@click.option('-m', '--mode', type=click.Choice(['declarative', 'imperative']), default='declarative')
@click.option('--tablespaces', envvar='SCM_INSTALL_TABLESPACES', type=click.Choice(['0', '1']), default='0', help=(
    'Install symlinks for tablespaces; defaults off to avoid errors related to missing directories or permissions.'))
def sandbox(path, mode, tablespaces, no_psql, kill, verbose=False):
    '''Create a temporary sandbox database given a schema/database/sql file.'''
    try:
        drvobj = _sandbox(path, scm_sandbox_mode=mode, verbose=verbose, scm_install_tablespaces=tablespaces)
    except EvalException as ex:
        print('error: failed to create sandbox database', file=sys.stderr)
        print('process %s failed with code %s' % (ex.proc.name, ex.exitcode), file=sys.stderr)
        if ex.main_obj and ex.main_obj.get('env') and ex.main_obj['env'].get('basedir'):
            _gc_basedir(ex.main_obj['env']['basedir'], stop=True, force=True, verbose=verbose)
        sys.exit(1)
    if not drvobj or 'env' not in drvobj or 'basedir' not in drvobj['env']:
        sys.exit(2)
    basedir = drvobj['env']['basedir']
    maybe_open_psql(basedir, no_psql)
    if kill:
        _gc_basedir(basedir, stop=True, verbose=verbose)
        sys.exit(0)
    if not no_psql:
        consent = prompt_yesno('remove sandbox database %s?' % unexpand_user(basedir), default=True)
        if not consent:
            sys.exit(0)
        _gc_basedir(basedir, stop=True, verbose=verbose)
    else:
        print('remove database: scm gc-basedir -s %s' % pretty_basedir(basedir), file=sys.stderr)

def _inspect(path, scm_sandbox_mode='declarative', include_all=False):
    nix_module = get_nix_module(path)
    if not nix_module:
        print('invalid nix module: %s' % path, file=sys.stderr)
        sys.exit(2)
    environ = {
        'SCM_SANDBOX_MODULE': os.path.abspath(nix_module),
        'SCM_SANDBOX_MODE': scm_sandbox_mode,
    }
    main_drv = instantiate_module(get_relative_nix_mod('lib/sandbox.nix'), environ=environ)
    all_drvs = parse_drv(main_drv, recursive=True)
    if not include_all:
        all_drvs = {drv: obj for drv, obj in all_drvs.items() if obj['env'].get('scm_type')}
    return {'meta': {'drv_path': main_drv, 'drv_obj': all_drvs[main_drv]}, 'dependencies': all_drvs}

@main.command()
@click.argument('path', shell_complete=autocomplete_path)
@click.option('-m', '--mode', type=click.Choice(['declarative', 'imperative']), default='declarative')
@click.option('-a', '--all', is_flag=True, help='All dependencies, including pure software.')
def inspect(path, mode, all):
    '''Inspect a schematic module, printing out dependencies and their metadata.'''
    data = _inspect(path, scm_sandbox_mode=mode, include_all=all)
    print(json.dumps(data, indent=4))

def drvobj_to_nixref(obj):
    ''' Return a "<name-guid>" string from a derivation object. '''
    env = obj['env']
    if not env.get('guid'):
        return  # sandbox database objects don't have a guid
    return '<%s-%s>' % (env['name'], env['guid'])

def _dependencies_list(path, mode):
    data = _inspect(path, scm_sandbox_mode=mode, include_all=False)
    deps = set()
    for obj in data['dependencies'].values():
        env = obj['env']
        if not env.get('guid'):
            continue
        dep = drvobj_to_nixref(obj)
        deps.add(dep) if dep else None
    return deps

@main.command(name='dependencies-list')
@click.argument('path', shell_complete=autocomplete_path)
@click.option('-m', '--mode', type=click.Choice(['declarative', 'imperative']), default='declarative')
def dependencies_list(path, mode):
    ''' Print a list of dependencies for the given object. '''
    for dep in sorted(list(_dependencies_list(path, mode)), reverse=True):
        print(dep)

def parse_anglepath(path):
    '''
    >>> parse_anglepath('<pg_repack>')
    'pg_repack'
    '''
    match = re.match(r'\<(?P<name>.+)\>', path) if path else None
    if match:
        return match.group('name')

def get_nix_module(path, incl_default_nix=False, nix_paths=os.environ.get('NIX_PATH')):
    '''
    >>> root_dir = env.get_str('ROOT_DIR')
    >>> scm_path = env.get_str('SCM_PATH')
    >>> pkg = os.path.join(root_dir, 'pkg')
    >>> repack = 'pg_repack-HAHOMTIBILACVICS'
    >>> assert get_nix_module('pkg/%s' % repack) == os.path.join(pkg, repack), \
        (get_nix_module('pkg/%s' % repack), os.path.join(pkg, repack))
    >>> assert get_nix_module('pkg/%s' % repack, incl_default_nix=True) == os.path.join(pkg, '%s/default.nix' % repack)
    >>> assert get_nix_module('<pg_repack-HAHOMTIBILACVICS>') == os.path.join(os.path.join(scm_path, 'pkg'), repack), \
        (get_nix_module('<pg_repack-HAHOMTIBILACVICS>'), os.path.join(os.path.join(scm_path, 'pkg'), repack))
    >>> assert get_nix_module('<foo/pg_repack-HAHOMTIBILACVICS>', nix_paths='foo=%s' % pkg) == os.path.join(pkg, repack)
    '''
    anglepath = parse_anglepath(path)
    if anglepath:
        for nix_path in nix_paths.split(':'):
            if '=' in nix_path:
                prefix_str, _, sub = nix_path.partition('=')
                prefix_list = prefix_str.split(os.path.sep)
                path_list = anglepath.split(os.path.sep)
                if prefix_list == path_list[:len(prefix_list)]:
                    path = os.path.join(sub, *path_list[len(prefix_list):])
                    break
            else:
                _path = os.path.join(nix_path, anglepath)
                if os.path.exists(_path):
                    path = _path
                    break
    path = os.path.expanduser(os.path.normpath(path))
    if not os.path.isabs(path):
        path = os.path.abspath(path)
    default_nix_path = os.path.join(path, 'default.nix')
    if path.endswith('.nix') and os.path.exists(path):
        return path
    if os.path.isdir(path) and os.path.exists(default_nix_path):
        if incl_default_nix:
            return default_nix_path
        return path.rstrip('/')

def get_sql_file(path):
    if path.endswith('.sql') and os.path.exists(path):
        return path

def get_free_port():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', 0))
    addr = s.getsockname()
    s.close()
    return addr[1]

def ls_basedirs(store=os.environ['SCM_PG'], sort=lambda x: x[1].st_ctime, reverse=False):
    bds = glob.glob(os.path.join(store, '*'))
    return sorted(((bd, os.stat(bd)) for bd in bds), key=sort, reverse=reverse)

def ls_tmuxservers(scm_tmux=os.environ['SCM_TMUX']):
    return glob.glob(os.path.join(scm_tmux, '*'))

def unexpand_user(path, home=os.environ['HOME']):
    '''
    >>> unexpand_user('/home/scott/var/pg', home='/home/scott')
    '~/var/pg'
    '''
    return re.sub('^%s/' % home.rstrip('/'), '~/', path) if path else path

def validate_name(name):
    if not re.match(r'^[a-z][a-z0-9._-]*$', name):
        print('error: please use a name that includes only letters, numbers, dash, dot or underscore', file=sys.stderr)
        sys.exit(1)

@main.command()
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
def status(verbose):
    '''List status of all databases in ~/var/pg.'''
    for (basedir, stat) in ls_basedirs():
        if not os.path.isdir(basedir):
            continue
        pidfile = os.path.join(basedir, 'data/postmaster.pid')
        pid = int(open(pidfile).readlines()[0].strip()) if os.path.exists(pidfile) else None
        metajson = os.path.join(basedir, 'meta.json')
        try:
            metadata = json.loads(open(metajson).read())
        except (json.JSONDecodeError, KeyError):
            print('invalid %s' % unexpand_user(metajson), file=sys.stderr) if verbose else None
            continue
        except FileNotFoundError:
            print('missing %s' % unexpand_user(metajson), file=sys.stderr) if verbose else None
            continue
        try:
            proc = psutil.Process(pid) if pid and pid > 0 else None
        except psutil.NoSuchProcess:
            proc = None
        out = ''
        out += ('pid %5d' % pid) if pid else '         '
        if proc:
            out += (' %s %s' % (colored.green('up'), dates.fmt_age(proc.create_time())))
        else:
            out += (' %s %s' % (colored.red('dn'), dates.fmt_age(stat.st_mtime)))
        out += ' port %5d' % metadata['port']
        out += ' %s' % unexpand_user(basedir)
        print(out)

def get_basedir(path):
    basedir = os.path.normpath(os.path.expanduser(path))
    if basedir.endswith('/data'):
        basedir = '/'.join(basedir.split('/')[:-1])
    return basedir

def autocomplete_basedir(ctx, args, incomplete):  # pylint: disable=unused-argument
    prefix = os.environ['SCM_PG'].rstrip('/')
    basedirs = glob.glob('%s/*' % prefix)
    basedirs = basedirs + [unexpand_user(d) for d in basedirs]
    return [d for d in basedirs if d.startswith(incomplete)]

@main.command()
@click.argument('basedir', shell_complete=autocomplete_basedir, nargs=-1)
@click.option('--shutdown-mode', '-m', type=click.Choice(['smart', 'fast', 'immediate']), default='fast', help='Specifies the shutdown mode. Default: fast') # pylint: disable=line-too-long
@click.option('-q', '--quiet', is_flag=True, help='Quiet mode. Updates only the log file. No output is shown.')
def stop(basedir, shutdown_mode, quiet):
    '''Stop specified database server.'''
    for bdir in basedir:
        pg_ctl_fn(get_basedir(bdir), 'stop', shutdown_mode=shutdown_mode, quiet=quiet)

@main.command()
@click.argument('basedir', shell_complete=autocomplete_basedir, nargs=-1)
@click.option('-q', '--quiet', is_flag=True, help='Quiet mode. Updates only the log file. No output is shown.')
def start(basedir, quiet):
    '''Start specified database server.'''
    for bdir in basedir:
        pg_ctl_fn(get_basedir(bdir), 'start', quiet=quiet)

@main.command()
@click.argument('basedir', shell_complete=autocomplete_basedir, nargs=-1)
@click.option('-q', '--quiet', is_flag=True, help='Quiet mode. Updates only the log file. No output is shown.')
def reload(basedir, quiet):
    '''Reload specified database server.
        This allows changing of configuration-file options that do not require a complete restart to take effect.'''
    for bdir in basedir:
        pg_ctl_fn(get_basedir(bdir), 'reload', quiet=quiet)

@main.command()
@click.argument('basedir', shell_complete=autocomplete_basedir, nargs=-1)
@click.option('--shutdown-mode', '-m', type=click.Choice(['smart', 'fast', 'immediate']), default='fast', help='Specifies the shutdown mode. Default: fast') # pylint: disable=line-too-long
@click.option('-q', '--quiet', is_flag=True, help='Quiet mode. Updates only the log file. No output is shown.')
def restart(basedir, shutdown_mode, quiet):
    '''Restart specified database server. This allows changing the postgres command-line options.'''
    for bdir in basedir:
        pg_ctl_fn(get_basedir(bdir), 'restart', shutdown_mode=shutdown_mode, quiet=quiet)

def psql_from_basedir(basedir):
    metajson = os.path.join(basedir, 'meta.json')
    if not os.path.exists(metajson):
        sys.stderr.write('The following %s database does not exist', metajson)
        sys.exit(1)
    metadata = json.loads(open(metajson).read())
    if not sys.stderr.isatty() or not sys.stdout.isatty():
        sys.stderr.write(f'refusing to open psql for {basedir} because shell is not a tty')
        sys.exit(5)
    ensure_pg_running(basedir, metadata['port'])
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    psql_cmd = [
        os.path.join(basedir, 'bin/psql'), '-p', str(metadata['port']), '-U', metadata['user'], '-h', 'localhost',
        metadata['dbname']]
    subprocess.Popen(psql_cmd).communicate()

@main.command()
@click.argument('basedir', shell_complete=autocomplete_basedir)
def psql(basedir):
    '''Open psql shell to specified database.'''
    basedir = get_basedir(basedir)
    psql_from_basedir(basedir)

def has_pidfile(basedir):
    return os.path.exists(os.path.join(basedir, 'data/postmaster.pid'))

def _gc_basedir(basedir, stop=False, force=False, verbose=False, raise_exc=False):
    if not basedir:
        return
    metajson = os.path.join(basedir, 'meta.json')
    metadata = None
    try:
        metadata = json.loads(open(metajson).read())
    except (json.JSONDecodeError, KeyError):
        print('invalid %s' % unexpand_user(metajson), file=sys.stderr) if verbose else None
        if raise_exc:
            raise
        return
    except FileNotFoundError:
        print('missing %s' % unexpand_user(metajson), file=sys.stderr) if verbose else None
        if raise_exc:
            raise
        return
    if not metadata['istemp'] and not force:
        print('not a temp datababase: %s (hint: use `scm gc-basedir --force` to insist on deleting)' % \
              unexpand_user(basedir), file=sys.stderr) if verbose else None
        return
    is_running = has_pidfile(basedir)
    if not metadata['istemp'] and (stop or not is_running):
        consent = prompt_yesno('remove database %s?' % unexpand_user(basedir))
        if not consent:
            return
    if is_running:
        if not stop:
            print(
                'won\'t gc a running server: %s  (hint: use `scm gc-basedir --stop` to insist on stopping)' %
                unexpand_user(basedir), file=sys.stdout) if verbose else None
            if raise_exc:
                raise RuntimeError
            return
        pg_ctl_fn(basedir, 'stop', shutdown_mode='immediate', quiet=(not verbose))
    if verbose:
        print('removing %s' % unexpand_user(basedir), file=sys.stderr)
    subprocess.check_call(['chmod', '-R', '0700', basedir])
    shutil.rmtree(basedir)

@main.command(name='gc-basedir')
@click.argument('basedir', shell_complete=autocomplete_basedir)
@click.option('-s', '--stop', is_flag=True, help='Stop database if it\'s running (otherwise leave it be).')
@click.option('-f', '--force', is_flag=True, help='Remove a non-sandbox database (prompting for safety).')
def gc_basedir(basedir, stop, force):
    try:
        _gc_basedir(basedir, stop=stop, force=force, verbose=True, raise_exc=True)
    except FileNotFoundError:
        sys.exit(2)
    except (json.JSONDecodeError, KeyError):
        sys.exit(3)
    except RuntimeError:
        sys.exit(4)

def is_socket(path):
    s = os.stat(path)
    return stat.S_ISSOCK(s.st_mode)

def _gc(verbose):
    for (basedir, _) in ls_basedirs():
        if not has_pidfile(basedir):  # don't gc a running database
            _gc_basedir(basedir, verbose=verbose)
    for socket_path in ls_tmuxservers():
        if not is_socket(socket_path):
            continue
        remove_tmux_socket(socket_path)

@main.command()
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
def gc(verbose):
    '''
    Delete obsolete sandbox databases from ~/var/pg and detached sockets from ~/var/tmux/.

    Database must be stopped and specify istemp = true in meta.json.
    '''
    _gc(verbose)

def create_schema(name):
    validate_name(name)
    guid = guids.get_schema_guid()
    dirname = '%s-%s' % (name, guid)
    schema_path = os.path.abspath(os.path.join(env.get_str('ROOT_DIR'), 'pkg', dirname))
    mkdir(schema_path)
    nixout = os.path.join(schema_path, 'default.nix')
    if os.path.exists(nixout):
        print('error, file exists: %s' % nixout, file=sys.stderr)
        sys.exit(1)
    with open(nixout, 'w') as out:
        out.write('''
stdargs @ { scm, ... }:

scm.schema {
    guid = "%s";
    name = "%s";
    upgrade_sql = ./upgrade.sql;
    dependencies = [

    ];
}\n'''.lstrip() % (guid, name))
    sqlout = os.path.join(schema_path, 'upgrade.sql')
    if os.path.exists(sqlout):
        print('error, file exists: %s' % sqlout, file=sys.stderr)
        sys.exit(1)
    with open(sqlout, 'w') as out:
        out.write('\n')
    print('created "<%s>"' % dirname)
    print('    %s' % os.path.relpath(nixout))
    print('    %s' % os.path.relpath(sqlout))
    return '<%s>' % dirname

@main.command()
@click.argument('name')
def schema(name):
    '''Create files for a new schema object.'''
    create_schema(name)

def escape_double_quote(text):
    r'''
    >>> escape_double_quote('hello')
    'hello'
    >>> escape_double_quote('hello "world"')
    'hello \\"world\\"'
    '''
    return text.replace('"', r'\"')

def create_tablespace(name, location, comment):
    validate_name(name)
    guid = guids.get_tablespace_guid()
    dirname = '%s-%s' % (name, guid)
    tablespace_path = os.path.abspath(os.path.join(env.get_str('ROOT_DIR'), 'pkg', dirname))
    mkdir(tablespace_path)
    nixout = os.path.join(tablespace_path, 'default.nix')
    if os.path.exists(nixout):
        print('error, file exists: %s' % nixout, file=sys.stderr)
        sys.exit(1)
    with open(nixout, 'w') as out:
        out.write('''
stdargs @ { scm, ... }:

scm.tablespace {
    guid = "%s";
    name = "%s";
    location = "%s";
    comment = "%s";
    seq_page_cost = null;
    random_page_cost = null;
    effective_io_concurrency = null;
    maintenance_io_concurrency = null;
}\n'''.lstrip() % (guid, name, escape_double_quote(location), escape_double_quote(comment or '')))
    print('created "<%s>"' % dirname)
    print('    %s' % os.path.relpath(nixout))
    return '<%s>' % dirname

@main.command()
@click.argument('name')
@click.argument('location')  # absolute fileystem path to place tablespace files
@click.option('-c', '--comment', help='comment to describe what the tablespace is for')
def tablespace(name, location, comment):
    ''' Create files for a new tablespace object.

        Use this instead of a schema because tablespaces require setup on the filesystem.
        Tablespace objects don't require revisions like schemas do. They are idempotent.
        You can change the fields following fields in the default.nix and the changes will be performed automatically:
        - comment
        - seq_page_cost
        - random_page_cost
        - effective_io_concurrency
        - maintenance_io_concurrency

        You can not change the name field, if you do, it will create a new tablespace using the new name.
        If you change the location, it will cause a build failure because this change cannot be made against a running
        server. To resolve this, stop postgresql, make the change manually, and start postgresql. Then the build will
        succeed.
    '''
    create_tablespace(name, location, comment)

def create_namespace(name, comment):
    validate_name(name)
    guid = guids.get_namespace_guid()
    dirname = '%s-%s' % (name, guid)
    namespace_path = os.path.abspath(os.path.join(env.get_str('ROOT_DIR'), 'pkg', dirname))
    mkdir(namespace_path)
    nixout = os.path.join(namespace_path, 'default.nix')
    if os.path.exists(nixout):
        print('error, file exists: %s' % nixout, file=sys.stderr)
        sys.exit(1)
    with open(nixout, 'w') as out:
        out.write('''
stdargs @ { scm, ... }:

scm.namespace {
    guid = "%s";
    name = "%s";
    comment = "%s";
}\n'''.lstrip() % (guid, name, escape_double_quote(comment or '')))
    print('created "<%s>"' % dirname)
    print('    %s' % os.path.relpath(nixout))
    return '<%s>' % dirname

@main.command()
@click.argument('name')
@click.option('-c', '--comment', help='comment to describe what the namespace is for')
def namespace(name, comment):
    ''' Create files for a new namespace (ie "CREATE SCHEMA") object.

        The terms "schema" and "namespace" refer to different things, but are used inconsistently. Schematic uses the
        term "schema" to refer to a package that contains sql statements, and "namespace" for the qualified prefix that
        schema objects are nested under. PostgreSQL uses the term "namespace" internally to mean the same thing, but
        externally it often uses the term "schema" to mean the same thing (eg "CREATE SCHEMA").

        It is recommended to create a schematic namespace object instead of creating a schema package with a
        "CREATE SCHEMA" command in it. By doing so, adding a dependency on a namespace package will automatically
        add it to the search_path so that objects can be referred to without the namespace prefix. When one namespace
        depends on another namespace (directly or transitively), it will place them in the correct order in search_path
        (the dependee will occur first, so that it occludes the dependency).

        Namespaces objects don't require revisions like schemas do. They are idempotent.

        You can not change the name field, if you do, it will create a new namespace using the new name.
    '''
    create_namespace(name, comment)

def create_pghba(name, comment):
    validate_name(name)
    guid = guids.get_pghba_guid()
    dirname = '%s-%s' % (name, guid)
    pghba_path = os.path.abspath(os.path.join(env.get_str('ROOT_DIR'), 'pkg', dirname))
    mkdir(pghba_path)
    nixout = os.path.join(pghba_path, 'default.nix')
    if os.path.exists(nixout):
        print('error, file exists: %s' % nixout, file=sys.stderr)
        sys.exit(1)
    with open(nixout, 'w') as out:
        out.write('''
stdargs @ { scm, ... }:

scm.pghba {
    guid = "%s";
    name = "%s";
    comment = "%s";
    rules = [
        {
            type = "host";
            dbname = "all";
            user = "all";
            addr = "127.0.0.1/32, ::1/128, ...";
            auth = "trust, reject, scram-sha-256, md5, ...";
            opts = "";
            comment = "...";
        }
    ];
    dependencies = [];
}\n'''.lstrip() % (guid, name, escape_double_quote(comment or '')))
    print('created "<%s>"' % dirname)
    print('    %s' % os.path.relpath(nixout))
    print('    docs: https://www.postgresql.org/docs/current/auth-pg-hba-conf.html')
    print('    hint: for multiple rules, separate by whitespace (not comma)')
    return '<%s>' % dirname

@main.command()
@click.argument('name')
@click.option('-c', '--comment', help='comment to describe what this host-base auth config is for')
def pghba(name, comment):
    ''' Create a package for a new pghba object which generates a section in a pg_hba.conf file.

        "HBA" stands for "host-based authentication". The pg_hba.conf file has a list of rules for validating client
        authentication requests. See: https://www.postgresql.org/docs/current/auth-pg-hba-conf.html

        Instead of maintaining a pg_hba.conf file directly, use these pghba packages to generate it. This enables
        composability by splitting the configuration into smaller logical packages that can be combined together for
        a given database instead of overwriting eachother. It automatically sends a reload signal to postgresql when the
        configuration changes, and performs additional validation checks before doing so.

        pghba objects don't require revisions; they are idempotent.
    '''
    create_pghba(name, comment)

def is_local_scm_obj(path):
    ''' Is :path: a schematic object it the current working directory? '''
    if not path:
        return False
    path = path_relative(path, '.')
    objtypes = ['pkg', 'srv', 'rev']
    for objtype in objtypes:
        if path.startswith('./%s/' % objtype):
            return True
    return False

def path_relative(path, relativeto):
    '''Turn an absolute path reference into a path to a reference path.'''
    relpath = os.path.relpath(path, relativeto)
    if not relpath.startswith('/') and not relpath.startswith('.'):
        relpath = './%s' % relpath
    return relpath

def path_nixpath(path, nix_paths=os.environ.get('NIX_PATH')):
    '''Turn an absolute path reference into a path relative to NIX_PATH.'''
    if not nix_paths:
        return
    abs_path = os.path.normpath(os.path.expanduser(path))
    basename = os.path.basename(path)
    for nix_path in nix_paths.split(':'):
        if '=' in nix_path:
            prefix_str, _, sub = nix_path.partition('=')
            sub_list = sub.split(os.path.sep)
            path_list = abs_path.split(os.path.sep)
            if sub_list == path_list[:len(sub_list)]:
                return '<%s/%s>' % (prefix_str, os.path.sep.join(path_list[len(sub_list):]))
        elif os.path.relpath(abs_path, nix_path) == basename:
            return '<%s>' % basename

def path_norm(path, relativeto=None, nix_paths=os.environ.get('NIX_PATH')):
    '''
    Turn an absolute path reference into a path relative to NIX_PATH or relative to a reference path.

    >>> root_dir = env.get_str('ROOT_DIR')
    >>> rev = os.path.join(root_dir, 'rev/2020-11-16-country-R000GHY2HI7C7675')
    >>> pkg = os.path.join(root_dir, 'pkg/country-S0Y2F1PPW4X10VTW')
    >>> srv = os.path.join(root_dir, 'srv/world-D0J6PLZEYV46LZA8')
    >>> path_norm(srv, None, os.path.join(root_dir, 'srv'))
    '<world-D0J6PLZEYV46LZA8>'
    >>> path_norm(srv, None, 'srv=%s' % os.path.join(root_dir, 'srv'))
    '<srv/world-D0J6PLZEYV46LZA8>'
    >>> path_norm(rev, None, os.path.join(root_dir, 'rev'))
    '<2020-11-16-country-R000GHY2HI7C7675>'
    >>> path_norm(rev, None, 'rev=%s' % os.path.join(root_dir, 'rev'))
    '<rev/2020-11-16-country-R000GHY2HI7C7675>'
    >>> path_norm(pkg, None, os.path.join(root_dir, 'pkg'))
    '<country-S0Y2F1PPW4X10VTW>'
    >>> path_norm(pkg, None, 'pkg=%s' % os.path.join(root_dir, 'pkg'))
    '<pkg/country-S0Y2F1PPW4X10VTW>'
    >>> path_norm(srv, pkg, None)
    '../../srv/world-D0J6PLZEYV46LZA8'
    >>> path_norm(rev, pkg, None)
    '../../rev/2020-11-16-country-R000GHY2HI7C7675'
    >>> path_norm(pkg, srv, None)
    '../../pkg/country-S0Y2F1PPW4X10VTW'
    '''
    return path_nixpath(path, nix_paths) or path_relative(path, relativeto)

def update_schema_deps(schema_path, dependencies):
    '''Replace the 'dependencies' attribute in a schema source file.'''
    schema_nix = get_nix_module(schema_path, incl_default_nix=True)
    orig_src = open(schema_nix).read()
    norm_deps = set(filter(None, dependencies))
    def replacement(match):  # pylint: disable=unused-argument
        return 'dependencies = [%s\n    ];' % ''.join(['\n        %s' % d for d in sorted(norm_deps, reverse=True)])
    updated_src = re.sub(r'dependencies\s*=\s*\[\s*([\w\./\-_\s\<\>]*)\s*\];', replacement, orig_src, flags=re.DOTALL)
    with open(schema_nix, 'w') as out:
        out.write(updated_src)
    updated_schema_metadata = _inspect(schema_nix, scm_sandbox_mode='imperative')
    updated_schema_obj = list(get_immediate_deps(updated_schema_metadata, scm_types=('schema',)))[0]
    updated_norm_deps = filter(None, [
        drvobj_to_nixref(d) for d in
        get_immediate_deps(updated_schema_metadata, cursor=updated_schema_obj['drv_path'])])
    if set(norm_deps) == set(updated_norm_deps):
        print('updated %s' % os.path.relpath(schema_nix))
    else:
        print('failed updating %s - must update dependencies by hand' % os.path.relpath(schema_nix), file=sys.stderr)
        with open(schema_nix, 'w') as out:
            out.write(orig_src)

def get_transitive_dependencies(inspect_metadata, cursor=None):
    cursor = cursor or inspect_metadata['meta']['drv_path']
    cursor_obj = inspect_metadata['dependencies'][cursor]
    for sub_drv in cursor_obj['inputDrvs']:
        if sub_drv not in inspect_metadata['dependencies']:
            continue  # pure software dependency
        sub_obj = inspect_metadata['dependencies'][sub_drv]
        yield sub_obj
        yield from get_transitive_dependencies(inspect_metadata, cursor=sub_drv)

def get_transitive_revision_heads(inspect_metadata, cursor=None):
    cursor = cursor or inspect_metadata['meta']['drv_path']
    cursor_obj = inspect_metadata['dependencies'][cursor]
    for sub_drv in cursor_obj['inputDrvs']:
        if sub_drv not in inspect_metadata['dependencies']:
            continue  # pure software dependency
        sub_obj = inspect_metadata['dependencies'][sub_drv]
        if sub_obj['env']['scm_type'] in ('revision', 'tablespace', 'namespace', 'pghba'):
            yield sub_obj
        elif sub_obj['env']['scm_type'] == 'schema':
            yield from get_transitive_revision_heads(inspect_metadata, cursor=sub_drv)

def get_minimal_revision_heads(inspect_metadata):
    ''' Get the mininum set of revisions dependended on (eliminating redundant transitive dependencies). '''
    suppress = set()
    deps = list(get_transitive_revision_heads(inspect_metadata))
    for dep in deps:
        for trans_dep in get_transitive_dependencies(inspect_metadata, cursor=dep['drv_path']):
            nixref = drvobj_to_nixref(trans_dep)
            suppress.add(nixref) if nixref else None
    for dep in deps:
        nixref = drvobj_to_nixref(dep)
        if nixref and nixref not in suppress:
            yield dep

def get_immediate_deps(inspect_metadata, cursor=None, scm_types=None):
    cursor = cursor or inspect_metadata['meta']['drv_path']
    cursor_obj = inspect_metadata['dependencies'][cursor]
    for sub_drv in cursor_obj['inputDrvs']:
        if sub_drv not in inspect_metadata['dependencies']:
            continue  # pure software dependency
        sub_obj = inspect_metadata['dependencies'][sub_drv]
        if not scm_types or sub_obj['env']['scm_type'] in scm_types:
            yield sub_obj

def get_transitive_schemas(inspect_metadata, cursor=None):
    ''' Yield dependent schemas, depth-firsth. '''
    cursor = cursor or inspect_metadata['meta']['drv_path']
    cursor_obj = inspect_metadata['dependencies'][cursor]
    for sub_drv in cursor_obj['inputDrvs']:
        if sub_drv not in inspect_metadata['dependencies']:
            continue  # pure software dependency
        sub_obj = inspect_metadata['dependencies'][sub_drv]
        if sub_obj['env']['scm_type'] == 'schema':
            yield from get_transitive_schemas(inspect_metadata, sub_drv)
            yield sub_obj

def get_transitive_schemas_unique(inspect_metadata, cursor=None):
    ''' Yield dependent schemas, depth-firsth, de-duplicated. '''
    seen = set()
    for dep in get_transitive_schemas(inspect_metadata, cursor=cursor):
        nixref = drvobj_to_nixref(dep)
        if nixref and nixref not in seen:
            seen.add(nixref)
            yield dep

def _revision_diff_filesystem(rev_basefiles, obj_basedir, sbj_basedir, verbose=False):
    '''
    If the schema has basefiles defined, diff the declarative against the imperative basedir to discover basefiles
    that have diverged.

    This is useful for discovering basefiles that have been edited in a schema so that they can be included in a
    generated revision.

    Returns list of relative files names that have been changed.
    '''
    ret = []
    for path in walk_dir_rel(rev_basefiles):
        rev_path = os.path.join(rev_basefiles, path)
        if os.path.isdir(rev_path):
            if not os.listdir(rev_path):
                os.rmdir(rev_path)
            continue
        obj_path = os.path.join(obj_basedir, path) if obj_basedir else None
        sbj_path = os.path.join(sbj_basedir, path) if sbj_basedir else None
        if same_file_content(obj_path, sbj_path):
            print('same %s' % path, file=sys.stderr) if verbose else None
            os.unlink(rev_path)
        else:
            ret.append(path)
            print('diff %s' % path, file=sys.stderr) if verbose else None
    if not os.listdir(rev_basefiles):
        os.rmdir(rev_basefiles)
    return ret

def add_basefiles_attribute(schema_path, schema_drv_obj):
    ''' Add `basefiles = ./basefiles;` attribute to a schema object if it doesn't already exist. '''
    if schema_drv_obj['env']['basefiles']:
        return  # basefiles attribute already defined
    schema_relpath = os.path.relpath(schema_path)
    with open(os.path.join(schema_path, 'default.nix'), 'r+') as fp:
        schema_content = fp.read()
        fp.seek(0)
        for line in schema_content.split('\n'):
            print(line, file=fp)
            if re.search(r'\Wupgrade_sql\s*=', line):
                print('    basefiles = ./basefiles;', file=fp)
                print('added basefiles attribute to %s' % schema_relpath, file=sys.stderr)

def create_revision(path, verbose=False, auto=False):
    schema_path = get_nix_module_dir(path)
    if not schema_path:
        print('invalid nix module: %s' % path, file=sys.stderr)
        sys.exit(2)
    schema_metadata = _inspect(schema_path, scm_sandbox_mode='imperative')
    schema_drv_obj = list(get_immediate_deps(schema_metadata, scm_types=('schema',)))[0]
    schema_name = schema_drv_obj['env']['name']
    if os.path.isdir(os.path.join(schema_path, 'basefiles')):
        # if ./basefiles directory exists, ensure it's defined as an attribute on the schema otherwise it will be
        # ignored, causing bad UX
        add_basefiles_attribute(schema_path, schema_drv_obj)
    guid = guids.get_revision_guid()
    name = '%s-%s' % (datetime.datetime.now().date().isoformat(), schema_name)
    dirname = '%s-%s' % (name, guid)
    revision_dir = os.path.join(env.get_str('ROOT_DIR'), 'rev', dirname)
    mkdir(revision_dir)
    print('created "<%s>"' % dirname)
    revision_heads = list(get_minimal_revision_heads(schema_metadata))
    dependencies_str = '\n        '.join(sorted(
        filter(None, list({drvobj_to_nixref(r) for r in revision_heads})), reverse=True))
    revision_nix = os.path.join(revision_dir, 'default.nix')
    revision_content = ''
    obj_basedir = sbj_basedir = None
    do_diff = auto and bool(revision_heads or schema_drv_obj['env']['basefiles'])
    if do_diff:
        try:
            obj_drv = _sandbox(schema_path, scm_sandbox_mode='declarative', verbose=verbose)
            sbj_drv = _sandbox(schema_path, scm_sandbox_mode='imperative', verbose=verbose)
        except subprocess.CalledProcessError:
            print('error: failed to create sandbox database', file=sys.stderr)
            sys.exit(1)
        obj_basedir = obj_drv['env']['basedir']
        sbj_basedir = sbj_drv['env']['basedir']
    if not any(get_immediate_deps(schema_metadata, cursor=schema_drv_obj['drv_path'], scm_types=('revision',))):
        # prefer a literal copy of upgrade.sql for initial revision (over diff)
        revision_content = open(os.path.join(schema_path, 'upgrade.sql')).read()
    elif obj_basedir and sbj_basedir:
        migration = schema_diff(sbj_basedir, obj_basedir)
        revision_content = migration.sql
    schema_basefiles = os.path.join(schema_path, 'basefiles')
    rev_basefiles = os.path.join(revision_dir, 'basefiles')
    include_basefiles = False
    if os.path.isdir(schema_basefiles):
        include_basefiles = True
        subprocess.check_call(['rsync', '-rlc', '%s/' % schema_basefiles, rev_basefiles])
        if do_diff and obj_basedir and sbj_basedir:
            include_basefiles = bool(
                _revision_diff_filesystem(rev_basefiles, obj_basedir, sbj_basedir, verbose))
    revision_nix_content = '''
stdargs @ { scm, ... }:

scm.revision {
    guid = "%(guid)s";
    name = "%(name)s";
    basefiles = ./basefiles;
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        %(dependencies)s
    ];
}\n'''.lstrip() % {'guid': guid, 'name': name, 'dependencies': dependencies_str}
    if not include_basefiles:
        revision_nix_content = revision_nix_content.replace('''
    basefiles = ./basefiles;''', '')
    with open(revision_nix, 'w') as out:
        out.write(revision_nix_content)
    print('    %s' % os.path.relpath(revision_nix))
    upgrade_sql = os.path.join(revision_dir, 'upgrade.sql')
    with open(upgrade_sql, 'w') as out:
        out.write(revision_content)
    print('    %s' % os.path.relpath(upgrade_sql))
    schema_new_deps = list(filter(None, [
        drvobj_to_nixref(obj) for obj in get_immediate_deps(schema_metadata, cursor=schema_drv_obj['drv_path'])
        if obj['env'] and obj['env']['scm_type'] and obj['env']['scm_type'] != 'revision'] + [path_norm(revision_dir)]))
    update_schema_deps(schema_path, schema_new_deps)
    _gc_basedir(obj_basedir, force=True, stop=True, verbose=verbose)
    _gc_basedir(sbj_basedir, force=True, stop=True, verbose=verbose)

@main.command()
@click.argument('schema_path', shell_complete=autocomplete_path)
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
@click.option('-a', '--auto', is_flag=True, help='Initialize a suggested revision from diff (WIP).')
@click.option('-r', '--recursive', is_flag=True, help='Also create revisions for dependency schemas without any.')
def revision(schema_path, verbose, auto, recursive):
    '''Create files for a new revision object to migrate the specified schema.'''
    if recursive:
        schema_metadata = _inspect(schema_path, scm_sandbox_mode='imperative')
        for dep in get_transitive_schemas_unique(schema_metadata):
            if any(get_immediate_deps(schema_metadata, cursor=dep['drv_path'], scm_types=('revision',))):
                continue
            subpath = get_nix_module_dir(drvobj_to_nixref(dep))
            if not subpath or subpath.startswith('/nix/store'):
                continue  # don't attempt to create revisions for immutable 3rd party dependencies in nix store
            create_revision(subpath, verbose=verbose, auto=auto)
    create_revision(schema_path, verbose=verbose, auto=auto)

@main.command('diff-schema')
@click.argument('schema_path', shell_complete=autocomplete_path)
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
def diff_schema(schema_path, verbose):
    '''Find differences between declarative and imperative implementations of a schema'''
    try:
        obj_drv = _sandbox(schema_path, scm_sandbox_mode='declarative', verbose=verbose)
        sbj_drv = _sandbox(schema_path, scm_sandbox_mode='imperative', verbose=verbose)
    except subprocess.CalledProcessError:
        if verbose:
            print('error: failed to create sandbox database', file=sys.stderr)
        sys.exit(2)
    obj_basedir = obj_drv['env']['basedir']
    sbj_basedir = sbj_drv['env']['basedir']
    migration = schema_diff(sbj_basedir, obj_basedir)
    revision_content = migration.sql
    if revision_content:
        print(revision_content)
    if verbose and not revision_content:
        print('no differences', file=sys.stderr)
    _gc_basedir(obj_basedir, force=True, stop=True, verbose=verbose)
    _gc_basedir(sbj_basedir, force=True, stop=True, verbose=verbose)
    if revision_content:
        exit(1)

# @main.command(name='lint-dependencies')
# @click.argument('path', shell_complete=autocomplete_path, nargs=-1)
# def lint_dependencies(path):
#     '''Sort and de-duplicate 'dependencies' list in nix modules.'''
#     for p in path or []:
#         try:
#             metadata = _inspect(p)
#         except AssertionError:
#             metadata = None
#         if not metadata:
#             print('%s: invalid nix module?' % p, file=sys.stderr)
#             continue
#         deps = [d['source_path'] for d in metadata['dependencies']]
#         if deps != sorted(deps, reverse=True):
#             print('%s: sorted' % p, file=sys.stderr)
#         uniq_deps = sorted(list(set(deps)), reverse=True)
#         if len(deps) != len(uniq_deps):
#             print('%s: uniqued %d -> %d' % (p, len(deps), len(uniq_deps)), file=sys.stderr)
#         if deps == uniq_deps:
#             continue
#         update_schema_deps(p, uniq_deps)

def file_content(filename):
    '''Return file mode bits and file content in tuple (for equivalence comparison).'''
    try:
        return os.stat(filename).st_mode, open(filename).read()
    except FileNotFoundError:
        return (None, None)

def same_file_content(file1, file2):
    '''Check if the two filenames contain contain the same content and have the same file mode bits.'''
    data1 = file_content(file1)
    data2 = file_content(file2)
    return data1 == data2

def walk_dir_rel(prefix):
    '''Generate relative sub-paths of @prefix from bottom up.'''
    for dirpath, dirnames, filenames in os.walk(prefix, topdown=False):
        for path in dirnames + filenames:
            yield os.path.relpath(os.path.join(dirpath, path), prefix)

@main.command()
@click.argument('name')
@click.option('-d', '--dbname', help='Define postgresql dbname (if different from name).')
def database(name, dbname):
    '''
    Create files for a new database object.

    name: Logical name of database (tip: suffix primaries with 0, replicas with 1, 2, etc).

    dbname: Physical name of database (passed to PostgreSQL createdb).
    '''
    validate_name(name)
    dbname = dbname or name
    validate_name(dbname)
    database_guid = guids.get_database_guid()
    dirname = '%s-%s' % (name, database_guid)
    database_path = os.path.abspath(os.path.join(env.get_str('ROOT_DIR'), 'srv', dirname))
    server_guid = guids.get_server_guid()
    mkdir(database_path)
    outpath = os.path.join(database_path, 'default.nix')
    if os.path.exists(outpath):
        print('error, file exists: %s' % outpath, file=sys.stderr)
        sys.exit(1)
    params = {
        'database_guid': database_guid,
        'server_guid': server_guid,
        'name': name,
        'dbname': dbname,
        'port': get_free_port(),
        'user': env.get_str('SCM_USER', 'root'),
        'pass': env.get_str('SCM_PASS', guids.get_password()),
    }
    with open(outpath, 'w') as out:
        out.write('''
stdargs @ { scm, pkgs, ... }:

scm.database rec {
    guid = "%(database_guid)s";
    name = "%(name)s";
    server = scm.server rec {
        postgresql = pkgs.postgresql_15;
        guid = "%(server_guid)s";
        name = "%(name)s";
        dbname = "%(dbname)s";
        port = "%(port)d";
        user = "%(user)s";
        password = "%(pass)s";
    };
    dependencies = [

    ];
}\n'''.lstrip() % params)
    print('created %s' % outpath)


class ScmMigration(migra.Migration):

    def __init__(self, x_from, x_target, schema=None):
        super(ScmMigration, self).__init__(x_from, x_target, schema=schema)
        self.set_safety(False)

    def add_creations(self, privileges=False):
        self.add(self.changes.schemas(creations_only=True))
        self.add(self.changes.extensions(creations_only=True))
        self.add(self.changes.collations(creations_only=True))
        self.add(self.changes.enums(creations_only=True, modifications=True))
        self.add(self.changes.sequences(creations_only=True))
        # This command doesn't support creations_only; can generate DROP TABLE statements
        self.add(self.changes.selectables())
        self.add(self.changes.indexes(creations_only=True))
        self.add(self.changes.pk_constraints(creations_only=True))
        self.add(self.changes.non_pk_constraints(creations_only=True))
        self.add(self.changes.privileges(creations_only=True)) if privileges else None
        self.add(self.changes.rlspolicies(creations_only=True))
        self.add(self.changes.triggers(creations_only=True))

def _get_pg_uri(ref):
    ''' Get postgresql URI from a basedir path or postgresql URI. '''
    if not ref:
        return
    if ref.lower().startswith('postgresql://'):
        return ref
    return pguri_from_basedir(get_basedir(ref))

def schema_diff(obj, sbj):
    '''Diff two databases (specified by basedir) and output the schema difference.'''
    obj_uri = _get_pg_uri(obj)
    sbj_uri = _get_pg_uri(sbj)
    with sqlbag.S(str(obj_uri)) as obj_pg, sqlbag.S(str(sbj_uri)) as sbj_pg:
        migration = ScmMigration(obj_pg, sbj_pg)
        migration.add_creations()
        # HACK: remove me once migra is replaced with custom diff
        blacklist = ['drop table "meta"."revision";']
        for item in blacklist:
            if item in migration.statements:
                migration.statements.remove(item)
        return migration

@main.command()
@click.argument('declarative', shell_complete=autocomplete_basedir)
@click.argument('imperative', shell_complete=autocomplete_basedir)
def diff(declarative, imperative):
    '''Diff two databases (specified by basedir or postgresql URI) and output the schema difference.'''
    # TODO: should also diff files in basedir  # pylint: disable=fixme
    migration = schema_diff(declarative, imperative)
    if migration.statements:
        print(migration.sql)
    else:
        print('nothing to do', file=sys.stderr)

def get_pg_dump(pguri, tablename):
    cmd = [
        'pg_dump', '--no-owner', '--schema-only', '--no-privileges',
        '-d', str(pguri),
        '-t', tablename,
    ]
    return subprocess.check_output(cmd, text=True)  # pylint: disable=unexpected-keyword-arg

def parse_ddl_statements(text):
    statement = ''
    in_comment_block = False
    for line in text.split('\n'):
        if line.startswith('/*'):
            in_comment_block = True
        if in_comment_block:
            if line.endswith('*/'):
                in_comment_block = False
            continue
        if line.startswith('--') or line.startswith('SET ') or line.startswith('SELECT pg_catalog.set_config'):
            continue
        if not line.strip():
            continue
        statement += line + '\n'
        if line.endswith(';'):
            yield statement
            statement = ''

def parse_create_table_names(text):
    # TODO: replace this implementation with pglast  # pylint: disable=fixme
    for statement in parse_ddl_statements(text):
        match = re.match(r'CREATE( UNLOGGED)? TABLE (?P<tablename>(\w+\.)\w+)\W', statement)
        if match:
            yield match.group('tablename')

def find_statement_match(src_text, statement):
    if statement in src_text:
        return statement
    statement = re.sub(r'\s+', ' ', statement).rstrip(' ')
    if statement in src_text:
        return statement

def get_nix_module_dir(module):
    path = get_nix_module(module)
    if path:
        return path if os.path.isdir(path) else os.path.dirname(path)

def _schemaname(tablename):
    '''
    >>> _schemaname('public.foo')
    'foo'
    >>> _schemaname('bar.baz')
    'bar.baz'
    '''
    nameparts = tablename.split('.')
    if len(nameparts) == 2 and nameparts[0] == 'public':
        return nameparts[-1]
    return tablename

def safe_get(data, *keys):
    '''
    Access @*keys in nested @data dictionary and return `None` instead of `KeyError`.

    >>> safe_get(None, 'x')
    >>> safe_get({}, 'x')
    >>> safe_get({'x': 1}, 'x')
    1
    >>> safe_get({'x': {'y': 2}}, 'x', 'y')
    2
    >>> safe_get({'x': {'y': ''}}, 'x', 'y')
    ''
    >>> safe_get({'x': [{'y': 'z'}]}, 'x', 0, 'y')
    'z'
    >>> safe_get({'x': [{'y': 'z'}]}, 'x', '0', 'y')
    >>> safe_get(['a'], 1)
    '''
    val = data
    for key in keys:
        if val is None:
            return
        try:
            val = val[key]
        except (TypeError, IndexError, KeyError):
            val = None
    return val

def _parse_fk_ref_table(statement):
    '''
    >>> _parse_fk_ref_table('ALTER TABLE ONLY public.f34 ADD CONSTRAINT sdf FOREIGN KEY (f34256) REFERENCES public.fj349_r435h(id) NOT VALID;')
    'public.fj349_r435h'
    >>> _parse_fk_ref_table('ALTER TABLE ONLY f34 ADD CONSTRAINT SDF FOREIGN KEY (g456) REFERENCES foo.gh456_ert(id) NOT VALID;')
    'foo.gh456_ert'
    '''
    p = json.loads(pglast.parser.parse_sql_json(statement))
    ref = safe_get(p, 'stmts', 0, 'stmt', 'AlterTableStmt', 'cmds', 0, 'AlterTableCmd', 'def', 'Constraint', 'pktable')
    if ref:
        if 'schemaname' in ref:
            return '%s.%s' % (ref['schemaname'], ref['relname'])
        return ref['relname']

def _get_fk_reference_module(fk_statement, nix_paths=os.environ.get('NIX_PATH')):
    tableref = _parse_fk_ref_table(fk_statement)
    if not tableref:
        return
    files = file_search(r'CREATE\s+.*TABLE\s+%s\W' % tableref, nix_paths.split(':'), 'sql', case_sensitive=False)
    for file_match in files:
        mod_name = os.path.dirname(file_match)
        mod_metadata = _inspect(mod_name, scm_sandbox_mode='declarative')
        if mod_metadata and mod_metadata['meta']['drv_obj']['env'].get('scm_type') == 'schema':
            return mod_name

def _is_create_trigger(statement):
    '''
    >>> _is_create_trigger('CREATE TRIGGER w AFTER INSERT ON q.z FOR EACH ROW EXECUTE FUNCTION x.y();')
    True
    '''
    p = json.loads(pglast.parser.parse_sql_json(statement))
    return bool(safe_get(p, 'stmts', 0, 'stmt', 'CreateTrigStmt'))

def _is_create_index(statement):
    '''
    >>> _is_create_index('CREATE INDEX revision_date_idx ON meta.revision USING btree (date);')
    True
    '''
    p = json.loads(pglast.parser.parse_sql_json(statement))
    return bool(safe_get(p, 'stmts', 0, 'stmt', 'IndexStmt'))

def _is_add_constraint(statement):
    '''
    >>> _is_add_constraint('ALTER TABLE country ADD CONSTRAINT country_id_upper CHECK (id = upper(id));')
    True
    >>> _is_add_constraint('ALTER TABLE country ADD CONSTRAINT country_id_pkey PRIMARY KEY (id);')
    True
    >>> _is_add_constraint('CREATE INDEX county_id_ix ON public.country USING btree(id);')
    False
    >>> _is_add_constraint('ALTER TABLE country DROP COLUMN id;')
    False
    '''
    p = json.loads(pglast.parser.parse_sql_json(statement))
    for cmd in safe_get(p, 'stmts', 0, 'stmt', 'AlterTableStmt', 'cmds') or []:
        if safe_get(cmd, 'AlterTableCmd', 'def', 'Constraint'):
            return True
    return False


def _eq_pglast_name(name, struct):
    '''
    >>> _eq_pglast_name('public.wombat', [{'String': {'str': 'public'}}, {'String': {'str': 'wombat'}}])
    True
    >>> _eq_pglast_name('wombat', [{'String': {'str': 'public'}}, {'String': {'str': 'wombat'}}])
    True
    >>> _eq_pglast_name('foo.wombat', [{'String': {'str': 'public'}}, {'String': {'str': 'wombat'}}])
    False
    >>> _eq_pglast_name('foo.wombat', [{'String': {'str': 'wombat'}}])
    True
    '''
    np1 = name.split('.')[::-1]
    np2 = [safe_get(x, 'String', 'str') for x in struct or []][::-1]
    return all([x == y for (x, y) in zip(np1, np2)])


def _is_create_func_table_arg(statement, table):
    '''
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(r public.wombat) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(public.wombat) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(public.wombat, foo) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(x public.wombat, foo) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(foo, public.wombat) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(foo, x public.wombat) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(integer) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    False
    '''
    try:
        p = json.loads(pglast.parser.parse_sql_json(statement))
    except pglast.parser.ParseError:
        # TODO: fix parser in pglast; happens when $ is used inside a function body to refer to a positional argument  # pylint: disable=fixme
        print(('failed to parse %s' % (statement.replace('\n', '')))[:120] + '', file=sys.stderr)
        return
    for param in safe_get(p, 'stmts', 0, 'stmt', 'CreateFunctionStmt', 'parameters') or []:
        if _eq_pglast_name(table, safe_get(param, 'FunctionParameter', 'argType', 'names')):
            return True
    return False

def _is_create_func_table_ret(statement, table):
    '''
    >>> _is_create_func_table_ret('CREATE FUNCTION public.squawk() RETURNS public.wombat LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_ret('CREATE FUNCTION public.squawk(integer) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    False
    >>> _is_create_func_table_ret('CREATE FUNCTION public.squawk() RETURNS integer LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    False
    >>> _is_create_func_table_ret('CREATE FUNCTION public.squawk(integer) RETURNS wombat LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    '''
    try:
        p = json.loads(pglast.parser.parse_sql_json(statement))
    except pglast.parser.ParseError:
        # TODO: fix parser in pglast; happens when $ is used inside a function body to refer to a positional argument  # pylint: disable=fixme
        print(('failed to parse %s' % (statement.replace('\n', '')))[:120] + '', file=sys.stderr)
        return
    return _eq_pglast_name(table, safe_get(p, 'stmts', 0, 'stmt', 'CreateFunctionStmt', 'returnType', 'names'))

def _find_table_funcs(src_text, table):
    '''
    Return create function statements whose type signature depends on the given table.
    '''
    for ddl_statement in parse_ddl_statements(src_text):
        if _is_create_func_table_arg(ddl_statement, table) or _is_create_func_table_ret(ddl_statement, table):
            yield ddl_statement

def _factorize_log_statement(prefix, statement):
    print(('%s: %s' % (prefix, statement.replace('\n', '')))[:120] + '', file=sys.stderr)

@main.command()
@click.argument('schema', shell_complete=autocomplete_path)
@click.option('-t', '--table', help='Regexp search for server to attach with -a, or the name of a new server.')
@click.option('-r', '--revision', is_flag=True, help='Create initial revisions for new schemas.')
@click.option('--keep-failed', is_flag=True, help='Keep changes even when a schema fails to build.')
def factorize(schema, table, revision, keep_failed):
    '''
    Create one new schema per table present in @schema, removing tables and related objects from schema's upgrade.sql.

    WARNING: mutates source schema's upgrade.sql, ensure that it's backed up.

    Removes the table definitions and related objects from the schema's declarative definition, creates a new schema
    with these definitions, and adds a dependency to the new schema.

    This "factorize" utility is a proof of concept, pre-alpha.

    TODO: would save a bunch of manual work if it automatically determined dependencies between the new schemas. This
    would most likely be done by inspecting the catalog tables of the sandbox database created for the parent schema.
    '''
    upgrade_sql = os.path.join(get_nix_module_dir(schema), 'upgrade.sql')
    schema_metadata = _inspect(schema, scm_sandbox_mode='declarative')
    schema_deps = list(filter(
        None, [drvobj_to_nixref(d) for d in get_immediate_deps(schema_metadata, scm_types=('schema',))]))
    tablenames = [table] if table else list(parse_create_table_names(open(upgrade_sql).read()))
    try:
        drv = _sandbox(schema, scm_sandbox_mode='declarative', verbose=False)
    except subprocess.CalledProcessError:
        print('%s: schema build failed' % schema, file=sys.stderr)
        sys.exit(0)
    sandbox_uri = pguri_from_basedir(drv['env']['basedir'])
    for tablename in tablenames:
        if not tablename.strip():
            continue
        statement_matches = []
        failed_match = did_funcs = False
        src_text = open(upgrade_sql).read()
        for ddl_statement in parse_ddl_statements(get_pg_dump(sandbox_uri, tablename)):
            if _is_create_trigger(ddl_statement):
                '''
                Two reasons for skipping triggers:
                1. They depend on functions which aren't included in the pg_dump output for a given table (this could
                be worked around).
                2. There's a judgement call for which schema trigger might belong, so skipping leaves it to a human.
                '''
                continue
            if not did_funcs and (_is_create_index(ddl_statement) or _is_add_constraint(ddl_statement)):
                # constraints and indices may depend on functions, let's add the functions first
                for func_statement in _find_table_funcs(src_text, tablename):
                    statement_matches.append(func_statement)
                    _factorize_log_statement('MATCHED', func_statement)
                did_funcs = True
            match_statement = find_statement_match(src_text, ddl_statement)
            if not match_statement:
                failed_match = True
                _factorize_log_statement('UNMATCHED', ddl_statement)
                continue
            _factorize_log_statement('MATCHED', match_statement)
            statement_matches.append(match_statement)
        if failed_match and not keep_failed:
            print('skipping %s because of failed statement matches' % tablename, file=sys.stderr)
            continue
        if statement_matches:
            newschema = create_schema(_schemaname(tablename))
            newschema_deps = []
            with open(os.path.join(get_nix_module_dir(newschema), 'upgrade.sql'), 'a') as new_upgrade_sql:
                for sttmnt in statement_matches:
                    new_upgrade_sql.write(sttmnt)
                    if not sttmnt.endswith('\n'):
                        new_upgrade_sql.write('\n')
                    src_text = src_text.replace(sttmnt, '')
                    fk_ref_module = _get_fk_reference_module(sttmnt)
                    if fk_ref_module:
                        if get_nix_module(fk_ref_module) != get_nix_module(schema):
                            _factorize_log_statement('INFERRED', '%s depends on %s' % (newschema, fk_ref_module))
                            newschema_deps.append(fk_ref_module)
            if newschema_deps:
                update_schema_deps(get_nix_module(newschema), newschema_deps)
            # build new schema
            new_basedir = None
            try:
                drv = _sandbox(newschema, scm_sandbox_mode='declarative', verbose=False)
                new_basedir = drv['env']['basedir']
            except subprocess.CalledProcessError:
                print('%s: schema build failed' % newschema, file=sys.stderr)
            create_revision(newschema, verbose=True, auto=False) if new_basedir and revision else None
            if new_basedir or keep_failed:
                # update default.nix to add a dependency on the new schema
                schema_deps.append(get_nix_module(newschema))
                update_schema_deps(get_nix_module(schema), schema_deps)
                # update upgrade.sql to remove the extracted statements
                with open(upgrade_sql, 'w') as fp:
                    fp.write(src_text)
            shutil.rmtree(get_nix_module_dir(newschema)) if not new_basedir and not keep_failed else None
            _gc_basedir(new_basedir, stop=True) if new_basedir else None

def nix_path_list(nix_path=os.environ.get('NIX_PATH')):
    ret = []
    for path in nix_path.split(':'):
        if '=' in path:
            _, _, path = path.partition('=')
        ret.append(path)
    return ret

def file_search(pattern, paths, extension, case_sensitive=True):
    paths = list(set(paths))
    cmd = ['grep', '-Erl', '--include', '*.%s' % extension]
    if not case_sensitive:
        cmd = cmd + ['-i']
    try:
        output = subprocess.check_output(cmd + [pattern] + paths, text=True)  # pylint: disable=unexpected-keyword-arg
    except subprocess.CalledProcessError as ex:
        if ex.returncode != 1:
            raise
        return []
    for line in output.split('\n'):
        if os.path.isfile(line):
            yield line

def replace_path_name(source_name, dest_name, content):
    '''
    >>> source_name = '2020-03-17-kighluinbimfoobi-account'
    >>> dest_name = 'account-2020-03-17-kighluinbimfoobi'
    >>> replace_path_name(source_name, dest_name, 'dependencies = [ <2020-03-17-kighluinbimfoobi-account> ]; ')
    'dependencies = [ <account-2020-03-17-kighluinbimfoobi> ]; '
    >>> replace_path_name(source_name, dest_name, 'dependencies = [ <rev/2020-03-17-kighluinbimfoobi-account> ]; ')
    'dependencies = [ <rev/account-2020-03-17-kighluinbimfoobi> ]; '
    >>> replace_path_name(source_name, dest_name, 'dependencies = [ ./rev/2020-03-17-kighluinbimfoobi-account ]; ')
    'dependencies = [ ./rev/account-2020-03-17-kighluinbimfoobi ]; '
    >>> replace_path_name(source_name, dest_name, 'name = "2020-03-17-kighluinbimfoobi-account"; ')
    'name = "account-2020-03-17-kighluinbimfoobi"; '
    >>> replace_path_name(source_name, dest_name, 'name = "2020-03-17-kighluinbimfoobi-account"; ')
    'name = "account-2020-03-17-kighluinbimfoobi"; '
    '''
    content = re.sub(r'\<%s\>' % source_name, '<%s>' % dest_name, content)
    content = re.sub(r'\<((.+/))%s\>' % source_name, lambda m: '<%s%s>' % (m.group(1), dest_name), content)
    content = re.sub(r'"%s"' % source_name, '"%s"' % dest_name, content)
    content = re.sub(r'/%s(\s)' % source_name, '/' + dest_name + r'\1', content)
    return content

@main.command()
@click.argument('source', shell_complete=autocomplete_path)
@click.argument('dest', shell_complete=autocomplete_path)
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
def rename(source, dest, verbose):
    ''' Rename object (pkg, rev, srv, ...) from @source to @dest and update references.
        Updates source files destructively!
    '''
    if not is_local_scm_obj(source):
        print('source is not a schematic object: %s' % source, file=sys.stderr)
        sys.exit(5)
    if not is_local_scm_obj(dest):
        print('dest is not a schematic object: %s' % dest, file=sys.stderr)
        sys.exit(5)
    assert os.path.isdir(source), 'source must be a directory: %s' % source
    source = path_relative(source, '.')
    dest = path_relative(dest, '.')
    source_name = os.path.dirname(source)
    dest_name = os.path.dirname(dest)
    os.rename(source, dest)
    for filename in file_search(r'\W%s\W' % source_name, nix_path_list(), 'nix'):
        oldcontent = content = open(filename).read()
        content = replace_path_name(source_name, dest_name, content)
        if oldcontent == content:
            continue
        try:
            with open(filename, 'w') as fp:
                fp.write(content)
            print('updated %s' % filename, file=sys.stderr)
        except PermissionError as ex:
            if ex.errno == 13:
                print('readonly %s' % filename, file=sys.stderr)
                continue
            raise

@main.command(name='fast-forward')
@click.argument('database', shell_complete=autocomplete_path)
@click.argument('path', shell_complete=autocomplete_path)
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
def fast_forward(database, path, verbose):
    '''
    Upgrade a database by skipping revisions and marking them as already applied.

    Takes the target database path followed by a revision to fast-forward to. A schema or database can be passed
    instead of a revision to specify all of its dependent revisions.
    '''
    db_module = get_nix_module(database)
    ff_module = get_nix_module(path)
    assert ff_module is not None, 'not a nix module: %s' % ff_module
    assert db_module is not None, 'not a nix module: %s' % db_module
    environ = {
        'SCM_DATABSE_MODULE': db_module,
        'SCM_FAST_FORWARD_MODULE': ff_module,
        'SCM_FAST_FORWARD': '1',
        'SCM_FAST_FORWARD_GUID': guids.get_database_guid(),
    }
    eval_module(get_relative_nix_mod('lib/fast-forward.nix'), verbose=verbose, environ=environ)

def ensure_nix_installed(fabconn, hostname, installer=os.path.join(env.get_str('SCM_PATH'), 'shell/nix-install.sh')):
    is_installed = bool(fabconn.run('~/.nix-profile/bin/nix-shell --version', hide=True).stdout.strip())
    if is_installed:
        return
    subprocess.check_call(['rsync', installer, '%s:~/var/deploy/nix-install.sh' % hostname])
    try:
        fabconn.run('sh ~/var/deploy/nix-install.sh')
    except invoke.exceptions.UnexpectedExit:
        print('failed to install nix, please install and try again', file=sys.stderr)
        sys.exit(2)

def status_line(line):
    print('\033[0K\r', file=sys.stderr)
    print(line, file=sys.stderr)

@main.command()
@click.argument('hostname')
@click.option('-a', '--attach', is_flag=True, help='Attached to most recent existing tmux server.')
@click.option('-n', '--name', help='Regexp search for server to attach with -a, or the name of a new server.')
@click.option('-s', '--shell', default='default.nix', help='Nix file to use for nix-shell environment.')
def remote(hostname, attach, name, shell):
    '''Sync files to host and open a tmux session to adminster remote databases.'''
    status_line('ssh...')
    c = fabric.Connection(hostname)
    c.run('mkdir -p ~/var/{deploy,tmux}', hide=True)
    if attach:
        status_line('lookup tmux server...')
        socket = c.run('ls -t ~/var/tmux/ | grep -E "%s" | head -n 1' % (name or '')).stdout.strip()
        if not socket:
            status_line('tmux server not found\n')
            sys.exit(1)
    else:
        status_line('rsync...')
        subprocess.check_call([
            'rsync', '-zlrt', '--delete-after', 'pkg', 'rev', 'srv', shell, '%s:~/var/deploy' % hostname])
        socket = '-'.join(filter(None, [datetime.datetime.now().date().isoformat(), name, guids.get_deployment_guid()]))
    socket = os.path.join('~/var/tmux', socket)
    status_line('nix install...')
    ensure_nix_installed(c, hostname)

    def resize_window(sig=None, data=None):  # pylint: disable=unused-argument
        (rows, cols, _, _) = struct.unpack('hhhh', fcntl.ioctl(
            sys.stdout.fileno(), termios.TIOCGWINSZ, struct.pack('HHHH', 0, 0, 0, 0)))
        child.setwinsize(rows, cols)

    child = pexpect.spawn('ssh %s' % hostname)
    signal.signal(signal.SIGWINCH, resize_window)
    child.expect('$', timeout=120)
    child.sendline('cd ~/var/deploy ; export SCM_SOCK=%s' % socket)
    status_line('building nix shell...')
    child.sendline('nix-shell %s --run "tmux -S $SCM_SOCK new -A -s 0 ; scm kill-tmux -p $SCM_SOCK" ; exit' % shell)
    resize_window()
    child.expect(r'\[0\]', timeout=60 * 60 * 24 * 365)
    child.interact()

def tmux_server_is_dead(socket_path):
    try:
        return not subprocess.check_output(['lsof', socket_path]).strip()
    except subprocess.CalledProcessError:
        return True

def tmux_server_clients(socket_path):
    try:
        output = subprocess.check_output(['tmux', '-S', socket_path, 'list-clients'], text=True, stderr=subprocess.PIPE)  # pylint: disable=unexpected-keyword-arg
        return [l.strip() for l in output.split('\n') if l.strip()]
    except subprocess.CalledProcessError:
        return []

def file_open_pids(filepath):
    '''Get pids that have a file open.'''
    try:
        output = subprocess.check_output(['lsof', '-F', 'p', filepath], text=True, stderr=subprocess.PIPE)  # pylint: disable=unexpected-keyword-arg
    except subprocess.CalledProcessError:
        return []
    pids = []
    for line in output.split('\n'):
        match = re.match(r'p(\d+)', line)
        if match:
            pids.append(int(match.group(1)))
    return pids

def remove_tmux_socket(socket_path):
    if tmux_server_is_dead(socket_path):
        print('removing socket %s' % unexpand_user(socket_path), file=sys.stderr)
        try:
            subprocess.check_call(['rm', socket_path])
        except subprocess.CalledProcessError:
            pass

@main.command(name='kill-tmux')
@click.argument('socket_path', shell_complete=autocomplete_path)
@click.option('-p', '--prompt', is_flag=True, help='Prompt for confirmation before killing processes.')
@click.option('-f', '--force', is_flag=True, help='Kill even if there are attached clients.')
def kill_tmux(socket_path, prompt, force):
    '''Kill and clean up a tmux server.'''
    if not os.path.exists(socket_path):
        print('file not found %s' % socket_path, file=sys.stderr)
        sys.exit(1)
    clients = tmux_server_clients(socket_path)
    if clients and not force:
        print('%d client(s) still attached to tmux server %s' % (len(clients), unexpand_user(socket_path)), file=sys.stderr)
        sys.exit(2)
    if prompt and not tmux_server_is_dead(socket_path):
        consent = prompt_yesno('kill tmux server %s?' % unexpand_user(socket_path))
        if not consent:
            sys.exit(0)
    pids = file_open_pids(socket_path)
    if pids:
        subprocess.check_call(['kill'] + list(map(str, pids)))
    remove_tmux_socket(socket_path)



def _unique_list(thelist):
    suppress = set()
    for item in thelist:
        if item in suppress:
            continue
        suppress.add(item)
        yield item

@main.command(name='mknixpath')
def mknixpath():
    ''' Read SCM_REPOS env var, iterate sub-directories and output a value for NIX_PATH. '''
    scm_repos = env.get_str('SCM_REPOS', '')
    nix_paths = []
    for scm_repo in scm_repos.split(':'):
        if not scm_repo:
            continue
        if not os.path.exists(scm_repo):
            print('repo does not exist: %s' % scm_repo, file=sys.stderr)
            continue
        scm_repo = os.path.realpath(scm_repo)
        if not os.path.isdir(scm_repo):
            print('repo is not a directory: %s' % scm_repo, file=sys.stderr)
            continue
        for subpath in os.listdir(scm_repo):
            abspath = os.path.join(scm_repo, subpath)
            if os.path.isdir(abspath):
                nix_paths.append(abspath)
    nix_path = env.get_str('NIX_PATH')
    if nix_path:
        nix_paths.extend(nix_path.split(':'))
    print(':'.join(list(_unique_list(nix_paths))))

@main.command(name='oplog-ls')
@click.argument('s3uri')
@click.option('-t', '--tsn', type=str, help='Eg 00011AE4904CEB18. Exclude files that are before this TSN. If the TSN falls between two file names, include the previous file (treating it as a range).')  # pylint: disable=line-too-long
def ol_ls(s3uri, tsn):
    ''' List oplog files, optionally offset from a tsn.
        :s3uri: eg s3://some-bucket/SUBDIR/. Lists all files in SUBDIR.
        :tsn: eg 00011AE4904CEB18. Exclude files that are before this TSN.
            If the TSN falls between two file names, include the previous file (treating it as a range).
    '''
    for olf in oplog_ingress.ls(s3uri, tsn=tsn):
        print('%12.d bytes    %s    %s' % (olf.size, olf.modified, olf.path))

@main.command('oplog-ingress')
@click.argument('s3uri')
@click.argument('pguri')
@click.option('--tsn', type=str, help='Skip to this TSN. Useful for debugging. Do not use for production databases as it will create inconsistencies that are impractical to fix.')  # pylint: disable=line-too-long
@click.option('--replication-role', help='Sets PostgreSQL session_replication_role setting.', type=click.Choice(['replica', 'origin', 'local']), default='replica')  # pylint: disable=line-too-long
@click.option('--time-limit', help='Stop processing new files after running this many seconds.', type=int, default=0)
def ol_ingress(s3uri, pguri, tsn, replication_role, time_limit):
    ''' Receive oplog update feed into postgresql.
        :s3uri: eg s3://some-bucket/some-prefix
        :pguri: eg postgresql://user:pass@localhost:port/dbname?application_name=oplog-ingress
    '''
    try:
        oplog_ingress.ingress(s3uri, pguri, tsn=tsn, replication_role=replication_role, time_limit=time_limit)
    except (psycopg2.errors.AdminShutdown, psycopg2.OperationalError) as exc:  # pylint: disable=no-member
        print('error: %s' % type(exc).__name__, file=sys.stderr)
        for arg in exc.args:
            print('%s' % arg, file=sys.stderr)
        sys.exit(1)
    except psycopg2.errors.InsufficientPrivilege:  # pylint: disable=no-member
        slog.error2(f'replication role {replication_role} requires superuser')
        slog.error2((
            'hint: replication role "replica" is faster because triggers and foreign key constraints are '
            'skipped, but replication role "origin" can be used without superuser access.'))
        sys.exit(2)

def _sync(src, dst, yaml, replication_role, slotname, pubname, subname, time_limit, onconflict):
    tablesubs = pgyaml.cons_tablesubs(yaml)
    with pgcn.connect(src) as pg_src, pgcn.connect(dst) as pg_dst:
        pgsync.sync(
            pg_src, pg_dst, slotname, pubname, subname, tablesubs, replication_role=replication_role,
            time_limit=time_limit, onconflict=onconflict)

@main.command('sync')
@click.argument('src')
@click.argument('dst')
@click.option('--yaml', help='Path to YAML file defining tables to replicate. Use "scm sync-conf" to generate.', type=str, required=True)  # pylint: disable=line-too-long
@click.option('--replication-role', help='Sets PostgreSQL session_replication_role setting.', type=click.Choice(['replica', 'origin', 'local']), default='replica')  # pylint: disable=line-too-long
@click.option('--time-limit', help='Stop processing updates after running this many seconds.', type=int, default=0)
@click.option('--slotname', help='Name of the PostgreSQL replication slot (created on src server).', type=str, required=True)  # pylint: disable=line-too-long
@click.option('--pubname', help='Name of the PostgreSQL publication/replication slot (created on src server). Defaults to slotname.', type=str)  # pylint: disable=line-too-long
@click.option('--subname', help='Name of the PostgreSQL subscription (created at dst server). Defaults to slotname.', type=str)  # pylint: disable=line-too-long
@click.option('--onconflict', help='Opt to skip an update for existing records. Useful for ad-hoc synchronization to backfill missing records, but not touch existing records.', type=click.Choice(['do nothing', 'do update']), default='do update')  # pylint: disable=line-too-long
def sync(src, dst, yaml, replication_role, slotname, pubname, subname, time_limit, onconflict):
    ''' Use logical replication to stream changes from one postgresql database to another.
        Replicates a subset of tables and columns.
        Expects to be run again after it exits. To schedule it on cron, use the `scm cron` command.
        Send signal SIGUSR1 (10) to request a graceful shutdown at the next convenient point.

        :src: eg postgresql://user:pass@localhost:port/dbname
        :dst: eg postgresql://user:pass@localhost:port/dbname
    '''
    try:
        _sync(src, dst, yaml, replication_role, slotname, pubname or slotname, subname or slotname, time_limit,
              onconflict)
    except (psycopg2.errors.AdminShutdown, psycopg2.OperationalError) as exc:  # pylint: disable=no-member
        slog.error2('error: %s, %s, %r', exc, exc, exc.args)
        raise SystemExit(1)
    except psycopg2.errors.InsufficientPrivilege:  # pylint: disable=no-member
        slog.error2(f'replication role {replication_role} requires superuser')
        slog.error2((
            'hint: replication role "replica" is faster because triggers and foreign key constraints are '
            'skipped, but replication role "origin" can be used without superuser access.'))
        sys.exit(2)

@main.command('sync-conf')
@click.argument('pguri')
@click.option('-o', '--out', type=str, default='/dev/stdout', help='Path to output file')
@click.option('-n', '--namespaces', type=str, default='public', help='Comma-separated list of namespaces to include')
@click.option('-c', '--columns', is_flag=True, help='Include column list (optional; useful for excluding columns)')
def sync_conf(pguri, out, namespaces, columns):
    ''' Read tables from a database and generate a yaml file used by 'scm sync' for replication subscriptions.
        :pguri: eg postgresql://user:pass@localhost:port/dbname

        Format of output file is a list of records with fields:
        "namespace": table namespace (aka "schema")
        "tablename": name of table
        "initcopy": whether to perform an initial COPY export.
        "included": pass false to exclude this table from the subscription
        "inc_columns": a list of column names to include
            non-empty list will cause unnamed columns to be omitted
            an empty list will include all columns (including ones created during processing), except exc_columns
        "exc_columns": a list of column names to omit
        "sql_where": a text string to be used as a WHERE clause to filter the table for initial copy
            this is only for efficiency optimizations for initial COPY exports, it does not apply to incremental changes
    '''
    try:
        with pgcn.connect(pguri) as pg:
            pgyaml.write_schema_yaml(pg, namespaces=namespaces.split(','), out=out, inc_columns=columns)
    except (psycopg2.errors.AdminShutdown, psycopg2.OperationalError) as exc:  # pylint: disable=no-member
        slog.error2('error: %s, %s, %r', exc, exc, exc.args)
        sys.exit(1)

def escape_shell_cmd(cmd):
    r''' Escape shell :cmd: so that it can be wrapped in double quotes.

    >>> escape_shell_cmd('echo hi')
    'echo hi'
    >>> escape_shell_cmd("echo 'hi'")
    "echo 'hi'"
    >>> escape_shell_cmd('echo "hi"')
    'echo \\"hi\\"'
    '''
    return cmd.replace('"', '\\"')

def update_crontab(text, scriptpath, schedule):
    r''' Update or add a crontab line to a crontab file.
    >>> update_crontab('', 'example.sh', '* * * * *')
    '* * * * * example.sh # added by schematic\n'
    >>> update_crontab('* * * * * example.sh # added by schematic\n', 'example.sh', '* * * * *')
    '* * * * * example.sh # added by schematic\n'
    >>> update_crontab('* * * * * example2.sh # added by schematic\n', 'example.sh', '* * * * *')
    '* * * * * example2.sh # added by schematic\n* * * * * example.sh # added by schematic\n'
    >>> update_crontab('* * * * * example.sh\n', 'example.sh', '* * * * *')
    '* * * * * example.sh\n* * * * * example.sh # added by schematic\n'
    '''
    comment = '# added by schematic'
    ret = ''
    matched = False
    newcronline = '%s %s %s\n' % (schedule, scriptpath, comment)
    if text:
        for line in text.rstrip('\n').split('\n'):
            if line.lstrip(' ').startswith('#'):
                ret += '%s\n' % line
                continue
            # the comment serves as an additional heuristic to prevent false positive matches
            if scriptpath in line and comment in line:
                matched = True
                ret += newcronline
                continue
            ret += '%s\n' % line
    if not matched:
        ret += newcronline
    return ret

@main.command('cron')
@click.argument('name')
@click.argument('schedule')
@click.argument('command')
def cron(name, schedule, command):
    ''' Install a crontab task, wrapping the command in shell script with a suitable environment. '''
    validate_name(name)
    crondir = os.path.join(scm_var_path, 'cron')
    mkdir(crondir)
    cronpath = os.path.join(crondir, name)
    root_dir = env.get_str('ROOT_DIR')
    with open(cronpath, 'w') as fp:  # pylint: disable=invalid-name
        fp.write('#! /usr/bin/env bash\n')
        fp.write('PATH=~/.nix-profile/bin:"$PATH"\n')
        fp.write('export PATH\n')
        fp.write('cd "%s"\n' % root_dir)
        fp.write('nix-shell -I ~/.nix-defexpr/channels --command "%s"\n' % escape_shell_cmd(command))
    print('script path %s' % cronpath, file=sys.stderr)
    os.chmod(cronpath, 0o775)  # ensure executable bit is set
    try:
        crontext = subprocess.check_output(['crontab', '-l'], text=True, stderr=subprocess.PIPE)  # pylint: disable=unexpected-keyword-arg
    except subprocess.CalledProcessError as ex:
        if ex.returncode == 1:  # empty crontab
            crontext = ''
        else:
            raise
    newcrontext = update_crontab(crontext, cronpath, schedule)
    crontabproc = subprocess.Popen(
        ['crontab'], stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    (stdout, stderr) = crontabproc.communicate(input=newcrontext)
    if crontabproc.returncode != 0:
        print('error updating crontab!', file=sys.stderr)
        print(stderr, file=sys.stderr) if stderr.strip() else None
        print(stdout, file=sys.stdout) if stdout.strip() else None
        print('crontab text:\n%s' % newcrontext, file=sys.stderr)
        print('hint: try installing the cron line manually using crontab -e', file=sys.stderr)

if __name__ == '__main__':
    main()
