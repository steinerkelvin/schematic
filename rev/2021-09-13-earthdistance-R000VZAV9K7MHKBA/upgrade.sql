CREATE EXTENSION IF NOT EXISTS earthdistance WITH SCHEMA public;
COMMENT ON EXTENSION earthdistance IS 'calculate great-circle distances on the surface of the Earth';

