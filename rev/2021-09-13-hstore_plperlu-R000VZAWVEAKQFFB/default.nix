stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAWVEAKQFFB";
    name = "2021-09-13-hstore_plperlu";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-plperlu-R000VZAWR2OODR64>
        <2021-09-13-hstore-R000VZAVDPM02V78>
    ];
}
