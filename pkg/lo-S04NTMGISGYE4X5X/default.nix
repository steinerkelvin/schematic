stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S04NTMGISGYE4X5X";
    name = "lo";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-lo-R000VZAV1OS2WI7V>
    ];
}
