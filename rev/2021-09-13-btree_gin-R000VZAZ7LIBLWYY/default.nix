stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAZ7LIBLWYY";
    name = "2021-09-13-btree_gin";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
