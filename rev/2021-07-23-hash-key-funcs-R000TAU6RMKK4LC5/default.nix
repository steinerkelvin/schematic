stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000TAU6RMKK4LC5";
    name = "2021-07-23-hash-key-funcs";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-11-16-smallint_bit-R000GHY1QOY4QC9J>
    ];
}
