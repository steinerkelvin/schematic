stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S01GO0R28P6T2HZR";
    name = "unaccent";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-unaccent-R000VZA11GNZRS8Y>
    ];
}
