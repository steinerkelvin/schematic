stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0PFGNEK5Z58PRUE";
    name = "pg_prewarm";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-pg_prewarm-R000VZAYZVPKU3L7>
    ];
}
