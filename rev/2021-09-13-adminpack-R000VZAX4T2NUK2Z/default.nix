stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAX4T2NUK2Z";
    name = "2021-09-13-adminpack";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
