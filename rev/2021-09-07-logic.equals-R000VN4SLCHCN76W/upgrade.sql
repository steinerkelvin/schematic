CREATE OR REPLACE FUNCTION logic.eq(var1 anyelement, var2 anyelement) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS NOT DISTINCT FROM $2;
$function$;

COMMENT ON FUNCTION logic.eq(var1 anyelement, var2 anyelement) IS 'Shorter version of IS NOT DISTINCT FROM operator.';

CREATE OPERATOR logic.== (
    LEFTARG = anyelement,
    RIGHTARG = anyelement,
    FUNCTION = logic.eq,
    COMMUTATOR = OPERATOR(logic.==),
    NEGATOR = OPERATOR(logic.!==)
);

CREATE OR REPLACE FUNCTION logic.eq(var1 text, var2 text) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS NOT DISTINCT FROM $2;
$function$;

COMMENT ON FUNCTION logic.eq(var1 text, var2 text) IS 'Shorter version of IS NOT DISTINCT FROM operator.';

CREATE OPERATOR logic.== (
    LEFTARG = text,
    RIGHTARG = text,
    FUNCTION = logic.eq,
    COMMUTATOR = OPERATOR(logic.==),
    NEGATOR = OPERATOR(logic.!==)
);

CREATE OR REPLACE FUNCTION logic.not_eq(var1 anyelement, var2 anyelement) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS DISTINCT FROM $2;
$function$;

COMMENT ON FUNCTION logic.not_eq(var1 anyelement, var2 anyelement) IS 'Shorter version of IS DISTINCT FROM operator.';

CREATE OPERATOR logic.!== (
    LEFTARG = anyelement,
    RIGHTARG = anyelement,
    FUNCTION = logic.not_eq,
    COMMUTATOR = OPERATOR(logic.!==),
    NEGATOR = OPERATOR(logic.==)
);

CREATE OR REPLACE FUNCTION logic.not_eq(var1 text, var2 text) RETURNS bool
LANGUAGE sql CALLED ON NULL INPUT IMMUTABLE PARALLEL SAFE LEAKPROOF AS $function$
    SELECT $1 IS DISTINCT FROM $2;
$function$;

COMMENT ON FUNCTION logic.not_eq(var1 text, var2 text) IS 'Shorter version of IS DISTINCT FROM operator.';

CREATE OPERATOR logic.!== (
    LEFTARG = text,
    RIGHTARG = text,
    FUNCTION = logic.not_eq,
    COMMUTATOR = OPERATOR(logic.!==),
    NEGATOR = OPERATOR(logic.==)
);
