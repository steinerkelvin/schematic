stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S01PUCMWJSIHZKQA";
    name = "fuzzystrmatch";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-fuzzystrmatch-R000VZAXTVVLUSVD>
    ];
}
