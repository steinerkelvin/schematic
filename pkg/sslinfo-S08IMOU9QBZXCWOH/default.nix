stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S08IMOU9QBZXCWOH";
    name = "sslinfo";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-sslinfo-R000VZAWJKA3L16O>
    ];
}
