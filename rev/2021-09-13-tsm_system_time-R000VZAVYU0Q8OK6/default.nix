stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAVYU0Q8OK6";
    name = "2021-09-13-tsm_system_time";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
