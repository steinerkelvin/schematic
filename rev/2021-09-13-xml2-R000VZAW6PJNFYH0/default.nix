stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAW6PJNFYH0";
    name = "2021-09-13-xml2";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
