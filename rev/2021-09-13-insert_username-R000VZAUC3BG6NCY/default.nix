stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAUC3BG6NCY";
    name = "2021-09-13-insert_username";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
