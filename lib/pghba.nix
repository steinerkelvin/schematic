stdargs @ { scm, pkgs, stdenv, postgresql, python3, rsync, ... }:
args @ {
    guid, name, rules, comment ? null,
    dependencies ? [], preBuildInputs ? null,
    ...
}:

rec {
    scm_type = "pghba";
    inherit guid name dependencies preBuildInputs comment rules;
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    apply = { self, server }: scm.effect rec {
        inherit (server) postgresql pguri port basedir;
        inherit (self) guid name scm_type transDepGuids comment;
        rules = builtins.toJSON self.rules;
        SCM_EFFECT_PROG = ../py/schematic/build_pghba.py;
        buildInputs = [ rsync ] ++ (scm.buildDeps server self);
    };
}
