stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAST87XJD9L";
    name = "2021-09-13-uuid-ossp";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
