import json
import re
import sys

msg_call_sign = 'gadyeapcatlyophnoafhewvagsitjeno'
read_reg = re.compile(r'%s (?P<msg>.+)\n' % msg_call_sign)

def send(data):
    assert 'type' in data, 'message requires type field'
    assert re.match(r'[a-z]{12}\-[a-z\-]+', data['type']), 'invalid message type'
    print('%s %s\n' % (msg_call_sign, json.dumps(data)), file=sys.stdout)

def read(line):
    if not line.startswith(msg_call_sign):
        return
    match = read_reg.match(line)
    return json.loads(match.groupdict()['msg'])
