stdargs @ { scm, pkgs, rsync, ... }:
args @ {
    guid, name, upgrade_sql, basefiles ? null, autocommit ? false,
    dependencies ? [], preBuildInputs ? null,
    ...
}:

rec {
    scm_type = "schema";
    inherit guid name dependencies upgrade_sql preBuildInputs basefiles autocommit;
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    apply = { self, server }: scm.effect rec {
        inherit (server) postgresql pguri port basedir SCM_SANDBOX_MODE;
        inherit (self) guid transDepGuids upgrade_sql name scm_type basefiles autocommit ;
        SCM_EFFECT_PROG = ../py/schematic/build_schema.py;
        buildInputs = [ rsync ] ++ (scm.buildDeps server self);
    };
}
