stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAXXLV2LU4T";
    name = "2021-09-13-refint";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
