stdargs @ { scm, ... }:

scm.revision {
    guid = "R001BM4UUL062LK3";
    name = "2022-07-14-immutable_random";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-11-17-immutable_random-R000GK2EQWJERQPL>
    ];
}
