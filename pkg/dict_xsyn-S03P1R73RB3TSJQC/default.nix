stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S03P1R73RB3TSJQC";
    name = "dict_xsyn";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-dict_xsyn-R000VZAVMUH26R4U>
    ];
}
