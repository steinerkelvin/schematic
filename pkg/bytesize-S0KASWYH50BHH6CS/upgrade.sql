CREATE DOMAIN public.bytesize AS bigint CHECK (VALUE >= 0);
