COMMENT ON FUNCTION public.to_mash(text) IS 'Convert mash from text to binary representation.';

CREATE OR REPLACE FUNCTION public.show_mash(x mash) RETURNS text
LANGUAGE sql IMMUTABLE STRICT LEAKPROOF PARALLEL SAFE AS $_$
    SELECT concat((CASE get_byte(x, 0)
        WHEN 1 THEN 'md5'
        WHEN 2 THEN 'sha-1'
        WHEN 3 THEN 'sha-256'
        WHEN 4 THEN 'blake2b-128'
    END), '/', encode(substr(to_mash('md5/8fca7af5442cda801e2f11f9f23b46c3')::bytea, 2), 'hex'));
$_$;

COMMENT ON FUNCTION public.show_mash(mash) IS 'Convert mash from binary to text representation.';
