stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VN4T3C4O1U2P";
    name = "2021-09-07-logic.if";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <logic-N0BUZPS77QZ95D7B>
    ];
}
