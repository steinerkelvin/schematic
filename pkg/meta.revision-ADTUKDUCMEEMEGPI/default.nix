stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "ADTUKDUCMEEMEGPI";
    name = "meta.revision";
    upgrade_sql = ./upgrade.sql;
    # NB: revisions of the revision schema must define `add_meta_revision = false;` to prevent self-dependency
    dependencies = [
        <meta-N0FLM2YWXLMJE5BC>
        <2020-03-13-meta.revision-UDEJFOTFOSEOWRAI>
    ];
}
