stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S06C4E9A2GNXTHO8";
    name = "earthdistance";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <cube-S0BUHIMSSV0OHB86>
        <2021-09-13-earthdistance-R000VZAV9K7MHKBA>
    ];
}
