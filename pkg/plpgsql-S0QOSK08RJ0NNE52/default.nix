stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0QOSK08RJ0NNE52";
    name = "plpgsql";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-plpgsql-R000VZAYM707ZXOB>
    ];
}
