stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0YDQ1QT6TLS7SKJ";
    name = "citext";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-citext-R000VZAUXW4CG57A>
    ];
}
