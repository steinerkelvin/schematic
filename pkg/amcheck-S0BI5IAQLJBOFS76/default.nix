stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0BI5IAQLJBOFS76";
    name = "amcheck";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-amcheck-R000VZAXGVK12FIM>
    ];
}
