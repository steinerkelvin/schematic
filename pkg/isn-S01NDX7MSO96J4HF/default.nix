stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S01NDX7MSO96J4HF";
    name = "isn";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-isn-R000VZAWNZFP3OJR>
    ];
}
