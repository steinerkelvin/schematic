import os
import sys

import psycopg2
from psycopg2.sql import SQL, Identifier, Literal

from schematic import env, pgcn
from schematic.build_util import mkdir

def create_tablespace(pg, name, location, options):
    print('tablespace %s create, location %s' % (name, location), file=sys.stdout)
    with_options = SQL('WITH ({options})').format(options=SQL(', ').join([
        SQL('%s = {val}' % opt).format(val=Literal(val)) for (opt, val) in options.items()]))
    try:
        return pg.execute(SQL('''
            CREATE TABLESPACE {name} LOCATION {location} {with_options};
        ''').format(
            name=Identifier(name),
            location=Literal(location),
            with_options=with_options if options else SQL(''),
        ))
    except psycopg2.errors.InsufficientPrivilege:  # pylint: disable=no-member
        print('tablespace %s creation failed; check that %s is owned by the same user and group that runs postgresql' %
              (name, location), file=sys.stdout)
        raise

def alter_tablespace_reset(pg, name, options, existing_options):
    remopts = [opt for opt in existing_options if opt not in options]
    if remopts:
        print('tablespace %s reset %s' % (name, ', '.join(remopts)), file=sys.stdout)
        return pg.execute(SQL('''
            ALTER TABLESPACE {name} RESET ({options});
        ''').format(
            name=Identifier(name),
            options=SQL(', ').join([SQL(opt) for opt in remopts]),
        ))

def alter_tablespace_set(pg, name, options, existing_options):
    newopts = {opt: options[opt] for opt in options if options[opt] != existing_options.get(opt)}
    if newopts:
        print('tablespace %s set %r' % (name, ', '.join(['%s=%s' % x for x in newopts.items()])),
              file=sys.stdout)
        return pg.execute(SQL('''
            ALTER TABLESPACE {name} SET ({options});
        ''').format(
            name=Identifier(name),
            options=SQL(', ').join([SQL('%s = {val}' % opt).format(val=Literal(v)) for (opt, v) in newopts.items()]),
        ))

def set_tablespace_comment(pg, name, comment):
    existing_comment = pg.execute('''
        SELECT shobj_description(oid, 'pg_tablespace')
        FROM pg_catalog.pg_tablespace
        WHERE spcname = %s;
    ''', (name,)).scalar()
    if existing_comment != comment:
        print('tablespace %s comment %r' % (name, comment), file=sys.stdout)
        return pg.execute(SQL('COMMENT ON TABLESPACE {name} IS {comment};').format(
            name=Identifier(name), comment=Literal(comment)))

def setup_tablespace_directory(symlink, location, name):
    if not os.path.exists(location):
        print('mkdir %s' % location, file=sys.stdout)
        try:
            mkdir(location)
        except FileExistsError:
            pass
        except PermissionError:
            print('tablespace %s permission error: %s' % (name, location), file=sys.stderr)
            sys.exit(59)
    if not os.path.exists(symlink):
        print('symlink %s -> %s' % (symlink, location), file=sys.stdout)
        try:
            os.symlink(location, symlink, target_is_directory=True)
        except FileExistsError:
            pass
    symlink_dest = os.path.realpath(symlink).rstrip('/')
    if symlink_dest != location.rstrip('/'):
        print('symlink %s points to %s instead of desired %s' % (symlink, symlink_dest, location), file=sys.stdout)
        print('this change cannot be made with a running database, please stop postgresql and do it manually',
              file=sys.stdout)
        sys.exit(28)

def setup_fake_tablespace_directory(symlink):
    if not os.path.exists(symlink):
        print('mkdir %s' % symlink, file=sys.stdout)
        try:
            mkdir(symlink)
        except FileExistsError:
            pass

def main():
    name = env.get_str('name')
    location = env.get_str('location')
    comment = env.get_str('comment')
    symlink_dir = os.path.join(env.get_str('basedir'), 'tablespaces')
    symlink = os.path.join(symlink_dir, name)
    if not os.path.exists(symlink_dir):
        print('mkdir %s' % symlink_dir, file=sys.stdout)
        mkdir(symlink_dir)
    if env.get_bool('SCM_INSTALL_TABLESPACES'):
        setup_tablespace_directory(symlink, location, name)
    else:  # avoids issues with permissions/missing filesystem mounts/etc for dev/testing
        setup_fake_tablespace_directory(symlink)
        location = symlink
    options = {opt: val for (opt, val) in {
        'seq_page_cost': env.get_str('seq_page_cost'),
        'random_page_cost': env.get_str('random_page_cost'),
        'effective_io_concurrency': env.get_str('effective_io_concurrency'),
        'maintenance_io_concurrency': env.get_str('maintenance_io_concurrency'),
    }.items() if val}
    with pgcn.connect(env.get_str('pguri'), autocommit=True) as pg:
        if pg.execute('SELECT pg_is_in_recovery();').scalar():
            print('read-only replica, skipping tablespace %s' % name, file=sys.stdout)
            return
        existing = pg.execute('SELECT * FROM pg_tablespace WHERE spcname = %s;', (name,)).first()
        if existing:
            existing_options = dict(map(lambda s: s.split('='), existing.spcoptions or ''))
            alter_tablespace_reset(pg, name, options, existing_options)
            alter_tablespace_set(pg, name, options, existing_options)
        else:
            create_tablespace(pg, name, location, options)
        set_tablespace_comment(pg, name, comment)

if __name__ == '__main__':
    main()
