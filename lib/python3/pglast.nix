{ lib
, buildPythonPackage
, setuptools
, pytest
, pytestcov
, perl
, fetchPypi
}:

buildPythonPackage rec {
  pname = "pglast";
  version = "3.4";

  src = fetchPypi {
    inherit pname version;
    sha256 = "d2288d9607097a08529d9165970261c1be956934e8a8f6d9ed2a96d9b8f03fc6";
  };

  propagatedBuildInputs = [ setuptools ];

  checkInputs = [ pytest pytestcov ];

  pythonImportsCheck = [ "pglast" ];

  checkPhase = ''
    pytest
  '';

  meta = with lib; {
    homepage = "https://github.com/lelit/pglast/tree/v3";
    description = "PostgreSQL Languages AST and statements prettifier";
    license = licenses.gpl3Plus;
    maintainers = [ maintainers.marsam ];
  };
}
