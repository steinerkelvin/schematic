stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAYM707ZXOB";
    name = "2021-09-13-plpgsql";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
