#! /usr/bin/env python3

import contextlib
import datetime
import gzip
import itertools
import json
import multiprocessing
import os
import re
import tempfile
import time
from dataclasses import dataclass
from urllib.parse import urlparse

import boto3
import bugsnag
import sh
from botocore.exceptions import ClientError
from schematic import pgcn, pgingest, seqs, slog


class S3AccessError(Exception):
    pass


@dataclass
class OplogFile:
    bucket: str
    path: str
    name: str
    subdir: str
    tsn: str
    table: str
    size: str
    modified: datetime.datetime

def parse_oploguri(oploguri):
    '''
    >>> parse_oploguri('s3://example/foo.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='foo.jsonl.gz', name='foo.jsonl.gz', subdir='', tsn=None, table=None, size=None, modified=None)
    >>> parse_oploguri('s3://example/bar/foo.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='bar/foo.jsonl.gz', name='foo.jsonl.gz', subdir='bar', tsn=None, table=None, size=None, modified=None)
    >>> parse_oploguri('s3://example/playstore/0000A01CBADCBF28.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='playstore/0000A01CBADCBF28.jsonl.gz', name='0000A01CBADCBF28.jsonl.gz', subdir='playstore', tsn='0000A01CBADCBF28', table=None, size=None, modified=None)
    >>> parse_oploguri('s3://example/playstore/00009FF539AF5667-public.play_app.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='playstore/00009FF539AF5667-public.play_app.jsonl.gz', name='00009FF539AF5667-public.play_app.jsonl.gz', subdir='playstore', tsn='00009FF539AF5667', table='public.play_app', size=None, modified=None)
    >>> parse_oploguri('s3://example/00009FF539AF5667-public.play_app.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='00009FF539AF5667-public.play_app.jsonl.gz', name='00009FF539AF5667-public.play_app.jsonl.gz', subdir='', tsn='00009FF539AF5667', table='public.play_app', size=None, modified=None)
    >>> parse_oploguri('s3://ex-ample/sub-1/00011AE4904CEB18')  # pylint: disable=line-too-long
    OplogFile(bucket='ex-ample', path='sub-1/00011AE4904CEB18', name='00011AE4904CEB18', subdir='sub-1', tsn='00011AE4904CEB18', table=None, size=None, modified=None)
    '''
    uri = urlparse(oploguri)
    assert uri.scheme == 's3'
    path = uri.path.lstrip('/') if uri.path else uri.path
    name = os.path.basename(path) if path else None
    subdir = os.path.dirname(path) if path else None
    tsn = re.match(r'([\w\.-]+/)?(?P<tsn>[0-9A-F]{16})([\.\-]|$).*', path) if path else None
    if tsn:
        tsn = tsn.group('tsn')
    table = re.match(r'(\w+/)?[0-9A-F]{16}\-(?P<table>\w+\.\w+)\.*', path) if path else None
    if table:
        table = table.group('table')
    return OplogFile(uri.netloc, path, name, subdir, tsn, table, None, None)

def s3ls_page(bucket, subdir=None, start=None):
    ''' List s3 files, eg s3://bucket/subdir.
        :start: exclude files that are alphabetically before this path/key (relative to bucket, not subdir)
    '''
    s3 = boto3.client('s3')
    try:
        resp = s3.list_objects_v2(Bucket=bucket, StartAfter=start, Prefix=subdir or '')
    except ClientError as ex:
        aws_config = os.path.exists(os.path.expanduser('~/.aws/config'))
        def before_notify(notification):
            notification['extraData']['awsConfigFileExists'] = aws_config
        bugsnag.before_notify(before_notify)
        raise S3AccessError(ex)  # pylint: disable=raise-missing-from
    for k in sorted(resp.get('Contents', []), key=lambda x: x['Key']):
        olfile = parse_oploguri(os.path.join('s3://', resp['Name'], k['Key']))
        if subdir and olfile.subdir != subdir:  # multiple subscriptions can be in one bucket, don't include others
            continue
        olfile.modified = k['LastModified']
        olfile.size = k['Size']
        yield olfile

def s3ls_all(bucket, subdir=None):
    ''' List all oplog files in a bucket and subdir. '''
    start = ''
    for i in range(1, 100):
        page = list(s3ls_page(bucket, subdir=subdir or '', start=start))
        if not page:
            break
        for olfile in page:
            yield olfile
        start = olfile.path

def ls(path, tsn=None):
    ''' List oplog files, optionally offset from a tsn.
        :path: eg s3://some-bucket/SUBDIR. Exclude files not in SUBDIR.
        :tsn: eg 00011AE4904CEB18. Exclude files that are before this TSN.
            If the TSN falls between two file names, include the previous file (treating it as a range).
    '''
    path = re.sub(r'/+$', '/', '%s/' % path)  # add trailing slash
    parsed = parse_oploguri(path)
    assert not parsed.name, f'expected no filename after subdir in {path}, got {parsed.name}, omit it or use tsn param'
    olfiles = list(s3ls_all(parsed.bucket, subdir=parsed.subdir or ''))
    for (olfile, next_olfile) in itertools.zip_longest(olfiles, olfiles[1:]):
        if tsn and next_olfile and tsn >= next_olfile.tsn:
            continue  # we're past this file already
        if tsn and olfile.table and tsn > olfile.tsn:
            continue  # initcopy exports all have same tsn, so we can safely skip this
        yield olfile

def read_jsonlgz(fileobj):
    for line in gzip.GzipFile(fileobj=fileobj):
        yield json.loads(line)

@contextlib.contextmanager
def open_oplog(olfile):
    tmpdir = os.environ.get('SCM_TMPDIR') or os.path.expandvars('$HOME/var/tmp')
    try:
        sh.mkdir('-p', tmpdir)
    except sh.ErrorReturnCode_1:
        slog.error2(f'failed to create temp directory: {tmpdir}')
        slog.error2('hint: set SCM_TMPDIR env var to use a different temp directory')
        raise
    with tempfile.TemporaryFile(dir=tmpdir) as opfp:
        boto3.client('s3').download_fileobj(olfile.bucket, olfile.path, opfp)
        opfp.seek(0)
        yield opfp

def read_oplog(olfile):
    with open_oplog(olfile) as opfp:
        for op in read_jsonlgz(opfp):
            yield op

def logstatus(i, txn, tsn, tts):
    slog.info2(f'cursor {i:10d} ops, txn {txn}, tsn {tsn}, tts {tts}')

def ingress_stream(pg, olfile, start_tsn, column_defs, status_interval: int=5 * 60):
    ''' Download oplog file and concurrently process ops as writes into the database. '''
    txn = tsn = tts = lastlog = None
    stream = itertools.dropwhile(lambda x: start_tsn and x['tsn'] <= start_tsn, read_oplog(olfile))
    i = 0
    for i, op in enumerate(stream, start=1):
        if txn is not None and txn != op['txn']:  # first op of new txn, infer commit
            pgingest.commit_tsn(pg, op['tsn'], tts)  # use op['tsn'] instead of tsn to restart at this op, not prev
        txn, tsn, tts = op['txn'], op['tsn'], op['tts']
        if op['op'] == 'insert':  # use upsert instead of insert in case there's existing data
            pgingest.upsert(pg, op['table'], op['op'], op.get('old'), op['new'], column_defs)
        elif op['op'] == 'update':  # use upsert for idempotence, especially with filtered subscriptions
            pgingest.upsert(pg, op['table'], op['op'], op.get('old'), op['new'], column_defs)
        elif op['op'] == 'delete':
            pgingest.delete(pg, op['table'], op['old'])
        elif op['op'] == 'truncate':
            pgingest.truncate(pg, op['tables'], cascade=op['cascade'], restartid=op['restartid'])
        now = time.time()
        if not lastlog or now - lastlog >= status_interval:
            logstatus(i, txn, tsn, tts)
            lastlog = now
    logstatus(i, txn, tsn, tts)
    pgingest.commit_tsn(pg, tsn, tts)

def initcopy_proc(pguri, replication_role, column_defs, queue, is_done):
    table = None
    with pgcn.connect(pguri) as pg:
        pgingest.init_replication_settings(pg, replication_role)
        while not is_done.is_set() or not queue.empty():
            try:
                batch = queue.get(timeout=.1)
            except multiprocessing.queues.Empty:
                continue
            if not batch:
                continue
            batch = [json.loads(l) for l in batch]
            table = table or batch[0]['table']
            for op in batch:
                assert op['op'] == 'insert', 'expected insert, got %r' % op['op']
                assert (op['table']['namespace'], op['table']['tablename']) == (
                    table['namespace'], table['tablename']), 'table mismatch %r vs %r' % (table, op['table'])
            pgingest.upsert_batch(pg, table, [x['new'] for x in batch], column_defs)
            pg.commit()

def ingress_initcopy(
        pg, olfile, column_defs, replication_role, batch_size=1000, parallelism=50, status_interval: int=5 * 60):
    ''' Import an initcopy file using batching, higher performance than ingress_stream. '''
    txn = tsn = tts = op = lastlog = None
    ops = 0
    is_done = multiprocessing.Event()
    queue = multiprocessing.Queue()
    procs = []
    for i in range(parallelism):
        p = multiprocessing.Process(
            name='scm initcopy %s/%s %s' % (i, parallelism, olfile.name),
            target=initcopy_proc, args=(pg.pguri, replication_role, column_defs, queue, is_done))
        p.start()
        procs.append(p)
    with open_oplog(olfile) as opfp:
        for batch in seqs.batches(gzip.GzipFile(fileobj=opfp), batch_size):
            op = json.loads(batch[0])
            queue.put(batch)
            while queue.qsize() >= parallelism:
                time.sleep(.1)
            ops += len(batch)
            txn, tsn, tts = op['txn'], op['tsn'], op['tts']
            now = time.time()
            if not lastlog or now - lastlog >= status_interval:
                logstatus(ops, txn, tsn, tts)
                lastlog = now
    is_done.set()
    for p in procs:
        p.join()
        assert p.exitcode == 0, 'got non-zero exitcode from initcopy_proc: %r' % p.exitcode
        p.close()
    logstatus(ops, txn, tsn, tts)
    # This logical message is partially a work-around for a bug that prevents the logical replication origin from being
    # updated in pgingest.commit_tsn. The bug occurs because this main session isn't doing any writes of its own,
    # and pg_replication_origin_xact_setup doesn't take effect if no writes have been performed in the same txn.
    pg.execute('''SELECT pg_logical_emit_message(true, 'fujladyoihys-initcopy-finished', %s);''', (olfile.table,))
    pgingest.commit_tsn(pg, tsn, tts)

def is_initcopy_file(olfile):
    ''' Determines if it's an initcopy file, used to determine whether batching can be used. '''
    return bool(olfile.table)

def ingress(s3uri, pguri, tsn=None, replication_role='replica', time_limit=None):
    ''' Receive oplog update feed into postgresql.
        :s3uri: eg s3://some-bucket/some-prefix
        :pguri: eg postgresql://user:pass@localhost:port/dbname?application_name=oplog-ingress
        :tsn: skip to this TSN; useful for debugging; do not use for production databases or risk corrupt data!
        :replication_role: specifies PostgreSQL's session_replication_role setting[1];
            use 'replica' to disable triggers including foreign key constraint validation (requires superuser);
            use 'origin' to enable triggers and foreign key constraint validation
        :time_limit: stop processing new files after running this many seconds

        [1] https://www.postgresql.org/docs/current/runtime-config-client.html#RUNTIME-CONFIG-CLIENT-STATEMENT
    '''

    assert s3uri, 's3uri is required'
    subid = re.match(r's3://[\w-]+/\w+/', s3uri)
    assert subid, 'Unable to parse subscription id from s3uri. Check correctness of provided uri'
    subid = subid.group(0)
    start = time.time()
    with pgcn.connect(pguri) as pg:
        lock = pg.execute('SELECT pg_try_advisory_lock(-1699317507, hashtext(%s));', (subid,)).scalar()
        if not lock:
            slog.info2(f'there is another running process consuming the subscription {subid}')
            raise SystemExit(1)
        start_tsn = pgingest.init_replication_settings(pg, replication_role, subid=subid)
        start_tsn = tsn or start_tsn
        column_defs = pgingest.get_column_defs(pg)
        for olfile in ls(s3uri, tsn=start_tsn):
            slog.info2(f'oplog file {olfile.size:12d} bytes\t {olfile.modified} \t {olfile.path}')
            bugsnag.leave_breadcrumb(f'olfile {olfile.bucket}/{olfile.path}')
            if is_initcopy_file(olfile):
                ingress_initcopy(pg, olfile, column_defs, replication_role)
            else:
                ingress_stream(pg, olfile, start_tsn, column_defs)
            if time_limit and (time.time() - start) > time_limit:
                slog.info2('hit time limit, breaking')
                break
