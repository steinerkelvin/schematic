stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "FURWIAHEDBAYRIJO";
    name = "pg_libphonenumber";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-03-24-pg_libphonenumber-PYIVEKDOCEOSHJOT>
    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
