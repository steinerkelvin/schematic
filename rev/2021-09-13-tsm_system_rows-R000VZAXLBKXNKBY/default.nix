stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAXLBKXNKBY";
    name = "2021-09-13-tsm_system_rows";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
