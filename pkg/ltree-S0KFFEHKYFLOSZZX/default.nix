stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0KFFEHKYFLOSZZX";
    name = "ltree";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-ltree-R000VZAUKUQZ13LE>
    ];
}
