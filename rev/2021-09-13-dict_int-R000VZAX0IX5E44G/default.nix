stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAX0IX5E44G";
    name = "2021-09-13-dict_int";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
