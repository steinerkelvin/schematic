CREATE EXTENSION IF NOT EXISTS pageinspect WITH SCHEMA public;
COMMENT ON EXTENSION pageinspect IS 'inspect the contents of database pages at a low level';
