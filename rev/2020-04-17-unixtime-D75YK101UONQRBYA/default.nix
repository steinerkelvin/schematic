stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "D75YK101UONQRBYA";
    name = "2020-04-17-unixtime";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
