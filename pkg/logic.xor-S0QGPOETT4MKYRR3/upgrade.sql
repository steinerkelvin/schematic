CREATE OR REPLACE FUNCTION logic.xor(VARIADIC boolean[]) RETURNS boolean
LANGUAGE sql IMMUTABLE STRICT LEAKPROOF PARALLEL SAFE AS $_$
    SELECT sum(x::integer) = 1 FROM unnest($1) x;
$_$;

COMMENT ON FUNCTION logic.xor IS 'Compute logical XOR on one or more boolean inputs';
