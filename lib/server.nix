stdargs @ { scm, python3, rsync, ... }:
args @ {
    postgresql,
    guid, name, dbname, port, user, password,
    SCM_ISTEMP ? false,
    SCM_VERBOSE ? false,
    SCM_SANDBOX_MODE ? "imperative",
    ...
}:

rec {
    scm_type = "server";
    dependencies = [];
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    inherit postgresql guid name dbname port user password SCM_ISTEMP SCM_VERBOSE SCM_SANDBOX_MODE;
    SCM_PG = builtins.getEnv "SCM_PG";
    host = "localhost";
    basedir = "${SCM_PG}/${name}-${guid}";
    datadir = "${basedir}/data";
    pguri = "postgresql://${user}:${password}@${host}:${port}/${dbname}?application_name=scm";
    pguri_postgres = "postgresql://${user}:${password}@${host}:${port}/postgres?application_name=scm";
    pkgs =  (import ./nixpkgs.nix {
        overlays = [
            (self: super: {
                inherit postgresql;
            })
        ];
    });
    apply = { self }: scm.effect rec {
        inherit (self) postgresql dbname port user password SCM_ISTEMP SCM_VERBOSE SCM_SANDBOX_MODE;
        inherit (self) guid name scm_type transDepGuids SCM_PG host basedir datadir pguri pguri_postgres;
        SCM_EFFECT_PROG = ../py/schematic/build_server.py;
        buildInputs = [ rsync ];
        propagatedBuildInputs = [
            postgresql
            python3
        ];
    };
}
