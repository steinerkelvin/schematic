stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAVQYHDDOA2";
    name = "2021-09-13-tablefunc";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
