CREATE SEQUENCE public.country_id_seq;

CREATE TABLE public.country (
    id integer NOT NULL DEFAULT nextval('country_id_seq'),
    iso text NOT NULL,
    iso3 text NOT NULL,
    iso_numeric text NOT NULL,
    fips text,
    country text NOT NULL,
    capital text,
    area_sqkm double precision NOT NULL,
    population integer NOT NULL,
    continent text NOT NULL,
    tld text,
    currency_code text,
    currency_name text,
    phone text,
    postal_code_format text,
    postal_code_regex text,
    languages text[],
    geonameid integer NOT NULL,
    neighbours text[],
    equivalent_fips_code text,
    aliases text[] NOT NULL
);

ALTER TABLE ONLY public.country ADD CONSTRAINT country_pkey PRIMARY KEY (iso);
CREATE UNIQUE INDEX country_id_idx ON public.country USING btree (id);
CREATE UNIQUE INDEX country_country_idx ON public.country USING btree (country);
CREATE UNIQUE INDEX country_fips_idx ON public.country USING btree (fips);
CREATE UNIQUE INDEX country_geonameid_idx ON public.country USING btree (geonameid);
CREATE UNIQUE INDEX country_iso3_idx ON public.country USING btree (iso3);
CREATE UNIQUE INDEX country_iso_numeric_idx ON public.country USING btree (iso_numeric);
CREATE INDEX country_tld_idx ON public.country USING btree (tld);
CREATE INDEX country_equivalent_fips_code_idx ON public.country (equivalent_fips_code);

ALTER TABLE ONLY public.country ADD CONSTRAINT country_aliases_uniq EXCLUDE USING gist (public.map_hashtext(aliases) WITH OPERATOR(public.&&));
