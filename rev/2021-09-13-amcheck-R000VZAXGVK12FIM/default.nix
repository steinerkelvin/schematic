stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAXGVK12FIM";
    name = "2021-09-13-amcheck";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
