stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZ9XQ1A6277S";
    name = "2021-09-13-pg_trgm";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
