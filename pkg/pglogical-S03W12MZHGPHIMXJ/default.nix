stdargs @ { scm, pkgs, ... }:

scm.schema {
    # WIP: doesn't install automatically because of dependency on setting shared_preload_libraries and restarting
    # server. Depends on better configuration system that would merge shared_preload_libraries settings from multiple
    # schemas. Then, `CREATE EXTENSION pglogical;` depends on server restart which needs user consent.
    guid = "S03W12MZHGPHIMXJ";
    name = "pglogical";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2023-02-23-pglogical-R001N49PBIGY8J7A>
    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
