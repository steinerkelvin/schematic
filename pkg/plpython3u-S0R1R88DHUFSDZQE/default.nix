stdargs @ { scm, pkgs, ... }:
# TODO: build breaks, debug me
scm.schema {
    guid = "S0R1R88DHUFSDZQE";
    name = "plpython3u";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-plpython3u-R000VZDMTW1CCIL1>
    ];
}
