import calendar
import datetime

def datetime_to_unix(dt):
    return calendar.timegm(
        dt.utctimetuple() if isinstance(dt, datetime.datetime) else
        dt.timetuple() if isinstance(dt, datetime.date) else None) if dt else None

def fmt_age(timestamp):
    age = datetime.datetime.now() - datetime.datetime.fromtimestamp(timestamp)
    secs = age.total_seconds()
    if age.days:
        return '%4dd' % age.days
    if secs >= 60 * 60:
        return '%4dh' % int(secs / (60 * 60))
    if secs >= 60:
        return '%4dm' % int(secs / 60)
    return '%4ds' % int(secs)
