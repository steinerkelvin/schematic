stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S09A9O1MVOEVETSG";
    name = "uuid-ossp";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-uuid-ossp-R000VZAST87XJD9L>
    ];
}
