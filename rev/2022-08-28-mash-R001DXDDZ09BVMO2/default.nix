stdargs @ { scm, ... }:

scm.revision {
    guid = "R001DXDDZ09BVMO2";
    name = "2022-08-28-mash";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2022-06-16-mash-R001A61VRE510SAE>
    ];
}
