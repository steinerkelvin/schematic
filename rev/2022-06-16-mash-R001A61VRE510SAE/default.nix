stdargs @ { scm, ... }:

scm.revision {
    guid = "R001A61VRE510SAE";
    name = "2022-06-16-mash";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2022-05-11-mash-R0018BR5KSU0TPQO>
    ];
}
