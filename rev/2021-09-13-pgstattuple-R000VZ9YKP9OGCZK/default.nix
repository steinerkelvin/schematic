stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZ9YKP9OGCZK";
    name = "2021-09-13-pgstattuple";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
