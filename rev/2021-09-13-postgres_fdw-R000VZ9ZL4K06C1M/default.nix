stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZ9ZL4K06C1M";
    name = "2021-09-13-postgres_fdw";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
