stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0YBO76V6TFHWET9";
    name = "file_fdw";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-file_fdw-R000VZ9PEVNLEGMR>
    ];
}
