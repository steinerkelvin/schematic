stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0CBBR09GZJ962NC";
    name = "btree_gin";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-btree_gin-R000VZAZ7LIBLWYY>
    ];
}
