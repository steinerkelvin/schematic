stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "HAHOMTIBILACVICS";
    name = "pg_repack";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-03-23-pg_repack-DISKYALJIBASOCAC>
    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
