stdargs @ { scm, pkgs, stdenv, python3, ... }:
args @ { SCM_EFFECT_PROG, SCM_VERBOSE ? false, ... }:

derivation (rec {
    inherit stdenv SCM_VERBOSE;
    inherit (stdenv) system;
    inherit (pkgs) bash coreutils;
    builder = "${bash}/bin/bash";
    args = [ ./build_effect.sh ];
    PYTHONPATH = ../py;
    SCM_EFFECT_EXEC = "${python3}/bin/python3";
    SCM_EFFECT_PROG = SCM_EFFECT_PROG;
} // args)
