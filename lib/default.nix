let
    pkgs = (import ./nixpkgs.nix {});
    stdenv = pkgs.stdenv;
    python3 = (pkgs.callPackage ./python3 {});
    stdargs = { inherit scm pkgs stdenv python3; };
    scm = rec {
        effect = pkgs.callPackage ./effect.nix stdargs;
        server = pkgs.callPackage ./server.nix stdargs;
        replica = pkgs.callPackage ./replica.nix stdargs;
        remoteServer = pkgs.callPackage ./remote-server.nix stdargs;
        database = pkgs.callPackage ./database.nix stdargs;
        immutableDatabase = pkgs.callPackage ./immutable-database.nix stdargs;
        revision = pkgs.callPackage ./revision.nix stdargs;
        schema = pkgs.callPackage ./schema.nix stdargs;
        tablespace = pkgs.callPackage ./tablespace.nix stdargs;
        namespace = pkgs.callPackage ./namespace.nix stdargs;
        pghba = pkgs.callPackage ./pghba.nix stdargs;
        checkUser = pkgs.callPackage ./check-user.nix stdargs;
        callmod = module: pkgs.callPackage module stdargs;
        deps = dependencies: (map (x: (pkgs.callPackage x stdargs)) dependencies);
        getTransDepGuids = depends: (pkgs.lib.unique (builtins.concatMap (d: d.transDepGuids ++ [ d.guid ]) depends));  # guids of all transitive dependencies
        buildDeps = server: self:
            with rec {
                # don't use revisions if we're in "declarative" mode
                depends = builtins.filter (x: server.SCM_SANDBOX_MODE != "declarative" || x.scm_type != "revision") self.depends;
                depInputs = (map (x: x.apply { self = x; inherit server; }) depends);
                buildInputs = (if self.preBuildInputs == null then [] else (self.preBuildInputs { pkgs = server.pkgs; }));
            };
            [ (server.apply { self = server; }) ] ++ buildInputs ++ depInputs;
    };
in scm
