stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0JHVJ9MRB9J2IW1";
    name = "pg_visibility";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-pg_visibility-R000VZAUGAFFHW8J>
    ];
}
