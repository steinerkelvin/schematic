stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000GHY1QOY4QC9J";
    name = "2020-11-16-smallint_bit";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
