stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S06Y4F541JSADNHH";
    name = "hashtypes-aleksable";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-10-11-hashtypes-aleksable-R000XEZTAE04JU0E>
    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
