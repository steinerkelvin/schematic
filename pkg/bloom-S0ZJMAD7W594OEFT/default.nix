stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0ZJMAD7W594OEFT";
    name = "bloom";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-bloom-R000VZAUTJ7H3E22>
    ];
}
