stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VBAGY4HINRSK";
    name = "2021-08-31-hash-key-funcs";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-07-23-hash-key-funcs-R000TAU6RMKK4LC5>
    ];
}
