stdargs @ { scm, ... }:

scm.pghba {
    guid = "C12CC9KJLBVT3TPM";
    name = "localhost-pghba";
    comment = "Trust localhost connections.";
    rules = [
        {
            type = "local";
            dbname = "all";
            user = "all";
            addr = "";
            auth = "trust";
            opts = "";
            comment = "unix domain sockets";
        }
        {
            type = "host";
            dbname = "all";
            user = "all";
            addr = "127.0.0.0/8";
            auth = "trust";
            opts = "";
            comment = "ipv4 loopback addresses";
        }
        {
            type = "host";
            dbname = "all";
            user = "all";
            addr = "::1/128";
            auth = "trust";
            opts = "";
            comment = "ipv6 loopback address";
        }
    ];
}
