stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAVUD3VVBPL";
    name = "2021-09-13-jsonb_plperl";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-plperl-R000VZATJTXXQGMN>
    ];
}
