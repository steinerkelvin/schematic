stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0WJ2U29DSOLXF3J";
    name = "refint";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-refint-R000VZAXXLV2LU4T>
    ];
}
