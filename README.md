# Overview

Schematic is a version control system and package manager for PostgreSQL. Like git, schematic introduces a branching distributed development model, hash integrity checks, and is structured as a directed acyclic graph (DAG) of revisions.

Features:
* Schema migrations.
* Auto-generate migrations by diffing declarative SQL.
* Data migrations. Small data in text files or big data by URL reference.
* Extensions support (including native libraries).
* Configuration management.
* PostgreSQL 12, 13, 14 (including any point release or custom build by specifying a git commit).
* A communal library of packages (extensions, schema, data, code).
* Distributed workflow. Works well with branches.
* Easily create sandbox databases for experimentation, populated with schema and data.
* Idempotent and reproducibile wherever possible.
* Programming language agnostic. No assumption is made of what your primary language is.
* Linux and MacOS support. Works in Windows via WSL.

Packages and revisions are defined in the [nix programming language](https://nixos.org/), and can depend on any of the 60,000+ nix software packages.

# Installation

Linux and MacOS are supported natively. Windows via WSL.

There's only one dependency that needs to be installed on your system: `nix`. Install it by running the shell script `./nix-install.sh` or [following these instructions](https://nixos.org/download.html).

Packages will require other dependencies, but they will only be installed in an isolated directory and will not interfere with any existing software on your system— it's safe to install on a development machine or server without risk of overwriting or changing anything.

Installation inside of containers does not work because the builds cannot run as root, they require an unprivileged user. Containers are also not advised because IO virtualization breaks ACID (Atomicity, Consistency, Isolation, Durability) guarantees. Virtual machines are fine.


# Walkthrough

Define a new database:
```
scm database hello-world
```

This creates a file that defines a new database.

To build a sandbox/test copy of this database:
```
scm sandbox srv/hello-world*
```

This will build the new database, including PostgreSQL itself and any dependent packages, and drop you into a `psql` shell. You'll see there's no schema in there. Exit `psql` so we can define a new schema object:

```
scm schema country_info
```

Open the SQL file it generated and enter this:
```
CREATE TABLE country_info (
    id text NOT NULL,
    name text NOT NULL,
    continent text NOT NULL,
    currency_code text,
    currency_name text,
    tld text,
    phone text,
    aliases text[] NOT NULL
);
ALTER TABLE country_info ADD CONSTRAINT country_pkey PRIMARY KEY (id);
CREATE UNIQUE INDEX country_info_name_ix ON country_info USING btree (name);
ALTER TABLE country_info ADD CONSTRAINT country_info_id_upper CHECK (id = upper(id));
ALTER TABLE country_info ADD CONSTRAINT country_info_continent_upper CHECK (continent = upper(continent));

INSERT INTO country_info (id, name, continent, currency_code, currency_name, tld, phone, aliases)
VALUES ('US', 'United States', 'NA', 'USD', 'Dollar', '.us', '1', array['United States of America', 'United States', 'USA', 'US']);
```

We can create a sandbox database with this schema with the command:

```
scm sandbox pkg/country_info-*
```

This will build a database, run the SQL, and open a `psql` shell for us to experiment with.


Let's make the database depend on this schema. Open `srv/hello-world-*/default.nix`. Add the line `<country_info-GUID>` to the `dependencies` list (replacing `GUID` with the id you see when you `ls pkg`, leave out the trailing slash). List items are separated by whitespace (no commas), in this case let's use newlines for readability. The angle brackets are a special notation for a file reference that will look for the file by that name by searching the `NIX_PATH` environmental variable, similar to how your shell finds a program to run by searching for its name in the `PATH` environmental variable. Instead, you could also use a relative or absolute file reference.

Now you can run the sandbox command on the datatabase and get this schema populated:

```
scm sandbox srv/hello-world-*
```

That built a new database and installed the schema it depends on.

There's two ways to define schema: declarative and imperative. The approach we did above was declarative. This is useful for building sandbox databases from scratch. To build a database that will evolve over time, we need to use revisions. Let's create a revision to add the schema we defined above:

```
scm revision --auto pkg/country_info-*
```
(The auto-generation of revisions with `--auto` is alpha. It works for simple stuff, but you'll need to write your own migrations for more complex stuff. You should always review the output.)

This did a handful of things:
* It created a sandbox database using the declarative SQL defitions.
* It created a sandbox database using the imperative revisions (so far, none).
* It diffed the two to find what's in the declarative database that's missing in the imperative database.
* It created a new revision in the `rev/` directory.
* It populated `rev/.../upgrade.sql` with the difference from the two sandbox databases.
* It updated `pkg/country_info-.../default.nix` to depend on this new revision.

Check out the `upgrade.sql` file to see that it generated the right SQL. It didn't generate the `INSERT` statement (a feature to diff data automatically will be added in the future), so go ahead and add it manually.

Now, let's build a database that will use the imperative revision instead of the declarative SQL:
```
scm upgrade srv/hello-world-*
```

Use `psql` to check that everything is in order. If you run the command again, nothing will happen this time.

To see the database we created:
```
scm status
```

There's also a line of output for each of the sandbox databases we created. We can remove them by first stopping them:
```
scm stop ~/var/pg/*
```

Run `scm status` again to see that they are stopped. Now let's garbage collect them:
```
scm gc
```

Run `scm status` again to see that they are removed, except for the database we created with `scm upgrade` (the `scm gc` command will only remove temporary sandbox databases). We can turn that one back on:
```
scm start ~/var/pg/*
```

We can open a new `psql` shell to it:
```
scm psql ~/var/pg/*hello-world*
```

# Directories

There's three content directories: `srv`, `pkg`, `rev`.

Each of these will contain subdirectories for objects of the given type, and those will contain a `default.nix` file that defines the object. The `default.nix` is implied in filesystem paths, you can reference just the parent directory to be more succinct (eg `pkg/country-S0Y2F1PPW4X10VTW/` instead of `pkg/country-S0Y2F1PPW4X10VTW/default.nix`).

To be even more succint, you can use a nix notation to reference the object without the relative path by wrapping the object name in angle brackets, eg `<pg_repack-HAHOMTIBILACVICS>` instead of `pkg/pg_repack-HAHOMTIBILACVICS`. These references work like references to programs in your `PATH` environmental variable. Instead of searching `PATH`, they search `NIX_PATH` for the matching object. The unique ID in the names of objects ensures the right object is being matched without risk of ambiguity/conflict. Referencing objects this way is recommended instead of relative or absolute filesystem paths because 1) it makes it easy to move and refactor code, 2) it enables references to third party packages that might be stored in different places relative to your packages, and 3) it enables overriding third party content by using the same object name in your local packages.

## `./srv/`

Short for "server", or "service" if you prefer to think of a server as an operating system, containing definitions for database servers.

Includes definitions for postgresql databases (ie a database "cluster" in postgresql terminology, a single server instance).

## `./pkg/`

Short for "package", logical components that are composed into databases.

Includes schema definitions. Most often each table and its immediate dependents like indexes and constraints will be bundled into a schema, but a schema can bundle extensions, configuration, utility functions, etc.

## `./rev/`

Short for "revision", stateful/imperative changes made to packages/schemas/databases.

Used for schema migrations, data migrations, configuration changes, etc.

# Create a database

1. Use `scm database` to create a database (recommended: lowercase ascii without symbols). See `scm database --help`. Suppose it's called `bar-D0Q9HU8QL40ZSWFM`.

2. Review `srv/bar-D0Q9HU8QL40ZSWFM/default.nix`. Consider editing `name`, `dbname`, `port`, `user`, `password`. You'll add schema packages to the `dependencies` list to install a package into this database.

# Create a schema

1. Use `scm schema` command to create a new schema. See `scm schema --help`. Suppose it's called `foo-S0DDCOO472RBML4B` ("foo" is the name, and the second part is a generated unique ID).

2. Update `pkg/foo-S0DDCOO472RBML4B/upgrade.sql` to reflect the desired/declarative state.

3. Update `pkg/foo-S0DDCOO472RBML4B/default.nix` to add dependencies on other schemas. Dependencies are required for references to other objects via foreign keys, function calls, etc. If it's unclear whether something should be a dependency or not, assume not, and add it later to fix a build error.

4. Run `scm sandbox -v pkg/foo-S0DDCOO472RBML4B` to test building the schema. If it builds and looks good from inspecting it in the resulting psql shell, move on to next step, otherwise go back to step 2.

5. Create a revision for the schema. See instructions for creating a revision.

# Create a revision

1. Identify the schema you want to revise in the `pkg/` directory. Use `ls` or `grep`. Suppose it's called `foo-S0DDCOO472RBML4B`.

2. Open `pkg/foo-S0DDCOO472RBML4B/upgrade.sql`. This defines the target state of the schema (as opposed to a migration to change it from one state to another). Edit it to the state you desire.

3. Test the changes you made by running `scm sandbox -v pkg/foo-S0DDCOO472RBML4B`. If it builds and looks good from inspecting it in the resulting psql shell, move on to next step, otherwise go back to step 2.

4. Create a revision. This is what will run against existing databases, including production. Run `scm revision schema/.../`.

5. Open the generated revision's `upgrade.sql`. Write out the changes required to upgrade an existing database to the desired state.

6. Test the revision you wrote by running `scm sandbox -v pkg/foo-S0DDCOO472RBML4B -m imperative`. The `-m imperative` will generate a sandbox database by using the revisions instead of the declarative target state. If it builds and looks good from inspecting it in the resulting psql shell, you're done. Otherwise go back to step 5.


# Development

Clone this repo and enter the development environment with `nix-shell dev.nix`.
