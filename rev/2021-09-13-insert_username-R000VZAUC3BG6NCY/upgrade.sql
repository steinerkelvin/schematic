CREATE EXTENSION IF NOT EXISTS insert_username WITH SCHEMA public;
COMMENT ON EXTENSION insert_username IS 'functions for tracking who changed a table';

