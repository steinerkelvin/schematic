stdargs @ { scm, pkgs, ... }:

scm.schema {
    # WIP: doesn't install automatically because of dependency on setting shared_preload_libraries and restarting
    # server. Depends on better configuration system that would merge shared_preload_libraries settings from multiple
    # schemas. Then, `CREATE EXTENSION pg_squeeze;` depends on server restart which needs user consent.
    guid = "S03UZHGLY2U7Y8MV";
    name = "pg_squeeze";
    upgrade_sql = ./upgrade.sql;
    dependencies = [

    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
