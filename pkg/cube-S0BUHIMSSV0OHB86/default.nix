stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0BUHIMSSV0OHB86";
    name = "cube";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-cube-R000VZAVI37GCQPX>
    ];
}
