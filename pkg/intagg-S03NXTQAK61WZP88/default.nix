stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S03NXTQAK61WZP88";
    name = "intagg";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-intagg-R000VZATV8C6DE82>
    ];
}
