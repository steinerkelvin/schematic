stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.schema {
    guid = "VATKAFMOMBEE";
    name = "sandbox-schema";
    upgrade_sql = (/. + (builtins.getEnv "SCM_SANDBOX_SQL"));
    dependencies = [

    ];
}
