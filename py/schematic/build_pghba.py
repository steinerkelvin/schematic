'''
Translate host-based authentication rules into a section of the pg_hba.conf file.

See: https://www.postgresql.org/docs/current/auth-pg-hba-conf.html
'''
import itertools
import json
import os
import re
import sys
from collections import OrderedDict

from schematic import env, pgcn, seqs

type_enum = ('host', 'hostgssenc', 'hostnogssenc', 'hostnossl', 'hostssl', 'local')
auth_enum = (
    'bsd', 'cert', 'gss', 'ident', 'ldap', 'md5', 'pam', 'password', 'peer', 'radius', 'reject', 'scram-sha-256',
    'sspi', 'trust')

def fmt_line(parts, colsizes=(13, 20, 20, 25, 14, 1)):
    out = ''
    for idx, part in enumerate(parts):
        out += ((part or '') + ' ').ljust(colsizes[idx] if len(colsizes) > idx else 30)
    return out.rstrip(' ')

def validate_reqd(rule, field, not_=False):
    if bool(rule.get(field)) is not_:
        print('"%s" field is %srequired for rule %r' % (field, 'not ' if not_ else '', rule), file=sys.stderr)
        sys.exit(10)

def validate_quotes(rule, field):
    val = rule.get(field)
    if val and re.search(r'[\s,]+', val) and not re.match(r'".+"', val):
        print('"%s" field has value %r that must use double quotes' % (field, val), file=sys.stderr)
        sys.exit(10)

def validate_enum(rule, field, options):
    val = rule.get(field)
    if val is not None and val not in options:
        print('"%s" field expected one of %r, got %r' % (field, options, val), file=sys.stderr)
        sys.exit(10)

def validate_notrust_remote(rule):
    ''' Validate that we're only trusting (password-less) local connections. '''
    insecure = rule.get('insecure')
    if rule.get('auth') == 'trust' and rule.get('type') in ('host', 'hostnossl'):
        addr = rule.get('addr').strip()
        if addr:
            if addr.startswith('127.') or addr == '::1/128' or addr == 'localhost':
                return
            msg = '"trust" auth method for non-local client "%s" is insecure' % addr
            if insecure:
                return print('warn: %s' % msg, file=sys.stderr)
            print('error: %s' % msg, file=sys.stderr)
            print('hint: use auth = "md5" instead for password authentication', file=sys.stderr)
            print('hint: use insecure = true to override this error', file=sys.stderr)
            sys.exit(10)

def validate_unencrypted_password(rule):
    ''' Validate that we aren't allowing plaintext passwords over the network. '''
    insecure = rule.get('insecure')
    if rule.get('auth') == 'password':
        addr = rule.get('addr').strip()
        if addr:
            msg = '"password" auth method sends plain-text passwords which can be intercepted'
            if insecure:
                return print('warn: %s' % msg, file=sys.stderr)
            print('error: %s' % msg, file=sys.stderr)
            print('hint: use auth = "md5" instead send hashed passwords', file=sys.stderr)
            print('hint: use insecure = true to override this error', file=sys.stderr)
            sys.exit(10)

def validate_rule(rule):
    for key in rule.keys() - {'type', 'dbname', 'user', 'addr', 'auth', 'opts', 'comment', 'insecure'}:
        print('warning: unrecognized field %r in pghba rule %s' % (key, rule), file=sys.stderr)
    validate_reqd(rule, 'type')
    validate_enum(rule, 'type', type_enum)
    validate_reqd(rule, 'dbname')
    validate_quotes(rule, 'dbname')
    validate_reqd(rule, 'user')
    validate_quotes(rule, 'user')
    validate_reqd(rule, 'addr', not_=(rule['type'] == 'local'))
    validate_quotes(rule, 'addr')
    validate_reqd(rule, 'auth')
    validate_enum(rule, 'auth', auth_enum)
    validate_enum(rule, 'insecure', (None, True, False))
    validate_notrust_remote(rule)
    validate_unencrypted_password(rule)

def fmt_rule(rule):
    ''' Translate a rule object into a string line.
    >>> print(fmt_rule({'auth': 'trust', 'comment': 'test', 'dbname': 'testdb', 'type': 'local', 'user': 'root'}))
    local        testdb              root                                         trust          # test
    >>> print(fmt_rule({'addr': '0.0.0.0/0', 'auth': 'trust', 'opts': '', 'comment': 'test', 'dbname': 'testdb', 'type': 'host', 'user': 'root'}))
    host         testdb              root                0.0.0.0/0                trust          # test
    >>> print(fmt_rule({'addr': '0.0.0.0/0', 'auth': 'trust', 'opts': '', 'comment': 'test1\\ntest2', 'dbname': 'testdb', 'type': 'host', 'user': 'root'}))
    host         testdb              root                0.0.0.0/0                trust          # test1_test2
    '''
    return fmt_line((
        rule.get('type'), rule.get('dbname'), rule.get('user'), rule.get('addr'), rule.get('auth'), rule.get('opts'),
        '# %s' % re.sub(r'\n', '_', rule['comment']) if rule.get('comment') else None))

def fmt_header(guid, name, comment, depends):
    ''' Format a section header.
    >>> print(fmt_header('C1MKM4UFIN2B4S3X', 'pghba01', 'hello', ['C103QA2MJ7CI7BOK']))
    # <pghba01-C1MKM4UFIN2B4S3X> depends (C103QA2MJ7CI7BOK)
    # hello
    >>> print(fmt_header('C1MKM4UFIN2B4S3X', 'pghba01', 'world', []))
    # <pghba01-C1MKM4UFIN2B4S3X>
    # world
    >>> print(fmt_header('C1MKM4UFIN2B4S3X', 'pghba01', None, ['C103QA2MJ7CI7BOK']))
    # <pghba01-C1MKM4UFIN2B4S3X> depends (C103QA2MJ7CI7BOK)
    >>> print(fmt_header('C1MKM4UFIN2B4S3X', 'pghba01', '', []))
    # <pghba01-C1MKM4UFIN2B4S3X>
    >>> print(fmt_header('C1MKM4UFIN2B4S3X', 'pghba01', 'foo \\n bar', []))
    # <pghba01-C1MKM4UFIN2B4S3X>
    # foo _ bar
    '''
    ret = '# <%s-%s>' % (name, guid)
    if depends:
        ret += ' depends (%s)' % ' '.join(sorted(depends))
    if comment:
        ret += '\n# %s' % re.sub(r'\n', '_', comment)
    return ret

def fmt_footer(guid, name):
    ''' Format a section footer.
    >>> print(fmt_footer('C1MKM4UFIN2B4S3X', 'pghba01'))
    # </pghba01-C1MKM4UFIN2B4S3X>
    '''
    return '# </%s-%s>' % (name, guid)

def append_newline(text):
    return '%s\n' % text if not text.endswith('\n') else text

def parse_header(line):
    ''' Parse a section header into its guid and its depencencies' guids.
    >>> parse_header('# <pghba02-C1SMJSALSU559R9H>')
    ('C1SMJSALSU559R9H', ())
    >>> parse_header('# <pghba02-C1SMJSALSU559R9H> depends (C1MKM4UFIN2B4S3X C103QA2MJ7CI7BOK)')
    ('C1SMJSALSU559R9H', ('C1MKM4UFIN2B4S3X', 'C103QA2MJ7CI7BOK'))
    >>> parse_header('# <pghba00-C103QA2MJ7CI7BOK> depends (C103QA2MJ7CI7BOK)')
    ('C103QA2MJ7CI7BOK', ('C103QA2MJ7CI7BOK',))
    >>> parse_header('# <lan-pghba-C1ILCOBY3I22I7D6>')
    ('C1ILCOBY3I22I7D6', ())
    >>> parse_header('# <lan_pghba-C1ILCOBY3I22I7D6>')
    ('C1ILCOBY3I22I7D6', ())
    '''
    reg = re.compile(r'^# <[\w_-]+\-(?P<guid>C1[A-Z0-9]+)>( depends \((?P<deps>[A-Z0-9 ]+)\))?$')
    if match := reg.match(line.strip()):
        deps = match.group('deps')
        return (match.group('guid'), tuple(deps.split(' ')) if deps else ())

def parse_footer(line):
    ''' Parse a section footer into its guid.
    >>> parse_footer('# </pghba02-C1SMJSALSU559R9H>')
    'C1SMJSALSU559R9H'
    >>> parse_footer('# </lan-pghba_2-C1ILCOBY3I22I7D6>')
    'C1ILCOBY3I22I7D6'
    '''
    if match := re.match(r'^# </[\w_-]+\-(?P<guid>C1[A-Z0-9]+)>$', line.strip()):
        return match.group('guid')

def parse_pghbaconf(content):
    ''' Parse a pg_hba.conf file into sections (one section per pghba package, plus a section for default content). '''
    sections = OrderedDict()
    buf = []
    header = None
    for line in content.split('\n'):
        if footer := parse_footer(line):
            if not header:
                raise ValueError('invalid file, got footer with no matching header on line %r' % line)
            guid, depends = header  # pylint: disable=unpacking-non-sequence
            if guid != footer:
                raise ValueError('invalid file, expected footer to have guid %s on line %r' % (guid, line))
            sections[guid] = {'lines': buf + [line], 'depends': depends}
            buf = []
            header = None
            continue
        if newheader := parse_header(line):
            if header:
                raise ValueError('invalid file, got header inside another header header on line %r' % line)
            header = newheader
            buf.append(line)
            continue
        if header:
            buf.append(line)
            continue
        if None not in sections:
            sections[None] = {'lines': [], 'depends': []}
        sections[None]['lines'].append(line)
    if header:
        raise ValueError('header had no footer: %r' % header)
    return sections

def transitive_deps(edges):
    ''' Compute the transitive closure of edge-wise dependencies.
        :edges: a list of tuples (dependee, dependency)
    >>> sorted(list(transitive_deps([(1, 2), (2, 3)])))
    [(1, 2), (1, 3), (2, 3)]
    >>> sorted(list(transitive_deps([(1, 2), (2, 3), (3, 4)])))
    [(1, 2), (1, 3), (1, 4), (2, 3), (2, 4), (3, 4)]
    '''
    dependees = {x: [y for (_, y) in ys] for (x, ys) in seqs.groups(edges, key=lambda x: x[0])}
    edges = set(edges)
    while True:
        initlen = len(edges)
        for x, y in list(edges):
            if y in dependees:
                edges.update([(x, z) for z in dependees[y]])
        if len(edges) == initlen:
            break
    return edges

def topographic_sort_sections(sections):
    ''' Topographic sort sections so that dependees come before dependencies. This allows the user to control the order
        of sections by creating a dependency on another pghba package in order to force a given package to come first
        (hence override the rules in another package). In the absense of a dependency relation between two sections,
        order by guid for deterministic output.
        Returns a list of guids.
    '''
    deps = [(depender, dependee) for dependee, section in sections.items() for depender in section['depends']]
    trans_deps = transitive_deps(deps)
    def dep_cmp(x0, x1):
        if (x0, x1) in trans_deps:
            return 1
        if (x1, x0) in trans_deps:
            return -1
        return 0
    guids = list(sorted(filter(None, sections.keys())))  # initially sorted on guid
    for x, y in itertools.product(list(guids), list(guids)):
        x_idx = guids.index(x)
        y_idx = guids.index(y)
        if dep_cmp(x, y) and (seqs.cmp(x_idx, y_idx) != dep_cmp(x, y)):
            if x_idx < y_idx:
                guids.remove(y)
                guids.insert(x_idx, y)
            else:
                guids.insert(x_idx, y)
                guids.remove(y)
    return guids

def serialize_pghbaconf(sections):
    ''' Serialize the sections into a string. '''
    buf = []
    for guid in topographic_sort_sections(sections) + [None]:
        buf.extend(sections[guid]['lines'])
    return append_newline(''.join(append_newline(line) for line in buf).rstrip())

def update_config(filepath, guid, name, comment, rules):
    ''' Update config file, replacing content between start_flag and end_flag (if they exist) and returning boolean
        indicating whether any change was made.
    '''
    old_content = open(filepath).read() if os.path.exists(filepath) else ''
    sections = parse_pghbaconf(old_content)
    depends = [x for x in env.get_array('transDepGuids') or [] if x.startswith('C1')]
    lines = [fmt_header(guid, name, comment, depends)] + [fmt_rule(rule) for rule in rules] + [fmt_footer(guid, name)]
    sections[guid] = {'lines': lines, 'depends': depends, 'comment': comment}
    new_content = serialize_pghbaconf(sections)
    if new_content == old_content:
        return False
    with open(filepath, 'w') as fp:
        fp.write(new_content)
    return True

def main():
    name = env.get_str('name')
    guid = env.get_str('guid')
    comment = env.get_str('comment')
    rules = env.get_str('rules')
    rules = json.loads(rules)
    assert isinstance(rules, list), 'expected list of rules, got %r' % rules
    [validate_rule(rule) for rule in rules]
    with pgcn.connect(env.get_str('pguri'), autocommit=True) as pg:
        # obtain lock to avoid consistency issues with concurrent edits
        pg.execute('SELECT pg_advisory_lock(8980025710542546871);').first()
        pghba_path = (
            pg.execute('''SELECT setting FROM pg_settings WHERE name = 'hba_file';''').scalar() or
            os.path.join(env.get_str('basedir'), 'data/pg_hba.conf'))
        if update_config(pghba_path, guid, name, comment, rules):
            print('<%s-%s> -> %s' % (name, guid, pghba_path), file=sys.stderr)
        if errors := pg.execute('SELECT * FROM pg_hba_file_rules WHERE error IS NOT NULL;').all():
            print('ERROR: invalid pg_hba.conf configuration in %s' % pghba_path, file=sys.stderr)
            for line in errors:
                print('    line %d: %s' % (line.line_number, line.error), file=sys.stderr)
            sys.exit(11)
        pg.execute('SELECT pg_reload_conf();')

if __name__ == '__main__':
    main()
