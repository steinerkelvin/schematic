stdargs @ { scm, pkgs, rsync, ... }:
args @ {
    guid, name, upgrade_sql, basefiles ? null, add_meta_revision ? true, autocommit ? false,
    dependencies ? [], preBuildInputs ? null,
    ...
}:

rec {
    scm_type = "revision";
    inherit guid name dependencies upgrade_sql preBuildInputs basefiles add_meta_revision autocommit;
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    apply = { self, server }: scm.effect rec {
        inherit (server) postgresql pguri basedir SCM_SANDBOX_MODE;
        inherit (self) guid transDepGuids upgrade_sql name scm_type add_meta_revision basefiles autocommit;
        SCM_FAST_FORWARD = builtins.getEnv "SCM_FAST_FORWARD";
        SCM_EFFECT_PROG = ../py/schematic/build_revision.py;
        dependencies = map (a: a.guid) (builtins.filter (a: a ? guid) self.depends);
        # revisions have an implied dependency on revision to log that they've been applied
        buildInputs = [ rsync ] ++ (scm.buildDeps server (self // { depends = self.depends ++ (if add_meta_revision then scm.deps [ <meta.revision-ADTUKDUCMEEMEGPI> ] else []); }));
    };
}
