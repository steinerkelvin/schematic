stdargs @ { scm, ... }:

scm.revision {
    guid = "R001N49PBIGY8J7A";
    name = "2023-02-23-pglogical";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
