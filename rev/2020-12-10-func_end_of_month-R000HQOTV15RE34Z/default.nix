stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000HQOTV15RE34Z";
    name = "2020-12-10-func_end_of_month";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
