import os
import sys
from contextlib import contextmanager

import psycopg2
import psycopg2.extras
from gevent.socket import wait_read, wait_write
from schematic import urls


class Cursor(psycopg2.extras.NamedTupleCursor, psycopg2.extras.ReplicationCursor):

    def scalar(self):
        if self.rowcount == 0:
            return
        assert self.rowcount <= 1, 'scalar() expects a single row result'
        assert len(self.description) == 1, 'scalar() expects a single column result'
        return self.fetchone()[0]

    def first(self):
        return self.fetchone()

    def all(self):
        return self.fetchall()

class PGConn:

    def __init__(self, conn, pguri):
        self.conn = conn
        self.pguri = pguri
        self.cur = self.conn.cursor()
        self.outbox = []

    def execute(self, query, vars=None):
        self.flush()
        cur = self.conn.cursor()
        cur.execute(query, vars)
        return cur

    def flush(self):
        ''' Flush all enqueued queries in a single statement. '''
        if not self.outbox:
            return
        cur = self.conn.cursor()
        statement = b';\n'.join([cur.mogrify(q, v).rstrip().rstrip(b';') for (q, v) in self.outbox])
        self.outbox[:] = []
        cur.execute(statement)
        return cur

    def enqueue(self, query, vars=None):
        ''' Enqueue a query to be run in a batch. Flush the queue with flush(), execute(query, vars), or commit(). '''
        self.outbox.append((query, vars))

    def close(self):
        self.cur.close() if self.cur else None
        self.conn.close()

    def commit(self):
        self.flush()
        self.conn.commit()

    def rollback(self):
        self.outbox[:] = []
        self.conn.rollback()

    @property
    def is_open(self):
        return not self.conn.closed

class Unset:
    pass

def default_application_name(argv=None):
    '''
    >>> default_application_name(argv=[])
    'scm'
    >>> default_application_name(argv=['/nix/store/bm7vqc7a5adm1vmia7frz77lb4yqndlk-8i6av1pqx43b03by71y9k4rq90003alf-source/shell/scm', 'sync'])
    'scm-sync'
    >>> default_application_name(argv=['scm', 'sandbox', 'logic.if-S02OJ7RPEKLQ97I2/'])
    'scm-sandbox'
    '''
    argv = argv if argv is not None else sys.argv
    hitprog = False
    for arg in argv:
        if os.path.basename(arg) == 'scm':
            hitprog = True
        elif hitprog:
            return f'scm-{arg}'
    return 'scm'

@contextmanager
def connect(pguri, readonly=Unset, autocommit=Unset, connection_factory=None, cursor_factory=Cursor,
            isolation_level=None, application_name=None):
    '''Open a psycopg2 postgresql connection with a context manager.'''
    pg = None
    if application_name or 'application_name' not in urls.getquery(pguri):
        pguri = urls.modquery(pguri, {'application_name': application_name or default_application_name()})
    conn = psycopg2.connect(pguri, cursor_factory=cursor_factory, connection_factory=connection_factory)
    if readonly is not Unset:
        conn.set_session(readonly=readonly)
    if autocommit is not Unset:
        conn.set_session(autocommit=autocommit)
    if isolation_level:
        conn.set_session(isolation_level=isolation_level)
    pg = PGConn(conn, pguri)
    yield pg
    pg.close() if pg else None

def gevent_wait_callback(conn, timeout=None):
    ''' A wait callback for psycopg2 to poll a connection using gevent. '''
    while True:
        try:
            state = conn.poll()
            if state == psycopg2.extensions.POLL_OK:
                break
            elif state == psycopg2.extensions.POLL_READ:
                wait_read(conn.fileno(), timeout=timeout)
            elif state == psycopg2.extensions.POLL_WRITE:
                wait_write(conn.fileno(), timeout=timeout)
            else:
                raise psycopg2.OperationalError('Bad result from poll: %r' % state)
        except KeyboardInterrupt:
            conn.cancel()

def async_patch_psycopg2(cb=gevent_wait_callback):
    ''' Make psycopg2 connections in this process asynchronous/non-blocking in gevent greenlets. '''
    psycopg2.extensions.set_wait_callback(cb)
