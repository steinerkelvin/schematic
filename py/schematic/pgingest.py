'''
Utility functions for ingesting data into a postgresql database.

It offers functions for update, insert, upsert (update + insert together), delete, and truncate.

It's recommended to use upsert instead of insert because it supports merging when there's an existing record.

It's recommended to use upsert instead of update because it works when there's a missing record.

The PREPARE statement is used as an optimization that saves repetitive query parsing and planning, as well as
reducing query size over the wire. https://www.postgresql.org/docs/current/sql-prepare.html
'''

import base64
import hashlib
import math
import os
import re
from collections import OrderedDict, namedtuple

import psycopg2
from psycopg2.extras import execute_values
from psycopg2.sql import SQL, Identifier
from schematic import env, pglsn, slog

scm_var_path = env.get_str('SCM_VAR', os.path.expanduser('~/var'))
unique_error_flag_path = os.path.join(scm_var_path, 'schematic/.pgingest-conflicting-key')
unique_error_flag_path_exists = os.path.exists(unique_error_flag_path)

def try_unlink(filename):
    try:
        os.unlink(filename) if filename else None
    except (OSError, IOError):
        pass

def commit_tsn(pg, tsn, tts):
    ''' Update replication origin up to :tsn: and commit. '''
    pg.enqueue('SELECT pg_replication_origin_xact_setup(%s, %s);', (pglsn.tsn_to_lsn(tsn), tts))
    try:
        pg.commit()
        try_unlink(unique_error_flag_path) if unique_error_flag_path_exists else 0
    except psycopg2.errors.UniqueViolation as exc:  # pylint: disable=no-member
        # in order to unblock the subscription, a conflicting record needs to be removed
        if exc and exc.pgcode == '23505':  # unique_violation
            with open(unique_error_flag_path, 'w', encoding='utf8') as fp:
                fp.write(exc.pgerror)
            slog.warn2('conflicting row: %s    hint: on next run, the conflicting row will be removed', exc.pgerror)
        raise

def init_replication_settings(pg, replication_role, subid=None):
    ''' Configure a pg session for receiving a replication stream.
        :subid: when provided, setup replication origin and return the position
    '''
    # tables with self-referential foreign keys may cause constraint violation if rows are out of dependency order
    # this defers constraint validation until the end of the transaction
    # https://www.postgresql.org/docs/current/sql-set-constraints.html
    pg.execute('SET CONSTRAINTS ALL DEFERRED;')
    assert replication_role in ('replica', 'origin', 'local'), f'invalid replication_role {replication_role!r}'
    if replication_role != 'origin':
        pg.execute('SET session_replication_role = %s;', (replication_role,))
    # disabling synchronous_commit improves performance and is safe in this context because upon a postgresql crash
    # recent writes can be lost, but we'll just replay them anyway on the next run
    pg.execute('SET SESSION synchronous_commit TO OFF;')
    if subid:
        pg.execute('SELECT coalesce(pg_replication_origin_oid(%s), pg_replication_origin_create(%s));', (subid, subid))
        pg.execute('SELECT pg_replication_origin_session_setup(%s);', (subid,))
        return pglsn.lsn_to_tsn(pg.execute('SELECT pg_replication_origin_session_progress(false);').scalar())

def get_column_defs(pg):
    allcols = pg.execute('''
        SELECT
            n.nspname AS namespace,
            t.relname AS tablename,
            c.attname AS colname,
            c.attnotnull AS notnull,
            c.attgenerated AS generated,
            c.attstorage IN ('e', 'x') AS toastable
        FROM pg_class t
        LEFT JOIN pg_namespace n ON n.oid = t.relnamespace
        LEFT JOIN pg_attribute c ON c.attrelid = t.oid
        WHERE c.attnum > 0
        ORDER BY 1, 2, c.attnum;
    ''').all()
    ret = OrderedDict()
    for col in allcols:
        key = (col.namespace, col.tablename)
        ret[key] = ret.get(key, OrderedDict())
        ret[key][col.colname] = col
    return ret

def get_table_sym(table):
    ''' Get a psycopg2 Identifier symbol given a table dict. '''
    return Identifier(table['namespace'], table['tablename'])

PreparedStatementKey = namedtuple('PreparedStatementKey', ('opname', 'namespace', 'tablename', 'cols'))

def snip_middle(text, limit, mark='...'):
    ''' Limit text to max length by inserting a separator string in middle.
    >>> snip_middle(None, 1)
    >>> snip_middle('12345678', 5)
    '1...8'
    >>> snip_middle('12345678', 5, 'X')
    '12X78'
    >>> snip_middle('abcdefghijklmnopqrstuvwxyz', 12)
    'abcd...vwxyz'
    >>> snip_middle('abcdefg', 4)  # not enough room for mark
    'abcd'
    '''
    if not text or len(text) <= limit:
        return text
    if limit < len(mark) + 2:
        return text[:limit]
    limit -= len(mark)
    return f'{text[:math.floor(limit/2)]}{mark}{text[-math.ceil(limit/2):]}'

def gen_prep_name(prep_key):
    ''' Generate a name to use for a prepared statement.
    >>> gen_prep_name(PreparedStatementKey('insert', 'foo', 'bar', ('id', 'name', 'created_at')))
    'insert_bar_BELRJATTJZHR5DYJGVKUIUF4AU'
    >>> gen_prep_name(PreparedStatementKey('insert', 'foo', 'bar' * 50, ('id', 'name', 'created_at')))
    'insert_barbarbarbarbarbarbarbaXXXbar_SRIDN2AWHRI4YT5JCJ7KELCV7M'
    '''
    guid = re.sub(r'[^a-zA-Z0-9]', '', base64.b32encode(hashlib.md5(repr(prep_key).encode('utf8')).digest()).decode())
    # 63 is max length in postgresql for a prepared statement name
    return snip_middle(f'{prep_key.opname}_{prep_key.tablename}_{guid}', 63, 'XXX')

class MissingTable(Exception):
    pass

def is_generated_col(tbl_cols, colname):
    coldef = tbl_cols.get(colname)
    return coldef.generated == 's' if coldef else None

def get_cols(table, column_defs, rec):
    ''' Get tuple of columns names leading with primary key followed by sorted remaining columns.
        :column_defs: output of `get_column_defs(pg)`
    '''
    keycols = table['key']
    rec_cols = tuple(keycols + sorted(c for c in rec.keys() if c not in keycols))
    tblid = (table['namespace'], table['tablename'])
    if tblid not in column_defs:
        raise MissingTable('Table %s.%s is not present. Please upgrade database and try again.' % tblid)
    tbl_cols = column_defs[tblid]
    return tuple(  # can't insert or update generated columns, so omit them
        c for c in rec_cols if is_generated_col(tbl_cols, c) is not True)

def insert(pg, table, new, column_defs, _prep_cache={}):  # pylint: disable=dangerous-default-value
    ''' Perform an INSERT. '''
    cols = get_cols(table, column_defs, new)
    prep_key = PreparedStatementKey('insert', table['namespace'], table['tablename'], cols)
    if prep_key in _prep_cache:
        prep_name = _prep_cache[prep_key]
    else:
        prep_name = _prep_cache[prep_key] = gen_prep_name(prep_key)
        pg.enqueue(SQL('''
            PREPARE {prep_name} AS
            INSERT INTO {tbl} ({cols})
            VALUES ({args});
        ''').format(
            prep_name=Identifier(prep_name),
            tbl=get_table_sym(table),
            cols=SQL(', ').join([Identifier(c) for c in cols]),
            args=SQL(', ').join([SQL(f'${i}') for i, _ in enumerate(cols, start=1)]),
        ))
    pg.enqueue(
        SQL('EXECUTE {prep_name} %(vals)s;').format(prep_name=Identifier(prep_name)),
        {'vals': tuple(new[c] for c in cols)})

def update(pg, table, old, new, column_defs, _prep_cache={}):  # pylint: disable=dangerous-default-value
    ''' Perform an UPDATE.
        The WHERE clause is an optimization to eliminate a write when nothing has changed.
    '''
    keycols = table['key']
    cols = get_cols(table, column_defs, new)
    prep_key = PreparedStatementKey('update', table['namespace'], table['tablename'], cols)
    if prep_key in _prep_cache:
        prep_name = _prep_cache[prep_key]
    else:
        prep_name = _prep_cache[prep_key] = gen_prep_name(prep_key)
        pg.enqueue(SQL('''
            PREPARE {prep_name} AS
            UPDATE {tbl}
            SET ({cols}) = ({args})
            WHERE ({pkey}) = ({pkey_args});
        ''').format(
            prep_name=Identifier(prep_name),
            tbl=get_table_sym(table),
            cols=SQL(', ').join([Identifier(c) for c in cols]),
            pkey=SQL(', ').join([Identifier(c) for c in keycols]),
            args=SQL(', ').join([SQL(f'${i}') for i, _ in enumerate(cols, start=1)]),
            pkey_args=SQL(', ').join([SQL(f'${i + len(cols)}') for i, _ in enumerate(keycols, start=1)]),
        ))
    keyrec = old or new  # primary key might change, match on old key
    keyvals = tuple(keyrec[c] for c in keycols)
    pg.enqueue(
        SQL('EXECUTE {prep_name} %(vals)s;').format(prep_name=Identifier(prep_name)),
        {'vals': tuple(new[c] for c in cols) + keyvals})

def parse_unique_error_key(text):
    '''
    >>> parse_unique_error_key('')
    >>> parse_unique_error_key(None)
    >>> parse_unique_error_key('Key (foo_id, bar_id)=(1, 2) already exists.')
    (('foo_id', 'bar_id'), ('1', '2'))
    >>> parse_unique_error_key('Ya existe la llave (foo_id, bar_id)=(1, 2).')
    (('foo_id', 'bar_id'), ('1', '2'))
    >>> parse_unique_error_key('Key (key)=(:1:rl:28c8c88c2) already exists.')
    (('key',), (':1:rl:28c8c88c2',))
    >>> parse_unique_error_key('Key (key)=(some whitespace) already exists.')
    (('key',), ('some whitespace',))
    >>> parse_unique_error_key('Key (key)=(hello, world) already exists.')
    (('key',), ('hello, world',))
    >>> parse_unique_error_key('Key (key, key2)=(hello, world, goodbye) already exists.')
    (('key', 'key2'), ('hello', 'world, goodbye'))
    '''
    if match := re.match(r'.*\((?P<cols>.+)\)=\((?P<vals>.+)\).*', text or ''):
        cols = match.group('cols').split(', ')
        vals = match.group('vals').split(', ', maxsplit=len(cols) - 1)
        return tuple(cols), tuple(vals)

def upsert(pg, table, cmd, old, new, column_defs, onconflict='do update', _prep_cache={}):  # pylint: disable=dangerous-default-value
    ''' Perform an INSERT or UPDATE ("upsert") using PostgreSQL ON CONFLICT clause[1].
        The WHERE clause is an optimization to eliminate a write when nothing has changed.
        https://www.postgresql.org/docs/current/sql-insert.html
    '''
    assert cmd in ('insert', 'update'), f'expected insert or update for cmd, got {cmd!r}'
    cols = get_cols(table, column_defs, new)
    keycols = table['key']
    prep_key = PreparedStatementKey('upsert', table['namespace'], table['tablename'], cols)
    if prep_key in _prep_cache:
        prep_name = _prep_cache[prep_key]
    else:
        prep_name = _prep_cache[prep_key] = gen_prep_name(prep_key)
        pg.enqueue(SQL('''
            PREPARE {prep_name} AS
            INSERT INTO {tbl} ({cols})
            VALUES ({args})''' + ('''
            ON CONFLICT ({pkey}) DO NOTHING;
        ''' if onconflict == 'do nothing' else '''
            ON CONFLICT ({pkey}) DO UPDATE SET
                ({cols}) = ROW({args})
            WHERE ({qualcols}) IS DISTINCT FROM ({args});
        ''')).format(
            prep_name=Identifier(prep_name),
            tbl=get_table_sym(table),
            cols=SQL(', ').join([Identifier(c) for c in cols]),
            qualcols=SQL(', ').join([Identifier(table['tablename'], c) for c in cols]),
            pkey=SQL(', ').join([Identifier(c) for c in keycols]),
            args=SQL(', ').join([SQL(f'${i}') for i, _ in enumerate(cols, start=1)]),
        ))
    # When a non-nullable value is omitted (because it is TOASTed and hasn't changed), then this function will fail
    # the null constraint check on insert; use update instead
    tbl_cols = column_defs.get((table['namespace'], table['tablename']))
    if cmd == 'update' and any(
            k for k, c in tbl_cols.items() if k not in keycols and c.notnull and new.get(k) is None):
        return update(pg, table, old, new, column_defs)
    if old:  # if the primary key has changed, we delete the old record before upserting the new record
        try:
            oldkey = [old[c] for c in keycols]
        except KeyError:
            oldkey = None
        if oldkey and oldkey != [new[c] for c in keycols]:
            delete(pg, table, old)
    pgexec = pg.execute if unique_error_flag_path_exists else pg.enqueue
    try:
        pgexec(
            SQL('EXECUTE {prep_name} %(vals)s;').format(prep_name=Identifier(prep_name)),
            {'vals': tuple(new[c] for c in cols)})
    except psycopg2.errors.UniqueViolation as exc:  # pylint: disable=no-member
        # in order to unblock the subscription, a conflicting record needs to be removed
        if exc and exc.pgcode == '23505':  # unique_violation
            confkey = parse_unique_error_key(exc.diag.message_detail)
            if not confkey:
                raise
            cols, vals = confkey
            pg.rollback()
            ret = pg.execute(SQL('''
                DELETE FROM {tbl}
                WHERE ({cols}) = (%(vals)s)
                RETURNING *;
            ''').format(
                tbl=get_table_sym(table),
                cols=SQL(', ').join([Identifier(c) for c in cols]),
            ), {'vals': vals})
            slog.warn2('conflicting row detected and removed: %r\n    hint: retry to resume', ret.first())
            pg.commit()
            raise

def upsert_batch(pg, table, batch, column_defs, onconflict='do update'):
    ''' Perform an INSERT ... VALUES query with a batch of insert statements for the same table.
        Batching gives better performance than plain upsert.
    '''
    cols = get_cols(table, column_defs, batch[0])
    execute_values(pg.cur,
        SQL('''
            INSERT INTO {tbl} ({cols})
            VALUES %s''' + ('''
            ON CONFLICT ({pkey}) DO NOTHING;
        ''' if onconflict == 'do nothing' else '''
            ON CONFLICT ({pkey}) DO UPDATE SET
                ({cols}) = ROW({exclcols})
            WHERE ({qualcols}) IS DISTINCT FROM ({exclcols});
        ''')).format(
            tbl=get_table_sym(table),
            cols=SQL(', ').join([Identifier(c) for c in cols]),
            qualcols=SQL(', ').join([Identifier(table['tablename'], c) for c in cols]),
            pkey=SQL(', ').join([Identifier(c) for c in table['key']]),
            exclcols=SQL(', ').join([Identifier('excluded', c) for c in cols]),
        ), [tuple(new[c] for c in cols) for new in batch], page_size=len(batch))

def delete(pg, table, old, _prep_cache={}):  # pylint: disable=dangerous-default-value
    ''' Perform a DELETE. '''
    prep_key = PreparedStatementKey('delete', table['namespace'], table['tablename'], tuple(table['key']))
    if prep_key in _prep_cache:
        prep_name = _prep_cache[prep_key]
    else:
        prep_name = _prep_cache[prep_key] = gen_prep_name(prep_key)
        pg.enqueue(SQL('''
            PREPARE {prep_name} AS
            DELETE FROM {tbl}
            WHERE ({pkey}) = ({pkey_args});
        ''').format(
            prep_name=Identifier(prep_name),
            tbl=get_table_sym(table),
            pkey=SQL(', ').join([Identifier(c) for c in table['key']]),
            pkey_args=SQL(', ').join([SQL(f'${i}') for i, _ in enumerate(table['key'], start=1)]),
        ))
    pg.enqueue(
        SQL('EXECUTE {prep_name} %(vals)s;').format(prep_name=Identifier(prep_name)),
        {'vals': tuple(old[c] for c in table['key'])})

def truncate(pg, tables, cascade=False, restartid=False):
    ''' Perform a TRUNCATE command. '''
    statement = 'TRUNCATE {tbls}'
    if cascade:
        statement += ' CASCADE'
    if restartid:
        statement += ' RESTART IDENTITY'
    pg.enqueue(SQL(statement).format(tbls=SQL(', ').join([get_table_sym(table) for table in tables])))
