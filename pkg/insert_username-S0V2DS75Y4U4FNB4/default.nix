stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0V2DS75Y4U4FNB4";
    name = "insert_username";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-insert_username-R000VZAUC3BG6NCY>
    ];
}
