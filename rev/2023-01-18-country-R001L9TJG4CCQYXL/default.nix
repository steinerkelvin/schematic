stdargs @ { scm, ... }:

scm.revision {
    guid = "R001L9TJG4CCQYXL";
    name = "2023-01-18-country";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-06-04-country-R000QSCKEW5MBKWP>
    ];
}
