stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0JX12749PG4Q6G7";
    name = "pltcl";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-pltcl-R000VZAZBEK102GK>
    ];
}
