'''Utilities to support python nix builder scripts.'''

import os
import re
import subprocess
import sys

import psycopg2

from schematic import env


def mkdir(path):
    try:
        os.makedirs(path)
    except OSError as ex:
        if ex.errno not in (17,):
            raise

def build_setup():
    ''' Call nix initializer which propogates transitive dependencies and initializes env vars. '''
    bash = os.path.join(env.get_str('bash'), 'bin/bash') if env.get_str('bash') else 'bash'
    proc = subprocess.Popen([bash, '-c', 'source $stdenv/setup && env'], stdout=subprocess.PIPE)
    for line in proc.stdout:
        (key, _, value) = line.decode('utf8').partition('=')
        os.environ[key] = value
    proc.communicate()

def retry(attempts, exceptions=None, onerror=None):
    '''Decorator to retry a function on error.'''
    import functools  # pylint: disable=import-outside-toplevel

    def outer(func):
        @functools.wraps(func)
        def inner(*args, **kwargs):
            for attempt in range(attempts):
                try:
                    return func(*args, **kwargs)
                except exceptions:
                    if onerror is not None:
                        onerror(*args, **kwargs)
                    if attempt >= attempts - 1:
                        raise
        return inner
    return outer

def pg_is_ready(basedir, port):
    prog = os.path.join(basedir, 'bin/pg_isready')
    if not os.path.exists(prog):
        return
    cmd = [prog, '-d', 'postgres', '-p', str(port), '-h', 'localhost']
    try:
        subprocess.check_call(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return True
    except subprocess.CalledProcessError:
        return False

def poll_pg_isready(basedir, port, duration=10, interval=1):
    ''' Poll for a total of :poll_duration: seconds with interval :poll_interval: until postgresql is ready to
        accept connections. Return boolean indicating success.
    '''
    import time  # pylint: disable=import-outside-toplevel
    start = time.time()
    while time.time() - start < duration:
        if pg_is_ready(basedir, port):
            return True
        time.sleep(interval)
    return False

def pg_ctl_fn(basedir, mode, shutdown_mode='fast', quiet=False):
    pg_ctl = os.path.join(basedir, 'bin/pg_ctl')
    if not os.path.exists(pg_ctl):
        print('error: bin/pg_ctl not found in %s' % (basedir), file=sys.stderr)
        if os.path.exists(os.path.join(basedir, 'default.nix')):
            print('Perhaps you meant to pass a path from %s' % env.get_str('SCM_PG', '~/var/pg'), file=sys.stderr)
        sys.exit(19)

    data_dir = os.path.join(basedir, 'data')
    pg_ctl_log = os.path.join(data_dir, 'pg_ctl.log')
    cmd = [pg_ctl, mode, '-D', data_dir, '-l', pg_ctl_log]
    environ = {}
    if mode in ['stop', 'restart']:
        cmd.extend(['-m', shutdown_mode])
        environ = {
            # this ensures that children don't inherit postmaster's oom_score_adj
            'PG_OOM_ADJUST_FILE': os.environ.get('PG_OOM_ADJUST_FILE', '/proc/self/oom_score_adj'),
        }
    try:
        if quiet:
            output = subprocess.check_call(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        else:
            output = subprocess.check_call(cmd, stdout=2)
    except subprocess.CalledProcessError as ex:
        if ex.returncode != 1:
            raise

def ensure_pg_running(basedir, port, poll_duration=60 * 60 * 24, poll_interval=1):
    ''' Start postgresql if it's not already running. Poll for a total of :poll_duration: seconds with interval
        :poll_interval: until it's up and responding.
    '''
    if not pg_is_ready(basedir, port):
        pg_ctl_fn(basedir, 'start', quiet=(not env.get_bool('SCM_VERBOSE')))
    poll_pg_isready(basedir, port, duration=poll_duration, interval=poll_interval)

def announce_postgresql_ready():
    from schematic import build_msg  # pylint: disable=import-outside-toplevel
    build_msg.send({
        'type': 'nojbohishtim-postgresql-ready',
        'basedir': env.get_str('basedir'),
        'port': env.get_str('port'),
    })

def install_basedir_files(basedir, *derivs):
    for deriv in filter(None, derivs):
        if not os.path.exists(deriv):
            continue
        if deriv.endswith('-basefiles'):
            path = deriv
        elif os.path.exists(os.path.join(deriv, 'basefiles')):
            path = os.path.join(deriv, 'basefiles')
        else:
            continue
        print('installing %r -> %r' % (path, basedir), file=sys.stderr)
        # TODO: more sophisticated policy about copying/overwriting files into database
        subprocess.check_call(['rsync', '-rlc', '%s/' % path, basedir])

def install_buildinput_files():
    install_basedir_files(
        env.get_str('basedir'), env.get_str('out'), env.get_str('basefiles'), *env.get_array('buildInputs'))

def log_msg_upgrade_sql(upgrade_sql, prefix='applying'):
    # truncate: unfortunately shutil.get_terminal_size doesn't get the terminal width inside the env that nix sets up
    import shutil  # pylint: disable=import-outside-toplevel
    width = shutil.get_terminal_size().columns or 152
    text = open(upgrade_sql).read()
    summary = re.sub(r'\s+', ' ', text)
    line = ' '.join([prefix, upgrade_sql, '%s-%s' % (env.get_str('name'), env.get_str('guid')), summary])
    print(line[:width], file=sys.stderr)

def apply_sql_file(pg, upgrade_sql, attempts=3, retry_patience=30):
    ''' Execute contents of a sql file.
        :attempts: retry attempts for some errors related to concurrency/locks
        :retry_patience: only retry if this budget (in seconds) is not exceeded to avoid retrying expensive operations
    '''
    assert attempts >= 1, 'invalid argument attempts = %r' % attempts
    autocommit = env.get_bool('autocommit')
    if autocommit:
        attempts = 1  # these aren't safe to retry, better to have manual intevention
    content = open(upgrade_sql, encoding='utf8').read()
    if not content.strip():
        return
    empty_line = lambda l: not l or not l.strip() or l.lstrip().startswith('--')
    if all(empty_line(l) for l in content.splitlines()):
        return
    log_msg_upgrade_sql(upgrade_sql)
    import time  # pylint: disable=import-outside-toplevel
    start = time.time()
    for attempt in range(1, attempts + 1):
        if attempt > 0 or autocommit:  # autocommit revisions are frequent cause of race conditions, space them out
            import random  # pylint: disable=import-outside-toplevel
            time.sleep(random.random())  # helps mitigate livelock race conditions
        try:
            return pg.execute(content)
        except (psycopg2.errors.InternalError, psycopg2.errors.DeadlockDetected) as ex:  # pylint: disable=no-member
            if retry_patience and time.time() - start >= retry_patience:
                raise
            if attempt >= attempts:
                raise
            if isinstance(ex, psycopg2.errors.InternalError) and 'tuple concurrently updated' in ex.pgerror or '':  # pylint: disable=no-member
                pg.rollback() if not autocommit else 0
                continue  # happens when removing a postgresql catalog record that was concurrently updated
            if isinstance(ex, psycopg2.errors.DeadlockDetected):  # pylint: disable=no-member
                pg.rollback() if not autocommit else 0
                continue  # can happen with CREATE INDEX CONCURRENTLY
            raise

def write_meta_json():
    import datetime  # pylint: disable=import-outside-toplevel
    import json  # pylint: disable=import-outside-toplevel
    from schematic import dates  # pylint: disable=import-outside-toplevel
    with open(os.path.join(env.get_str('basedir'), 'meta.json'), 'w') as metajson:
        metajson.write(json.dumps({
            'guid': env.get_str('guid'),
            'basedir': env.get_str('basedir'),
            'created': dates.datetime_to_unix(datetime.datetime.utcnow()),
            'user': env.get_str('user'),
            'name': env.get_str('name'),
            'dbname': env.get_str('dbname'),
            'port': int(env.get_str('port')),
            'istemp': env.get_bool('SCM_ISTEMP'),
            'sandbox_mode': env.get_str('SCM_SANDBOX_MODE'),
        }))

def install_basedir_postgresql_files():
    basedir = env.get_str('basedir')
    pgdir = env.get_str('postgresql')
    for subdir in 'bin include lib share'.split():
        subprocess.check_call(['rsync', '-arc', os.path.join(pgdir, subdir), basedir])
        subprocess.check_call(['chmod', '-R', '+w', os.path.join(basedir, subdir)])


def alter_sysem_set(pg, key, value):
    pg.execute('ALTER SYSTEM SET %s = %s;' % (key, value))
