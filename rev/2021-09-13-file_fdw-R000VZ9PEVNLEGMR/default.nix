stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZ9PEVNLEGMR";
    name = "2021-09-13-file_fdw";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
