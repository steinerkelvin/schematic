stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000GHY2HI7C7675";
    name = "2020-11-16-country";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-11-16-map_hashtext-R000GHY1HT1XZ13U>
        <2020-11-19-intarray-R000GN1I735GUAX5>
    ];
}
