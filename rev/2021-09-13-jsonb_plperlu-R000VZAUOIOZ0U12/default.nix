stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAUOIOZ0U12";
    name = "2021-09-13-jsonb_plperlu";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-plperlu-R000VZAWR2OODR64>
    ];
}
