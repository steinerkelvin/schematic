stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAYIKBXYJ4Y";
    name = "2021-09-13-hstore_plperl";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-hstore-R000VZAVDPM02V78>
        <2021-09-13-plperl-R000VZATJTXXQGMN>        
    ];
}
