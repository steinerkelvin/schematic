stdargs @ { scm, ... }:

scm.revision {
    guid = "R0016Z29QR3Q5RRJ";
    name = "2022-04-15-bytesize";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
