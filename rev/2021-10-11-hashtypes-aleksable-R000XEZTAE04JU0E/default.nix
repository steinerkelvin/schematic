stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000XEZTAE04JU0E";
    name = "2021-10-11-hashtypes-aleksable";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
