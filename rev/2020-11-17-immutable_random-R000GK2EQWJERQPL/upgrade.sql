CREATE OR REPLACE FUNCTION rand.immutable_random(anyelement) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT random();$$;
