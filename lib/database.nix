stdargs @ { scm, pkgs, python3, rsync, ... }:
args @ {
    guid, name, server, basefiles ? null,
    dependencies ? [], preBuildInputs ? null,
    ...
}:

rec {
    scm_type = "database";
    inherit guid name dependencies server preBuildInputs basefiles;
    depends = scm.deps dependencies;
    transDepGuids = scm.getTransDepGuids depends;
    apply = { self, server }: scm.effect rec {
        inherit (server) postgresql pguri basedir port name;
        inherit (self) guid scm_type transDepGuids basefiles;
        SCM_EFFECT_PROG = ../py/schematic/build_database.py;
        buildInputs = [ rsync ] ++ (scm.buildDeps server self);
    };
}
