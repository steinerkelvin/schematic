'''
Parse pgoutput messages from postgresql's logical replication protcol.

Specification for message formats: https://www.postgresql.org/docs/current/protocol-logicalrep-message-formats.html

Forked from https://github.com/smilliken/py-pgoutput

MIT License

Copyright (c) [2020] [Daniel Geals] [Scott Milliken]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''

import struct
from collections import namedtuple
from datetime import datetime, timedelta
from enum import Enum
from dataclasses import dataclass, field

import pytz


def decode_pg_ts(buf, start):
    r''' Decode a PostgreSQL timestamp; microseconds since PostgreSQL epoch (2000-01-01).
    >>> decode_pg_ts(b'\x00\x00\x00\x00\x00\x00\x00\x00', 0)
    (8, datetime.datetime(2000, 1, 1, 0, 0, tzinfo=<UTC>))
    >>> decode_pg_ts(b'\x00\x03\x8d~\xa4\xc6\x80\x00', 0)
    (8, datetime.datetime(2031, 9, 9, 1, 46, 40, tzinfo=<UTC>))
    '''
    pos, microseconds = decode_int(buf, start, 8)
    return (pos, datetime(2000, 1, 1, 0, 0, 0, 0, tzinfo=pytz.UTC) + timedelta(microseconds=microseconds))


_int_codes = {8: '>q', 4: '>i', 2: '>h', 1: '>b'}

def decode_int(buf, start, width):
    r''' Decode big-endian signed integers of various sizes. https://docs.python.org/2/library/struct.html
        :start: offset into buf
        :width: byte width of integer to read
         Returns (end position, integer)

    >>> decode_int(b'8nnn', 0, 4)
    (4, 946761326)
    >>> decode_int(b'\x00', 0, 1)
    (1, 0)
    >>> decode_int(b'\x00v', 0, 2)
    (2, 118)
    >>> decode_int(b'\x00\x01\xe2@', 0, 4)
    (4, 123456)
    >>> decode_int(b'\x00\x00\x00\x00\x01|\x9f\x18', 0, 8)
    (8, 24944408)
    '''
    return (start + width, struct.unpack(_int_codes[width], buf[start:start + width])[0])

def decode_fixstr(buf, start, width):
    ''' Decode fixed-width string.
        :start: offset into buf
        :width: bytes to read
        Returns (end position, unicode)

    >>> decode_fixstr(b'abcdef', 1, 3)
    (4, 'bcd')
    '''
    return (start + width, buf[start:start + width].decode('utf8'))

def decode_char(buf, start):
    ''' Decode a single byte as an ascii character.
        :start: offset into buf
        Returns (end position, unicode)

    >>> decode_char(b'ABCDEF', 0)
    (1, 'A')
    >>> decode_char(b'ABCDEF', 2)
    (3, 'C')
    '''
    return (start + 1, chr(buf[start]))

def decode_nullstr(buf, pos):
    r''' Decode a null-terminated string.
        :pos: skip to this position in the buffer.
        Returns (end position, str).

    >>> decode_nullstr(b'\0', 0)
    (1, '')
    >>> decode_nullstr(b'abc\0', 0)
    (4, 'abc')
    >>> decode_nullstr(b'abc\0def\0ghi', 4)
    (8, 'def')
    '''
    term = buf.find(b'\0', pos)
    return term + 1, buf[pos:term].decode('utf8')

def decode_tuple(buf, pos):
    tupledata = TupleData(buf, pos)
    return tupledata.pos, tupledata

@dataclass
class PgoutputMessage:

    buf: bytes = field(repr=False)
    byte1: bytes = field(repr=False)

    def __init__(self, buf):
        super().__init__()
        self.buf = buf
        self.pos, self.byte1 = decode_char(self.buf, 0)

@dataclass
class Begin(PgoutputMessage):
    r'''
    Byte1('B')      Identifies the message as a begin message.
    Int64           The final LSN of the transaction.
    Int64           Commit timestamp of the transaction, microseconds since PostgreSQL epoch (2000-01-01).
    Int32           Xid of the transaction.

    >>> Begin(b'B\x00\x00\x00\x00\x00\x01\xe2@\x00\x00\x00\x00\x00\x03\xc4\x80\x00\x00\x11\x04')
    Begin(final_tx_lsn=123456, commit_tx_ts=datetime.datetime(2000, 1, 1, 0, 0, 0, 246912, tzinfo=<UTC>), tx_xid=4356)
    '''

    final_tx_lsn: int
    commit_tx_ts: datetime
    tx_xid: int

    def __init__(self, buf):
        super().__init__(buf)
        assert self.byte1 == 'B'
        self.pos, self.final_tx_lsn = decode_int(self.buf, self.pos, 8)
        self.pos, self.commit_tx_ts = decode_pg_ts(self.buf, self.pos)
        self.pos, self.tx_xid = decode_int(self.buf, self.pos, 4)

@dataclass
class Commit(PgoutputMessage):
    r'''
    Byte1('C')      Identifies the message as a commit message.
    Int8            Flags; currently unused (must be 0).
    Int64           The LSN of the commit.
    Int64           The end LSN of the transaction.
    Int64           Commit timestamp of the transaction, microseconds since PostgreSQL epoch (2000-01-01).

    # pylint: disable=line-too-long
    >>> Commit(b'C\x00\x00\x00\x00\x00\x00\x01\xe2@\x00\x00\x00\x00\x00\x03\xc4\x80\x00\x00\x00\x00\x00\x05\xa6\xc0')
    Commit(flags=0, lsn_commit=123456, final_tx_lsn=246912, commit_tx_ts=datetime.datetime(2000, 1, 1, 0, 0, 0, 370368, tzinfo=<UTC>))
    '''

    flags: int
    lsn_commit: int
    final_tx_lsn: int
    commit_tx_ts: datetime

    def __init__(self, buf):
        super().__init__(buf)
        assert self.byte1 == 'C'
        self.pos, self.flags = decode_int(self.buf, self.pos, 1)
        self.pos, self.lsn_commit = decode_int(self.buf, self.pos, 8)
        self.pos, self.final_tx_lsn = decode_int(self.buf, self.pos, 8)
        self.pos, self.commit_tx_ts = decode_pg_ts(self.buf, self.pos)

@dataclass
class Origin(PgoutputMessage):
    r'''
    Byte1('O')      Identifies the message as an origin message.
    Int64           The LSN of the commit on the origin server.
    String          Name of the origin.

    Note that there can be multiple Origin messages inside a single transaction.
    This seems to be what origin means: https://www.postgresql.org/docs/12/replication-origins.html

    >>> Origin(b'O\x00\x00\x00\x00\x01|\x9f\x18blueorigin\0')
    Origin(lsn=24944408, origin_name='blueorigin')
    '''

    lsn: int
    origin_name: str

    def __init__(self, buf):
        super().__init__(buf)
        assert self.byte1 == 'O'
        self.pos, self.lsn = decode_int(self.buf, self.pos, 8)
        self.pos, self.origin_name = decode_nullstr(self.buf, self.pos)

Column = namedtuple('Column', ('attrelid', 'attname', 'atttypid', 'attnum', 'atttypmod', 'iskey'))

@dataclass
class Relation(PgoutputMessage):
    r'''
    Byte1('R')      Identifies the message as a relation message.
    #Int32           Xid of the transaction (only present for streamed transactions).
    #                This field is available since protocol version 2.
    Int32           ID of the relation.
    String          Namespace (empty string for pg_catalog).
    String          Relation name.
    Int8            Replica identity setting for the relation (same as relreplident in pg_class).
    Int16           Number of columns.

    For each (non-generated) column:

    Int8            Flags for the column. Flag 1 indicated key column.
    String          Name of the column.
    Int32           ID of the column's data type.
    Int32           Type modifier of the column (atttypmod).

    # pylint: disable=line-too-long
    >>> Relation(b'R\x00\x00\x1e\xd8public\0mytable\0d\x00\x02\x01id\0\x00\x00\x00\x17\xff\xff\xff\xff\x00name\0\x00\x00\x00\x13\xff\xff\xff\xff')
    Relation(relid=7896, namespace='public', tablename='mytable', relreplident='d')
    >>> Relation(b'R\x00\x00\x1e\xd8public\0mytable\0d\x00\x02\x01id\0\x00\x00\x00\x17\xff\xff\xff\xff\x00name\0\x00\x00\x00\x13\xff\xff\xff\xff').columns
    [Column(attrelid=7896, attname='id', atttypid=23, attnum=1, atttypmod=-1, iskey=True), Column(attrelid=7896, attname='name', atttypid=19, attnum=2, atttypmod=-1, iskey=False)]
    '''
    relid: int
    namespace: str
    tablename: str
    relreplident: str  # https://www.postgresql.org/docs/current/catalog-pg-class.html
    columns: list[Column] = field(repr=False)

    def __init__(self, buf):
        super().__init__(buf)
        assert self.byte1 == 'R'
        self.pos, self.relid = decode_int(self.buf, self.pos, 4)
        self.pos, self.namespace = decode_nullstr(self.buf, self.pos)
        self.pos, self.tablename = decode_nullstr(self.buf, self.pos)
        self.pos, self.relreplident = decode_char(self.buf, self.pos)
        self.pos, self.n_columns = decode_int(self.buf, self.pos, 2)
        self.columns = []
        for i in range(self.n_columns):
            self.pos, flags = decode_int(self.buf, self.pos, 1)
            self.pos, attname = decode_nullstr(self.buf, self.pos)
            self.pos, atttypid = decode_int(self.buf, self.pos, 4)
            self.pos, atttypmod = decode_int(self.buf, self.pos, 4)
            self.columns.append(Column(self.relid, attname, atttypid, i + 1, atttypmod, bool(flags & 1)))

@dataclass
class PgType(PgoutputMessage):
    r'''
    Byte1('Y')      Identifies the message as a type message.
    #Int32           Xid of the transaction (only present for streamed transactions).
    #                This field is available since protocol version 2.
    Int32           ID of the data type.
    String          Namespace (empty string for pg_catalog).
    String          Name of the data type.

    >>> PgType(b'Y\x00\x01\xe2@public\0mytype\0')
    PgType(oid=123456, namespace='public', typename='mytype')
    >>> PgType(b'Y\x00\x00X<public\x00foo\x00')
    PgType(oid=22588, namespace='public', typename='foo')
    '''

    oid: int
    namespace: str
    typename: str

    def __init__(self, buf):
        super().__init__(buf)
        assert self.byte1 == 'Y'
        self.pos, self.oid = decode_int(self.buf, self.pos, 4)
        self.pos, self.namespace = decode_nullstr(self.buf, self.pos)
        self.pos, self.typename = decode_nullstr(self.buf, self.pos)

TOAST = Enum('TOAST', ['TOAST']).TOAST  # pylint: disable=invalid-name

@dataclass
class TupleData:
    r'''
    Int16           Number of columns.

    For each (non-generated) column:

    Byte1('n')      Identifies the data as NULL value.
    Byte1('u')      Identifies unchanged TOASTed value (the actual value is not sent).
    Byte1('t')      Identifies the data as text formatted value.
    Byte1('b')      Identifies the data as binary formatted value.
        Int32       Length of the column value.
        ByteN       The value of the column in text format. N is the above length.

    >>> TupleData(b'\x00\x02nu', 0)
    TupleData(record=(None, <TOAST.TOAST: 1>))
    >>> TupleData(b'\x00\x02t\x00\x00\x00\x0babcdefghijkn', 0)
    TupleData(record=('abcdefghijk', None))
    '''

    record: list[tuple]

    def __init__(self, buf, pos):
        super().__init__()
        self.buf = buf
        self.pos = pos
        self.record = ()
        self.pos, n_columns = decode_int(self.buf, self.pos, 2)
        for _ in range(n_columns):
            self.pos, col_type = decode_char(self.buf, self.pos)
            if col_type == 'n':  # NULL value
                self.record += (None,)
            elif col_type == 'u':  # TOAST value
                self.record += (TOAST,)
            elif col_type == 't':
                self.pos, width = decode_int(self.buf, self.pos, 4)
                self.pos, value = decode_fixstr(self.buf, self.pos, width)
                self.record += (value,)

@dataclass
class Insert(PgoutputMessage):
    r'''
    Byte1('I')      Identifies the message as an insert message.
    #Int32           Xid of the transaction (only present for streamed transactions).
    #                This field is available since protocol version 2.
    Int32           ID of the relation corresponding to the ID in the relation message.
    Byte1('N')      Identifies the following TupleData message as a new tuple.
    TupleData       TupleData message part representing the contents of new tuple.

    # pylint: disable=line-too-long
    >>> Insert(b'I\x00\x00@EN\x00\x05t\x00\x00\x00\x03120t\x00\x00\x00\x0215t\x00\x00\x00\x06name11t\x00\x00\x00\x1a2020-10-25 19:17:24.113664t\x00\x00\x00\x1a2020-10-25 19:17:24.113664')
    Insert(relid=16453, new_tuple=TupleData(record=('120', '15', 'name11', '2020-10-25 19:17:24.113664', '2020-10-25 19:17:24.113664')))
    '''

    relid: int
    new_tuple: TupleData

    def __init__(self, buf):
        super().__init__(buf)
        assert self.byte1 == 'I'
        self.pos, self.relid = decode_int(self.buf, self.pos, 4)
        self.pos, newtype = decode_char(self.buf, self.pos)
        assert newtype == 'N', 'unexpectd newtype {newtype!r}'
        self.pos, self.new_tuple = decode_tuple(self.buf, self.pos)

@dataclass
class Update(PgoutputMessage):
    r'''
    Byte1('U')      Identifies the message as an update message.
    #Int32           Xid of the transaction (only present for streamed transactions).
    #                This field is available since protocol version 2.
    Int32           ID of the relation corresponding to the ID in the relation message.
    Byte1('K')      Identifies the following TupleData submessage as a key. Optional; present if the update changed
                    data in any of the column(s) that are part of the REPLICA IDENTITY index.
    Byte1('O')      Identifies the following TupleData submessage as an old tuple. Optional; present if table in which
                    the update happened has REPLICA IDENTITY set to FULL.
    TupleData       TupleData message part representing the contents of the old tuple or primary key. Only present if
                    the previous 'O' or 'K' part is present.
    Byte1('N')      Identifies the following TupleData message as a new tuple.
    TupleData       TupleData message part representing the contents of a new tuple.

    The Update message may contain either a 'K' message part or an 'O' message part or neither of them, but never both.

    # pylint: disable=line-too-long
    >>> Update(b'U\x00\x00@EN\x00\x05t\x00\x00\x00\x03120t\x00\x00\x00\x0215t\x00\x00\x00\x07fooooo!t\x00\x00\x00\x1a2020-10-25 19:17:24.113664t\x00\x00\x00\x1a2020-10-25 19:17:24.113664')
    Update(relid=16453, old_tuple=None, new_tuple=TupleData(record=('120', '15', 'fooooo!', '2020-10-25 19:17:24.113664', '2020-10-25 19:17:24.113664')))
    '''

    relid: int
    old_tuple: TupleData
    new_tuple: TupleData

    def __init__(self, buf):
        super().__init__(buf)
        assert self.byte1 == 'U'
        self.pos, self.relid = decode_int(self.buf, self.pos, 4)
        self.pos, tupletype = decode_char(self.buf, self.pos)
        if tupletype in ('K', 'O'):
            self.pos, self.old_tuple = decode_tuple(self.buf, self.pos)
            self.pos, tupletype = decode_char(self.buf, self.pos)
        else:
            self.old_tuple = None
        assert tupletype == 'N', 'invalid tupletype: {tupletype!r}'
        self.pos, self.new_tuple = decode_tuple(self.buf, self.pos)
        self.pos = self.new_tuple.pos

@dataclass
class Delete(PgoutputMessage):
    r'''
    Byte1('D')      Identifies the message as a delete message.
    #Int32           Xid of the transaction (only present for streamed transactions).
    #                This field is available since protocol version 2.
    Int32           ID of the relation corresponding to the ID in the relation message.
    Byte1('K')      Identifies the following TupleData submessage as a key.
                    This field is present if the table uses an index as REPLICA IDENTITY.
    Byte1('O')      Identifies the following TupleData message as a old tuple.
                    This field is present if the table has REPLICA IDENTITY set to FULL.
    TupleData       TupleData message part representing the contents of the old tuple or primary key.

    The Delete message may contain either a 'K' message part or an 'O' message part, but never both of them.

    >>> Delete(b'D\x00\x00@EK\x00\x05t\x00\x00\x00\x03120nnnn')
    Delete(relid=16453, old_tuple=TupleData(record=('120', None, None, None, None)))
    '''

    relid: int
    old_tuple: TupleData

    def __init__(self, buf):
        super().__init__(buf)
        assert self.byte1 == 'D'
        self.pos, self.relid = decode_int(self.buf, self.pos, 4)
        self.pos, dtype = decode_char(self.buf, self.pos)
        assert dtype in ('K', 'O'), 'unexpected dtype {dtype!r}'
        self.pos, self.old_tuple = decode_tuple(self.buf, self.pos)

@dataclass
class Truncate(PgoutputMessage):
    r'''
    Byte1('T')      Identifies the message as a truncate message.
    #Int32           Xid of the transaction (only present for streamed transactions).
    #                This field is available since protocol version 2.
    Int32           Number of relations
    Int8            Option bits for TRUNCATE: 1 for CASCADE, 2 for RESTART IDENTITY
    Int32           ID of the relation corresponding to the ID in the relation message. Repeated for each relation.

    # pylint: disable=line-too-long
    >>> Truncate(b'T\x00\x00\x00\x12\x01\x00\x00A\x9f\x00\x00A\xb6\x00\x00CW\x00\x00D\xc9\x00\x00E\x06\x00\x00E#\x00\x00C\x94\x00\x00D\x02\x00\x00D@\x00\x00DT\x00\x00Du\x00\x00E;\x00\x00EO\x00\x00Ev\x00\x00E\xc7\x00\x00F8\x00\x00E\xdb\x00\x00F\x08')
    Truncate(options=1, cascade=True, restartid=False, relids=[16799, 16822, 17239, 17609, 17670, 17699, 17300, 17410, 17472, 17492, 17525, 17723, 17743, 17782, 17863, 17976, 17883, 17928])
    '''

    options: int
    cascade: bool
    restartid: bool
    relids: list[int]

    def __init__(self, buf):
        super().__init__(buf)
        assert self.byte1 == 'T'
        self.pos, relcount = decode_int(self.buf, self.pos, 4)
        self.pos, self.options = decode_int(self.buf, self.pos, 1)
        self.cascade = bool(self.options & 1)
        self.restartid = bool(self.options & 2)
        self.relids = []
        for _ in range(relcount):
            self.pos, relid = decode_int(self.buf, self.pos, 4)
            self.relids.append(relid)

@dataclass
class Message(PgoutputMessage):
    r'''
    Byte1('M')      Identifies the message as a logical decoding message.
    #Int32           Xid of the transaction (only present for streamed transactions).
    #                This field is available since protocol version 2.
    Int8            Flags; Either 0 for no flags or 1 if the logical decoding message is transactional.
    Int64           The LSN of the logical decoding message.
    String          The prefix of the logical decoding message.
    Int32           Length of the content.
    Byten           The content of the logical decoding message.

    >>> Message(b'M\x00\x00\x00\x00\x00\x00\x01\xe2\x01hello\0\x00\x00\x00\x07goodbye')
    Message(flags=0, lsn=123393, prefix='hello', content='goodbye')
    '''

    # xid: int  # disabled because we are using protocol version 1, and don't support streamed txns anyway
    flags: int
    lsn: int
    prefix: str
    content: str

    def __init__(self, buf):
        super().__init__(buf)
        assert self.byte1 == 'M'
        self.pos, self.flags = decode_int(self.buf, self.pos, 1)
        self.pos, self.lsn = decode_int(self.buf, self.pos, 8)
        self.pos, self.prefix = decode_nullstr(self.buf, self.pos)
        self.pos, contentlen = decode_int(self.buf, self.pos, 4)
        self.pos, self.content = decode_fixstr(self.buf, self.pos, contentlen)

opcodes = {
    'B': Begin,
    'C': Commit,
    'R': Relation,
    'I': Insert,
    'U': Update,
    'D': Delete,
    'T': Truncate,
    'O': Origin,
    'Y': PgType,
    'M': Message,
}

def decode_message(buf):
    ''' Peak first byte and initialise the appropriate message object. '''
    return opcodes[chr(buf[0])](buf)
