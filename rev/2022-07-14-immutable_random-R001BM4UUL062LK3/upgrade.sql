CREATE OR REPLACE FUNCTION rand.immutable_random(bigint) RETURNS double precision
    LANGUAGE sql IMMUTABLE
    AS $$SELECT random();$$;
