stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0LCV0IOU47TX1RW";
    name = "pg_freespacemap";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-pg_freespacemap-R000VZAU82APQW3F>
    ];
}
