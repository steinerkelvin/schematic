stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0VEQAXGNGHNCY1L";
    name = "btree_gist";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-btree_gist-R000VZ9NMOQEDNAB>
    ];
}
