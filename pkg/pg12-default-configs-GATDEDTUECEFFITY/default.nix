stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "GATDEDTUECEFFITY";
    name = "pg12-default-configs";
    upgrade_sql = ./upgrade.sql;
    basefiles = ./basefiles;
    dependencies = [
        <2020-04-15-pg12-default-configs-OORNEIFAJURERJYE>
    ];
}
