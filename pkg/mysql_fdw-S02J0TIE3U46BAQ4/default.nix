stdargs @ { scm, pkgs, ... }:

scm.schema rec {
    guid = "S02J0TIE3U46BAQ4";
    name = "mysql_fdw";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-06-22-mysql_fdw-R000RPBS91OL04WM>
    ];
    preBuildInputs = { pkgs, ... }: [
        (pkgs.callPackage ./extension.nix (stdargs // {inherit name guid;}) )
    ];
}
