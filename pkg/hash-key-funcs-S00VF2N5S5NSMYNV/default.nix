stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S00VF2N5S5NSMYNV";
    name = "hash-key-funcs";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <smallint_bit-S03PG8N8AKZ0964V>
        <2021-08-31-hash-key-funcs-R000VBAGY4HINRSK>
    ];
}
