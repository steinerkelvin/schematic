stdargs @ { scm, pkgs, ... }:

scm.schema {
    guid = "S0YQVIT96AMJO3NS";
    name = "adminpack";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-adminpack-R000VZAX4T2NUK2Z>
    ];
}
