CREATE EXTENSION IF NOT EXISTS pgstattuple WITH SCHEMA public;
COMMENT ON EXTENSION pgstattuple IS 'show tuple-level statistics';
