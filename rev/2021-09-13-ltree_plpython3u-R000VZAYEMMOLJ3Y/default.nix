stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAYEMMOLJ3Y";
    name = "2021-09-13-ltree_plpython3u";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2021-09-13-ltree-R000VZAUKUQZ13LE>
        <2021-09-13-plpython3u-R000VZDMTW1CCIL1>
    ];
}
