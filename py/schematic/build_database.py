from schematic import env
from schematic.build_util import install_buildinput_files


def main():
    install_buildinput_files()

if __name__ == '__main__':
    main()
