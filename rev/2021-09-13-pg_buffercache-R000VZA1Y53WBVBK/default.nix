stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZA1Y53WBVBK";
    name = "2021-09-13-pg_buffercache";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
