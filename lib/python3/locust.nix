{ pkgs
, stdenv
, buildPythonPackage
, pygit2
, pyyaml
, lxml
}: buildPythonPackage rec {
    name = "locust-2020-10-21";
    src = pkgs.fetchFromGitHub {
        owner  = "simiotics";
        repo   = "locust";
        rev    = "4fb908572fd185d15383520e53afc703fc461cc3";
        sha256 = "0zq1f7yq3bnw2lsbycs27nv1in1fmad8jk7n6z251ry19szz8gfn";
    };
    propagatedBuildInputs = [
        pygit2
        pyyaml
        lxml
    ];
    doCheck = false;
}
