'''
Build a postgresql replica server.
'''
import os
import re
import subprocess
import sys

from toolz import dicttoolz

from schematic import env, pgcn
from schematic.build_util import (
    alter_sysem_set, ensure_pg_running, install_basedir_postgresql_files, mkdir, write_meta_json)


def main():
    port = env.get_str('port')
    name = env.get_str('name')
    dbname = env.get_str('dbname')  # pylint: disable=unused-variable
    guid = env.get_str('guid')
    user = env.get_str('user')
    basedir = env.get_str('basedir')
    primary_host = env.get_str('primary_host')
    primary_port = env.get_str('primary_port')
    primary_dbname = env.get_str('primary_dbname')
    primary_user = env.get_str('primary_user')
    primary_password = env.get_str('primary_password')
    mkdir(basedir)
    write_meta_json()
    datadir = env.get_str('datadir')
    nodatadir = not os.path.exists(datadir)
    install_basedir_postgresql_files()
    if nodatadir:
        # install_basedir_postgresql_files may have created datadir, but it needs to be empty to init replication
        try:
            os.unlink(datadir)
        except FileNotFoundError:
            pass
    # .pgpass file https://www.postgresql.org/docs/12/libpq-pgpass.html
    pgpassfile = os.path.join(basedir, '.pgpass')
    try:
        with open(pgpassfile, 'w') as passfile:
            passfile.write(':'.join([primary_host, primary_port, primary_dbname, primary_user, primary_password]))
        subprocess.check_call(['chmod', '0600', pgpassfile])
        environ = dicttoolz.merge({'PGPASSFILE': pgpassfile}, os.environ)
        datadir = env.get_str('datadir')
        if os.path.exists(datadir):
            print('datadir already initialized: %s' % datadir, file=sys.stderr)
            sys.exit(0)
        slot = ('%s-%s' % (name, guid)).replace('-', '_').lower()
        try:
            # drop the slot if it already exists so that pg_basebackup doesn't fail creating it
            subprocess.check_call([
                'pg_receivewal', '--no-password', '--username', user, '--host', primary_host, '--port', primary_port,
                '--slot', slot, '--drop-slot'], env=environ, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        except subprocess.CalledProcessError:
            pass
        basebackup_args = [
            os.path.join(basedir, 'bin/pg_basebackup'), '--pgdata', datadir, '--write-recovery-conf',
            '--progress', '--create-slot', '--slot', slot, '--verbose',
            '--no-password', '--username', user, '--host', primary_host, '--port', primary_port]
        if env.get_bool('SCM_ISTEMP', default=True):
            basebackup_args += ['--no-sync']
        subprocess.check_call(basebackup_args, env=environ)
        install_basedir_postgresql_files()
        with open(os.path.join(datadir, 'postgresql.conf'), 'a') as conf_fp:
            conf_fp.write('\nport = %s\n' % port)
        try:
            # remove the port defined by the primary
            auto_conf = os.path.join(datadir, 'postgresql.auto.conf')
            auto_content = open(auto_conf).readlines()
            with open(auto_conf, 'w') as conf_fp:
                conf_fp.write('\n'.join([l for l in auto_content if not re.match(r'\s*port\s*=', l)]))
                conf_fp.write('\nport = %s\n' % port)
        except FileNotFoundError:
            pass
        ensure_pg_running(basedir, port)
        with pgcn.connect(env.get_str('pguri'), autocommit=True) as pg:
            alter_sysem_set(pg, 'port', port)
        install_basedir_postgresql_files()
    finally:
        try:
            os.unlink(pgpassfile)
        except OSError:
            pass

if __name__ == '__main__':
    main()
