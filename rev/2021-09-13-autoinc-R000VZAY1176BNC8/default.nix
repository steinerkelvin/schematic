stdargs @ { scm, pkgs, ... }:

scm.revision {
    guid = "R000VZAY1176BNC8";
    name = "2021-09-13-autoinc";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
