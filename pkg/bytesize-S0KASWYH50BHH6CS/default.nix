stdargs @ { scm, ... }:

scm.schema {
    guid = "S0KASWYH50BHH6CS";
    name = "bytesize";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2022-04-15-bytesize-R0016Z29QR3Q5RRJ>
    ];
}
